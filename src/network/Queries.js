import { fetchFactory } from '../utils';
import { persistStore, apartmentStore } from '../stores';
import { ToastAndroid } from 'react-native';

export async function loginQuery(username, password) {
  try {
    const response = await fetchFactory('/user/login', {
      method: 'POST',
      body: {
        username: username,
        password: password,
      },
    });
    if (response.ok) {
      const responseData = (await response.json())[0];
      if (responseData.ResultCode == 1) {
        const { Token } = responseData;
        persistStore.token = Token;
        return true;
      } else {
        return false;
      }
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function userInfoQuery() {
  try {
    const response = await fetchFactory('/user/info', {
      method: 'GET',
    });
    const responseData = await response.json();
    //TODO: FIX LATER
    if (response.ok) {
      let {
        ID,
        RoleID,
        Apartment_ID,
        Name,
        Status,
        UnitNumber,
      } = responseData[0];
      apartmentStore.userID = ID;
      apartmentStore.userRole = RoleID;
      apartmentStore.apartmentID = Apartment_ID;
      apartmentStore.apartmentName = Name[0];
      apartmentStore.userName = Name[1];
      apartmentStore.userStatus = Status;
      apartmentStore.userUnitNumber = UnitNumber;
      return true;
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function apartmentDataQuery() {
  try {
    const response = await fetchFactory('/apartment', {
      method: 'GET',
    });
    if (response.ok) {
      const {
        PostalCode,
        NumberOfFloors,
        NumberOfUnits,
        LobbyTel,
        Image,
      } = await response.json();
      apartmentStore.postalCode = PostalCode;
      apartmentStore.numberOfFloors = NumberOfFloors;
      apartmentStore.numberOfUnits = NumberOfUnits;
      apartmentStore.lobbyTell = LobbyTel;
      apartmentStore.apartmentImage = Image;
      return true;
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function getAllApartmentQuery() {
  try {
    const response = await fetchFactory('/apartment/all', {
      method: 'GET',
    });
    if (response.ok) {
      apartmentStore.allApartments = await response.json();
      return true;
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function unitsQuery(id) {
  try {
    const response = await fetchFactory(`/unit/${id}`, {
      method: 'GET',
    });
    if (response.ok) {
      apartmentStore.units = await response.json();
      return true;
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function addUnitQuery(unit) {
  try {
    const response = await fetchFactory('/unit', {
      method: 'POST',
      body: unit,
    });
    const responseData = await response.json();
    if (response.ok) {
      if (responseData[0].ResultCode == 1) {
        const fetchResponse = await unitsQuery(apartmentStore.apartmentID);
        if (fetchResponse) {
          return true;
        }
      } else {
        ToastAndroid.show(responseData[0].ResultText, ToastAndroid.LONG);
        return false;
      }
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function phoneBookQuery(id) {
  try {
    const response = await fetchFactory(`/phoneBook/${id}`, {
      method: 'GET',
    });
    if (response.ok) {
      apartmentStore.phoneBook = await response.json();
      return true;
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function addPhoneBookQuery(phone) {
  try {
    const response = await fetchFactory('/phoneBook', {
      method: 'POST',
      body: phone,
    });
    const responseData = await response.json();
    if (response.ok) {
      if (responseData[0].ResultCode == 1) {
        const queryRes = await phoneBookQuery(apartmentStore.apartmentID);
        if (queryRes) {
          return true;
        }
      } else {
        ToastAndroid.show(responseData[0].ResultText, ToastAndroid.LONG);
        return false;
      }
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function deletePhoneBookQuery(phoneID) {
  try {
    const response = await fetchFactory(`/phoneBook/${phoneID}`, {
      method: 'DELETE',
    });
    const responseData = await response.json();
    if (response.ok) {
      if (responseData[0].ResultCode == 1) {
        const queryRes = await phoneBookQuery(apartmentStore.apartmentID);
        if (queryRes) {
          return true;
        }
      } else {
        ToastAndroid.show(responseData[0].ResultText, ToastAndroid.LONG);
        return false;
      }
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function addNoticeBoardQuery(notice) {
  try {
    const response = await fetchFactory('/noticeBoard', {
      method: 'POST',
      body: notice,
    });
    const responseData = await response.json();
    console.warn(JSON.stringify(responseData));
    if (response.ok) {
      if (responseData[0].ResultCode == 1) {
        const queryRes = await getNoticeBoard(apartmentStore.apartmentID);
        if (queryRes) {
          return true;
        }
      } else {
        ToastAndroid.show(responseData[0].ResultText, ToastAndroid.LONG);
        return false;
      }
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function getNoticeBoard(apartmentID) {
  try {
    const response = await fetchFactory(`/noticeBoard/${apartmentID}`, {
      method: 'GET',
    });
    if (response.ok) {
      apartmentStore.noticeBoard = await response.json();
      return true;
    }
    return false;
  } catch (e) {
    return false;
  }
  return false;
}

export async function getContactUsQuery(apartmentID) {
  try {
    const response = await fetchFactory(`/contactUs/${apartmentID}`, {
      method: 'GET',
    });
    if (response.ok) {
      apartmentStore.contactUs = await response.json();
      return true;
    }
    return false;
  } catch (e) {
    return false;
  }
}

export async function getContactUsTypesQuery() {
  try {
    const response = await fetchFactory(`/contactUs/title`, {
      method: 'PUT',
    });
    if (response.ok) {
      apartmentStore.contactUsTypes = await response.json();
      return true;
    }
    return false;
  } catch (e) {
    return false;
  }
}

export async function addContactUsQuery(contact) {
  try {
    const response = await fetchFactory('/contactUs', {
      method: 'POST',
      body: contact,
    });
    const responseData = await response.json();
    if (response.ok) {
      if (responseData[0].ResultCode == 1) {
        const queryRes = await getContactUsQuery(apartmentStore.apartmentID);
        if (queryRes) {
          return true;
        }
      } else {
        ToastAndroid.show(responseData[0].ResultText, ToastAndroid.LONG);
        return false;
      }
      return false;
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function deleteContactUsQuery(contactID) {
  try {
    const response = await fetchFactory(`/contactUs/${contactID}`, {
      method: 'DELETE',
    });
    const responseData = await response.json();
    if (response.ok) {
      if (responseData[0].ResultCode == 1) {
        const queryRes = await getContactUsQuery(apartmentStore.apartmentID);
        if (queryRes) {
          return true;
        }
      } else {
        ToastAndroid.show(responseData[0].ResultText, ToastAndroid.LONG);
        return false;
      }
      return false;
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function getSuggestionQuery(apartmentID) {
  try {
    const response = await fetchFactory(`/suggestion/${apartmentID}`, {
      method: 'GET',
    });

    if (response.ok) {
      apartmentStore.suggestion = await response.json();
      return true;
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function addSuggestionQuery(suggestion) {
  try {
    const response = await fetchFactory('/suggestion', {
      method: 'POST',
      body: suggestion,
    });
    const responseData = await response.json();
    if (response.ok) {
      if (responseData[0].ResultCode == 1) {
        const queryRes = await getSuggestionQuery(apartmentStore.apartmentID);
        if (queryRes) {
          return true;
        }
      } else {
        ToastAndroid.show(responseData[0].ResultText, ToastAndroid.LONG);
        return false;
      }
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function getNotificationQuery() {
  try {
    const response = await fetchFactory('/notification', {
      method: 'GET',
    });
    const responseData = await response.json();
    console.warn(JSON.stringify(responseData));
    if (response.ok) {
      apartmentStore.notification = responseData;
      return true;
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function getFacilityQuery(apartmentID) {
  try {
    const response = await fetchFactory(`/facility/${apartmentID}`, {
      method: 'GET',
    });
    if (response.ok) {
      apartmentStore.facility = await response.json();
      return true;
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function addFacilityQuery(facility) {
  try {
    const response = await fetchFactory('/facility', {
      method: 'POST',
      body: facility,
    });
    const responseData = await response.json();
    if (response.ok) {
      if (responseData[0].ResultCode == 1) {
        const queryRes = await getFacilityQuery(apartmentStore.apartmentID);
        if (queryRes) {
          return true;
        } else {
          return false;
        }
      } else {
        ToastAndroid.show(responseData[0].ResultText, ToastAndroid.LONG);
        return false;
      }
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function changeFacilityDetails(facility) {
  try {
    const response = await fetchFactory('/facility/Detail/Change', {
      method: 'POST',
      body: facility,
    });
    const responseData = await response.json();
    if (response.ok) {
      if (responseData[0].ResultCode == 1) {
        return true;
      } else {
        ToastAndroid.show(responseData[0].ResultText, ToastAndroid.LONG);
        return false;
      }
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function getFacilityDetailsQuery(apartmentID, facilityID) {
  try {
    const response = await fetchFactory(
      `/facility/${apartmentID}/${facilityID}`,
      {
        method: 'GET',
      },
    );
    const responseData = await response.json();
    if (response.ok) {
      return { status: true, data: responseData };
    }
    return { status: false };
  } catch (e) {
    return { status: false };
  }
}

export async function getFacilityTitles() {
  try {
    const response = await fetchFactory('/facility/title', {
      method: 'PUT',
    });
    const responseData = await response.json();
    if (response.ok && responseData.length > 0) {
      apartmentStore.facilityTypes = responseData;
      return true;
    }
    return false;
  } catch (e) {
    return { status: false };
  }
}

export async function getFacilityDetailsSpecialDate(
  queryInfo,
  apartmentID,
  facilityID,
) {
  try {
    const response = await fetchFactory(
      `/facility/DetailSpecialDate/${apartmentID}/${facilityID}`,
      {
        method: 'PUT',
        body: queryInfo,
      },
    );
    const responseData = await response.json();
    if (response.ok) {
      return { status: true, data: responseData };
    }
    return { status: false };
  } catch (e) {
    return { status: false };
  }
  return { status: false };
}

export async function addSansQuery(sans) {
  try {
    const response = await fetchFactory('/facility/detail', {
      method: 'POST',
      body: sans,
    });
    const responseData = await response.json();
    if (response.ok) {
      if (responseData[0].ResultCode == 1) {
        const queryRes = await getFacilityDetailsQuery(
          apartmentStore.apartmentID,
          sans.facility_ID,
        );
        return queryRes;
      } else {
        ToastAndroid.show(responseData[0].ResultText, ToastAndroid.LONG);
        return { status: false };
      }
    }
    return { status: false };
  } catch (e) {
    return { status: false };
  }
  return { status: false };
}

export async function addRuleQuery(rule) {
  try {
    const response = await fetchFactory('/rule', {
      method: 'POST',
      body: rule,
    });
    const responseData = await response.json();
    if (response.ok) {
      if (responseData[0].ResultCode == 1) {
        const queryRes = getRuleQuery(apartmentStore.apartmentID);
        if (queryRes) {
          return true;
        }
      } else {
        ToastAndroid.show(responseData[0].ResultText, ToastAndroid.LONG);
        return false;
      }
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function getRuleQuery(apartmentID) {
  try {
    const response = await fetchFactory(`/rule/${apartmentID}`, {
      method: 'GET',
    });
    if (response.ok) {
      apartmentStore.rule = await response.json();
      return true;
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function getCostQuery(apartmentID) {
  try {
    const response = await fetchFactory(`/cost/Header/${apartmentID}`, {
      method: 'GET',
    });

    if (response.ok) {
      apartmentStore.costs = await response.json();
      return true;
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function getCostTypeQuery() {
  try {
    const response = await fetchFactory('/cost/type', {
      method: 'PUT',
    });

    if (response.ok) {
      apartmentStore.costTypes = await response.json();
      apartmentStore.costTypes.splice(0, 0, {
        ID: 0,
        Name: 'انتخاب کنید',
        HasDetail: false,
      });
      return true;
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function getSurveyQuery(apartmentID) {
  try {
    const response = await fetchFactory(`/survey/${apartmentID}`, {
      method: 'GET',
    });
    if (response.ok) {
      apartmentStore.survey = await response.json();
      return true;
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function submitSurveyResultQuery(result, ID) {
  try {
    const response = await fetchFactory(`/survey/opinion/${ID}`, {
      method: 'POST',
      body: result,
    });
    const responseData = await response.json();
    if (response.ok) {
      if (responseData[0].ResultCode == 1) {
        const queryRes = getSurveyQuery(apartmentStore.apartmentID);
        if (queryRes) {
          return true;
        }
      } else {
        ToastAndroid.show(responseData[0].ResultText, ToastAndroid.LONG);
        return false;
      }
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function submitSurveyQuery(survey) {
  try {
    const response = await fetchFactory(`/survey`, {
      method: 'POST',
      body: survey,
    });
    const responseData = await response.json();
    if (response.ok) {
      if (responseData[0].ResultCode == 1) {
        const queryRes = getSurveyQuery(apartmentStore.apartmentID);
        if (queryRes) {
          return true;
        }
      } else {
        ToastAndroid.show(responseData[0].ResultText, ToastAndroid.LONG);
        return false;
      }
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function addCostQuery(cost) {
  try {
    const response = await fetchFactory('/cost', {
      method: 'POST',
      body: cost,
    });
    const responseData = await response.json();
    if (response.ok) {
      if (responseData[0].ResultCode == 1) {
        const costRes = await getCostQuery(apartmentStore.apartmentID);
        if (costRes) {
          return true;
        }
      } else {
        ToastAndroid.show(responseData[0].ResultText, ToastAndroid.LONG);
        return false;
      }
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function confirmCostQuery(cost) {
  try {
    const response = await fetchFactory('/cost/confirm', {
      method: 'POST',
      body: cost,
    });
    const responseData = await response.json();
    if (response.ok) {
      if (responseData[0].ResultCode == 1) {
        const costRes = await getCostQuery(apartmentStore.apartmentID);
        if (costRes) {
          return true;
        }
      } else {
        ToastAndroid.show(responseData[0].ResultText, ToastAndroid.LONG);
        return false;
      }
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function getInvoiceQuery() {
  try {
    const response = await fetchFactory('/invoice', {
      method: 'GET',
    });
    const responseData = await response.json();
    if (response.ok) {
      apartmentStore.invoice = responseData;
      return true;
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function getInvoiceURL(invoiceID) {
  try {
    const response = await fetchFactory(`/invoice/bank/${invoiceID}`, {
      method: 'GET',
    });
    const responseData = await response.json();
    if (response.ok) {
      return { status: true, data: responseData };
    }
    return { status: false };
  } catch (e) {
    return { status: false };
  }
  return { status: false };
}

export async function getInvoiceDetailsQuery(body) {
  try {
    const response = await fetchFactory(`/invoice/${body.id}`, {
      method: 'GET',
    });
    const responseData = await response.json();
    if (response.ok) {
      return { status: true, data: responseData };
    }
  } catch (e) {
    return { status: false };
  }
  return { status: false };
}

export async function getRequestQuery(apartmentID) {
  try {
    const response = await fetchFactory(`/requests/${apartmentID}`, {
      method: 'GET',
    });
    const responseData = await response.json();
    if (response.ok) {
      apartmentStore.requests = responseData;
      return true;
    }
  } catch (e) {
    return false;
  }
  return false;
}

export async function addRequestQuery(request) {
  try {
    const response = await fetchFactory('/requests', {
      method: 'POST',
      body: request,
    });
    const responseData = await response.json();
    console.warn(JSON.stringify(responseData));
    if (response.ok) {
      if (responseData[0].ResultCode == 1) {
        const reqestRes = await getRequestQuery(apartmentStore.apartmentID);
        if (reqestRes) {
          return true;
        }
      } else {
        ToastAndroid.show(responseData[0].ResultText, ToastAndroid.LONG);
        return false;
      }
    }
  } catch (e) {
    return false;
  }
  return false;
}
