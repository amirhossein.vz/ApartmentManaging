import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  View,
  I18nManager,
  AsyncStorage,
  Platform,
} from 'react-native';
import { StackNavigator, DrawerNavigator } from 'react-navigation';
import { setCustomTextInput, setCustomText } from 'react-native-global-props';
import RNRestart from 'react-native-restart';
import Orientation from 'react-native-orientation';

import * as components from './components';
import * as screens from './screens';
import { getNotificationQuery } from './network/Queries';

const customTextProps = {
  style: {
    fontFamily:
      Platform.OS == 'android' ? 'IRANSansMedium' : 'IRANSansMobile(FaNum)',
    writingDirection: 'rtl',
  },
};

export default class ApartmentApp extends Component {
  componentWillMount() {
    Orientation.lockToPortrait();
    setCustomText(customTextProps);
    setCustomTextInput(customTextProps);
    setInterval(function() {
      getNotificationQuery();
    }, 60000);
  }

  render() {
    return <MainNavigator />;
  }
}

const DrawerScreens = DrawerNavigator(
  {
    Main: { screen: screens.Main },
  },
  {
    drawerWidth: 270,
    drawerPosition: Platform.OS == 'android' ? 'right' : 'left',
    contentComponent: components.DrawerPanel,
    header: {
      visible: false,
    },
  },
);

const MainNavigator = StackNavigator(
  {
    Splash: { screen: screens.Splash },
    SelectApartment: { screen: screens.SelectApartment },
    Login: { screen: screens.Login },
    FacilityManaging: { screen: screens.FacilityManaging },
    AddContact: { screen: screens.AddContact },
    ContactUs: { screen: screens.ContactUs },
    Main: { screen: DrawerScreens },
    Facilities: { screen: screens.Facilities },
    FacilityDetails: { screen: screens.FacilityDetails },
    Costs: { screen: screens.Costs },
    Contacts: { screen: screens.Contacts },
    Suggestion: { screen: screens.Suggestion },
    Informs: { screen: screens.Informs },
    Units: { screen: screens.Units },
    AddCost: { screen: screens.AddCost },
    AddUnit: { screen: screens.AddUnit },
    Lobby: { screen: screens.Lobby },
    AddSans: { screen: screens.AddSans },
    Rules: { screen: screens.Rules },
    Reports: { screen: screens.Reports },
    NonSchedule: { screen: screens.NonSchedule },
    Requests: { screen: screens.Requests },
  },
  { headerMode: 'none' },
);

const Client = () => {
  return <ApartmentApp />;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

async function applyRTL(rtl) {
  I18nManager.allowRTL(rtl);
  I18nManager.forceRTL(rtl);
  const expected = '' + rtl;
  const RTL_KEY = '@Apartment:isRTL';
  const value = await AsyncStorage.getItem(RTL_KEY);
  if (value !== expected) {
    await AsyncStorage.setItem(RTL_KEY, expected);
    RNRestart.Restart();
  }
}
applyRTL(true);

AppRegistry.registerComponent('apartmentManaging', () => Client);
