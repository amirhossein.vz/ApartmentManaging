import { persistStore } from './stores';
import { serverURL } from './constants/values';
import { monthOfYear } from './constants/values';
var moment = require('moment-jalaali');

export async function fetchFactory(url, options) {
  const { body, ...rest } = options;
  return await fetch(serverURL + url, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'x-access-token': persistStore.token,
    },
    body: JSON.stringify(body),
    ...rest,
  });
}

export function parseTimeToJalaali(time) {
  return (
    moment(time, 'YYYY-M-D HH:mm:ss').format('jYYYY/jM/jD') +
    '   ' +
    moment(time, 'YYYY-M-D HH:mm:ss').format('HH:mm')
  );
}

export function parseTimeToString(time) {
  let timeString = ':';
  if (time.getHours() < 10) {
    timeString = '0' + time.getHours() + timeString;
  } else {
    timeString = time.getHours() + timeString;
  }

  if (time.getMinutes() < 10) {
    timeString = timeString + '0' + time.getMinutes();
  } else {
    timeString = timeString + time.getMinutes();
  }
  return timeString;
}
