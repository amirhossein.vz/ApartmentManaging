import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import { phonecall } from 'react-native-communications';

import { primary, fabColor, primaryDark } from '../constants/colors';
import { Toolbar, AndroidBackButton } from '../components';
import images from '@assets/images';
import { apartmentStore } from '../stores';

const { height, width } = Dimensions.get('window');

export default class Lobby extends Component {
  render() {
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: 'لابی آپارتمان',
      },
    };
    return (
      <View style={{ flex: 1 }}>
        <Toolbar customStyle={toolbarStyle} />
        <AndroidBackButton
          onPress={() => {
            this.onBackPress();
            return true;
          }}
        />
        <View
          style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
        >
          <View
            style={{
              elevation: 4,
              marginVertical: 8,
            }}
          >
            <Image
              source={images.lobby_image}
              style={{
                width: width - 8,
                flex: 1,
                resizeMode: 'cover',
                justifyContent: 'flex-end',
              }}
            >
              <TouchableOpacity
                activeOpacity={0.4}
                onPress={() => phonecall(apartmentStore.lobbyTell, true)}
              >
                <View
                  style={{
                    backgroundColor: 'rgba(255,255,255,0.6)',
                    height: 60,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <Text style={{ color: primaryDark, fontSize: 18 }}>
                    تماس با لابی
                  </Text>
                </View>
              </TouchableOpacity>
            </Image>
          </View>
        </View>
      </View>
    );
  }

  onBackPress() {
    this.props.navigation.goBack();
  }
}
