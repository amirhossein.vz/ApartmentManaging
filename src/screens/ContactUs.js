import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
} from 'react-native';
import { phonecall } from 'react-native-communications';
import { MKTextField } from 'react-native-material-kit';
import { observer } from 'mobx-react/native';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import {
  primary,
  fabColor,
  primaryDark,
  placeholderTextColor,
  transparent,
} from '../constants/colors';
import {
  Toolbar,
  Fab,
  Overlay,
  LoadingPopUp,
  AndroidBackButton,
} from '../components';
import images from '@assets/images';
import { apartmentStore } from '../stores';
import {
  addContactUsQuery,
  deleteContactUsQuery,
  getContactUsQuery,
} from '../network/Queries';

class ContactUsCard extends Component {
  render() {
    const { Title_Name, Name, Tel } = this.props.item;
    return (
      <TouchableOpacity
        onLongPress={() => {
          if (
            apartmentStore.userRole == 'Admin' ||
            apartmentStore.userRole == 'Manager'
          ) {
            this.props.onLongPress(this.props.item);
          }
        }}
        style={{
          backgroundColor: 'white',
          marginHorizontal: 4,
          marginVertical: 2,
          padding: 8,
          flexDirection: 'row',
          alignItems: 'center',
        }}
      >
        <Image
          source={images.user_image}
          style={{
            height: 50,
            width: 50,
            resizeMode: 'contain',
            marginRight: 16,
            tintColor: placeholderTextColor,
          }}
        />
        <View style={{ flex: 1, alignItems: 'flex-start' }}>
          <Text style={{ fontSize: 16, color: 'black' }}>{Name}</Text>
          <Text>{Title_Name}</Text>
        </View>
        <TouchableOpacity
          style={{
            paddingVertical: 8,
            paddingHorizontal: 8,
            flexDirection: 'row',
            alignItems: 'center',
          }}
          onPress={() => phonecall(Tel, true)}
        >
          <Image
            source={images.telephone_icon}
            style={{
              tintColor: placeholderTextColor,
              width: 24,
              height: 24,
              resizeMode: 'contain',
              tintColor: primaryDark,
              marginRight: 8,
            }}
          />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  }
}

class DeletePopUp extends Component {
  render() {
    const { onConfirm, onDismiss, message } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          paddingTop: 16,
          paddingBottom: 8,
          paddingHorizontal: 8,
          width: 300,
          height: 120,
          justifyContent: 'space-between',
          borderRadius: 4,
        }}
      >
        <Text style={{ textAlign: 'left' }}>{message}</Text>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            onPress={onConfirm}
            style={{ paddingHorizontal: 16 }}
          >
            <Text style={{ color: primaryDark }}>تایید</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={onDismiss}
            style={{ paddingHorizontal: 16 }}
          >
            <Text style={{ color: primaryDark }}>انصراف</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

class PickerRow extends Component {
  render() {
    const {
      title,
      pickerData,
      pickedData,
      onPickedChange,
      onPressPicker,
    } = this.props;
    return (
      <View style={{ flexDirection: 'row', marginVertical: 4 }}>
        <Text
          style={{ color: 'black', fontSize: 14, flex: 1, textAlign: 'left' }}
        >
          {title}
        </Text>
        <View
          style={{
            flex: 1.3,
            alignItems: 'center',
            justifyContent: 'flex-start',
            flexDirection: 'row',
            elevation: 1,
            backgroundColor: 'white',
          }}
        >
          <Picker
            pickedData={pickedData}
            pickerData={pickerData}
            onPress={onPressPicker}
          />
        </View>
      </View>
    );
  }
}

class Picker extends Component {
  render() {
    const { pickerData, pickedData, onPress } = this.props;
    const picked = pickerData.find(item => item.value == pickedData);
    return (
      <TouchableOpacity
        style={{ flex: 1, flexDirection: 'row' }}
        onPress={onPress}
      >
        <View
          style={{
            flexDirection: 'row',
            elevation: 4,
            alignItems: 'center',
            backgroundColor: 'white',
            flex: 1,
            paddingVertical: 4,
            paddingHorizontal: 4,
          }}
        >
          <Image
            source={images.picker_icon}
            style={{
              height: 16,
              width: 16,
              tintColor: primaryDark,
              marginRight: 4,
            }}
          />
          <Text>{pickedData}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

class PickerPopUp extends Component {
  render() {
    const { pickerData, pickerTitle, onPress } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          borderRadius: 2,
          width: 250,
          height: 200 + (pickerData.length - 2) * 30,
          padding: 8,
        }}
      >
        <Text style={{ fontSize: 16, color: primaryDark, marginBottom: 8 }}>
          {pickerTitle}
        </Text>
        {pickerData.map(item => (
          <TouchableOpacity
            style={{ paddingVertical: 8, paddingHorizontal: 8 }}
            onPress={() => onPress(item)}
          >
            <Text style={{ color: 'black' }}>{item.Name}</Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  }
}

class AddContactUsPopUp extends Component {
  render() {
    const {
      onPressPicker,
      onConfirm,
      name,
      tel,
      onChangeTel,
      onChangeName,
      pickedData,
    } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 20,
          borderRadius: 4,
          width: 250,
        }}
      >
        <View style={{ padding: 16 }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginBottom: 16,
            }}
          >
            <Text style={{ marginRight: 16 }}>نام</Text>
            <MKTextField
              ref="textInput"
              multiline={false}
              keyboardType="default"
              returnKeyType="done"
              floatingLabelEnable={false}
              tintColor={placeholderTextColor}
              textInputStyle={{
                fontSize: 14,
                color: 'black',
                flexGrow: 1,
                textAlign: 'center',
              }}
              underlineSize={1}
              placeholder="امید عباسپور"
              style={{ width: 120 }}
              onChangeText={text => onChangeName(text)}
              highlightColor={primaryDark}
              value={name}
            />
          </View>
          <PickerRow
            title="عنوان"
            pickerData={apartmentStore.contactUsTypes}
            pickedData={pickedData}
            onPressPicker={() => onPressPicker()}
          />
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop: 16,
              justifyContent: 'space-between',
            }}
          >
            <Text style={{ marginRight: 16 }}>شماره تماس</Text>
            <MKTextField
              ref="textInput"
              multiline={false}
              keyboardType="numeric"
              returnKeyType="done"
              floatingLabelEnable={false}
              tintColor={placeholderTextColor}
              textInputStyle={{
                fontSize: 14,
                color: 'black',
                flexGrow: 1,
                textAlign: 'center',
              }}
              underlineSize={1}
              placeholder="۰۹۱۲ ***‌****"
              style={{ width: 120 }}
              onChangeText={text => onChangeTel(text)}
              highlightColor={primaryDark}
              value={tel}
            />
          </View>
        </View>
        <TouchableOpacity onPress={this.props.onConfirm}>
          <View style={{ flexDirection: 'row' }}>
            <View
              style={{
                flex: 1,
                height: 60,
                backgroundColor: primaryDark,
                alignItems: 'center',
                justifyContent: 'center',
                borderBottomLeftRadius: 4,
                borderBottomRightRadius: 4,
              }}
            >
              <Text style={{ fontSize: 18, color: 'white' }}>ذخیره</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

@observer
export default class ContactUs extends Component {
  componentWillMount() {
    getContactUsQuery(apartmentStore.apartmentID);
  }

  constructor() {
    super();
    this.state = {
      showOverlay: false,
      showPicker: false,
      showPopUp: false,
      showDeletePopUp: false,
      pickedData: 1,
      name: '',
      tel: '',
      nominatedToDeleteItem: null,
      loading: false,
      loadingMessage: '',
    };
  }

  render() {
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: 'ارتباط با ساختمان',
      },
      right: {
        onPress: () => this.onBackPress(),
      },
    };
    return (
      <View style={{ flex: 1 }}>
        <Toolbar customStyle={toolbarStyle} />
        <AndroidBackButton
          onPress={() => {
            if (this.state.loading) {
              return true;
            } else if (this.state.showPicker) {
              this.setState({ showPicker: false, showContactUsPopUp: true });
            } else if (this.state.showOverlay) {
              this.dismissPopUps();
            } else {
              this.onBackPress();
            }
            return true;
          }}
        />
        <FlatList
          data={apartmentStore.contactUs}
          renderItem={({ item }) => (
            <ContactUsCard
              item={item}
              onLongPress={item => this.onCardLongPress(item)}
            />
          )}
          style={{ marginVertical: 2 }}
          keyExtractor={(item, index) => index}
        />
        {(apartmentStore.userRole == 'Admin' ||
          apartmentStore.userRole == 'Manager') && (
          <View
            pointerEvents="box-none"
            style={{
              ...StyleSheet.absoluteFillObject,
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
              flex: 1,
              padding: 16,
            }}
          >
            <Fab
              bgColor={primaryDark}
              onPress={this.showContactUsPopUp.bind(this)}
              icon={images.plus_icon}
              iconColor="white"
            />
          </View>
        )}
        {this.state.showOverlay && (
          <Overlay catchTouch={true} onPress={this.dismissPopUps.bind(this)}>
            {this.state.showDeletePopUp ? (
              <DeletePopUp
                onConfirm={this.confirmDelete.bind(this)}
                onDismiss={this.dismissPopUps.bind(this)}
                message={`آیا از حذف ${this.state.nominatedToDeleteItem
                  .Name} مطمئن هستید؟ `}
              />
            ) : this.state.showContactUsPopUp ? (
              <View>
                <AddContactUsPopUp
                  onConfirm={this.saveContactUs.bind(this)}
                  name={this.state.name}
                  onChangeName={text => this.setState({ name: text })}
                  tel={this.state.tel}
                  onChangeTel={text => this.setState({ tel: text })}
                  onPressPicker={() =>
                    this.setState({
                      showContactUsPopUp: false,
                      showPicker: true,
                    })}
                  pickedData={
                    apartmentStore.contactUsTypes.find(
                      item => item.ID == this.state.pickedData,
                    ).Name
                  }
                />
                {Platform.OS == 'ios' && <KeyboardSpacer />}
              </View>
            ) : this.state.showPicker ? (
              <PickerPopUp
                pickerData={apartmentStore.contactUsTypes}
                pickerTitle="انتخاب عنوان"
                onPress={item =>
                  this.setState({
                    showPicker: false,
                    showContactUsPopUp: true,
                    pickedData: item.ID,
                  })}
              />
            ) : null}
          </Overlay>
        )}
        {this.state.loading && (
          <Overlay>
            <LoadingPopUp message={this.state.loadingMessage} />
          </Overlay>
        )}
      </View>
    );
  }

  onBackPress() {
    this.props.navigation.goBack();
  }

  showContactUsPopUp() {
    this.setState({
      showOverlay: true,
      showContactUsPopUp: true,
      showDeletePopUp: false,
    });
  }

  dismissPopUps() {
    this.setState({
      showOverlay: false,
      showContactUsPopUp: false,
      showPicker: false,
      showDeletePopUp: false,
    });
  }

  onCardLongPress(item) {
    this.setState({
      showOverlay: true,
      showDeletePopUp: true,
      nominatedToDeleteItem: item,
    });
  }

  async confirmDelete() {
    this.setState({ loading: true, loadingMessage: 'در حال حذف ...' });
    const { ID } = this.state.nominatedToDeleteItem;

    const removeContactUsRes = await deleteContactUsQuery(ID);
    if (removeContactUsRes) {
      this.setState({
        showOverlay: false,
        showPicker: false,
        showPopUp: false,
        showDeletePopUp: false,
        nominatedToDeleteItem: null,
        pickedData: 1,
        name: '',
        tel: '',
      });
    } else {
      //TODO : show proper erro later
    }
    this.setState({ loading: false, loadingMessage: 'در حال حذف ...' });
  }

  async saveContactUs() {
    Keyboard.dismiss();
    this.setState({ loading: true, loadingMessage: 'در حال ذخیره ...' });
    const data = {
      apartment_ID: apartmentStore.apartmentID,
      title_ID: this.state.pickedData,
      name: this.state.name,
      tel: this.state.tel,
    };

    const addContactUsRes = await addContactUsQuery(data);
    if (addContactUsRes) {
      this.setState({
        showOverlay: false,
        showPicker: false,
        showPopUp: false,
        showDeletePopUp: false,
        nominatedToDeleteItem: null,
        pickedData: 1,
        name: '',
        tel: '',
      });
    } else {
      //TODO : show proper erro later
    }
    this.setState({ loading: false, loadingMessage: 'در حال ذخیره ...' });
  }
}

const styles = StyleSheet.create({
  textInput: {
    paddingHorizontal: 4,
    borderColor: primaryDark,
    borderWidth: 0.5,
    height: 200,
    width: 240,
    borderRadius: 2,
    fontSize: 14,
    textAlignVertical: 'top',
    textAlign: 'right',
  },
});
