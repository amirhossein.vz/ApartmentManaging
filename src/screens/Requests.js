import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Keyboard,
  ToastAndroid,
  Dimensions,
  FlatList,
} from 'react-native';
import { MKTextField } from 'react-native-material-kit';
import { observer } from 'mobx-react/native';
var moment = require('moment-jalaali');
const PersianCalendarPicker = require('react-native-persian-calendar-picker');
import DateTimePicker from 'react-native-modal-datetime-picker';

import {
  Toolbar,
  Overlay,
  LoadingPopUp,
  AndroidBackButton,
} from '../components';
import {
  primaryDark,
  primary,
  placeholderTextColor,
  transparent,
} from '../constants/colors';
import images from '@assets/images';
import { apartmentStore } from '../stores';
import { dayOfWeek, monthOfYear, ownerDataTypes } from '../constants/values';
import { getRequestQuery, addRequestQuery } from '../network/Queries';
import { parseTimeToString } from '../utils';

class RequestCardRow extends Component {
  render() {
    return (
      <View
        style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 8 }}
      >
        <Image
          source={this.props.icon}
          style={{
            width: 18,
            height: 18,
            tintColor: primaryDark,
            marginRight: 4,
          }}
        />
        <Text style={{ flex: 1, fontSize: 12 }}>{this.props.title}</Text>
        <View style={{ flex: 2, flexDirection: 'row' }}>
          <Text
            style={{
              color: this.props.color ? this.props.color : 'black',
              fontSize: 12,
              textAlign: 'left',
            }}
          >
            {this.props.description}
          </Text>
          {this.props.price && (
            <Text
              style={{
                color: 'black',
                fontSize: 12,
                textAlign: 'left',
              }}
            >
              {' '}
              تومان
            </Text>
          )}
        </View>
      </View>
    );
  }
}

class ConfirmPopUp extends Component {
  constructor() {
    super();
    this.state = {
      price: '',
    };
  }
  render() {
    const { confirm, dismiss, item, onReject, haveEmptyField } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          paddingTop: 16,
          paddingBottom: 8,
          paddingHorizontal: 8,
          width: 300,
          height: 180,
          justifyContent: 'space-between',
          borderRadius: 4,
        }}
      >
        <View>
          <Text style={{ textAlign: 'left', marginBottom: 8 }}>
            آیا از تایید درخواست مطمئن هستید؟
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginBottom: 4,
            }}
          >
            <Text style={{ textAlign: 'left', flex: 1 }}>عنوان :‌</Text>
            <Text
              style={{
                textAlign: 'left',
                flex: 2,
                color: 'black',
              }}
            >
              {item.Title}
            </Text>
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Text style={{ textAlign: 'left', flex: 1 }}>تعیین قیمت :</Text>

            <MKTextField
              ref={'textInput'}
              multiline={false}
              keyboardType={'default'}
              returnKeyType={'done'}
              floatingLabelEnable={false}
              tintColor={
                this.props.haveEmptyField && this.state.price == ''
                  ? 'red'
                  : placeholderTextColor
              }
              textInputStyle={{
                color: 'black',
                fontSize: 14,
                textAlign: 'center',
              }}
              underlineSize={1}
              placeholder={'۱۰۰۰۰'}
              style={{ flex: 2 }}
              onChangeText={text => this.setState({ price: text })}
              highlightColor={primary}
              value={this.state.price}
              onSubmitEditing={() => Keyboard.dismiss()}
            />
          </View>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            onPress={() => confirm(this.state.price)}
            style={{ paddingHorizontal: 16 }}
          >
            <Text style={{ color: primaryDark }}>تایید</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={dismiss} style={{ paddingHorizontal: 8 }}>
            <Text style={{ color: primaryDark }}>انصراف</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={onReject}
            style={{ marginHorizontal: 32, paddingHorizontal: 32 }}
          >
            <Text style={{ color: primaryDark }}>رد درخواست</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

class RequestCard extends Component {
  render() {
    const {
      RequestDateTimeFrom,
      RequestDatetimeTo,
      HasConfirmed,
      ConfirmedDatetitme,
      HasPaid,
      IsManager,
      UnitNumber,
      Name,
      Status,
      Description,
      Title,
      Price,
    } = this.props.request;
    return (
      <TouchableOpacity
        onLongPress={() => this.props.onLongPress(this.props.request)}
      >
        <View
          style={{
            marginVertical: 4,
            marginHorizontal: 8,
            backgroundColor: 'white',
            elevation: 2,
            paddingHorizontal: 8,
            paddingVertical: 16,
          }}
        >
          <RequestCardRow
            title="عنوان"
            description={Title}
            icon={images.request_icon}
          />
          <RequestCardRow
            title="درخواست‌کننده"
            description={` ${Name} ${ownerDataTypes.find(
              item => item.ID == Status,
            ).Name} واحد ${UnitNumber}  `}
            icon={images.user_icon_filled}
          />
          {Price && (
            <RequestCardRow
              title="قیمت"
              description={Price}
              price={true}
              icon={images.money_icon}
            />
          )}
          <RequestCardRow
            title="تاریخ رزرو"
            description={moment(
              RequestDateTimeFrom,
              'YYYY-M-D HH:mm:ss',
            ).format('jYYYY/jM/jD')}
            icon={images.calendar_icon}
          />
          <RequestCardRow
            title="از ساعت"
            description={moment(
              RequestDateTimeFrom,
              'YYYY-M-D HH:mm:ss',
            ).format('HH:mm')}
            icon={images.time_start_icon}
          />
          <RequestCardRow
            title="تا ساعت"
            description={moment(RequestDatetimeTo, 'YYYY-M-D HH:mm:ss').format(
              'HH:mm',
            )}
            icon={images.time_end_icon}
          />
          <RequestCardRow
            title="وضعیت "
            description={
              HasConfirmed && HasPaid
                ? 'پرداخت‌شده'
                : HasConfirmed ? 'تائید شده' : 'در انتظار تائید'
            }
            color={HasConfirmed || HasPaid ? 'green' : 'red'}
            icon={images.status_icon}
          />
          <View style={{ height: 1, backgroundColor: placeholderTextColor }} />
          <Text style={{ fontSize: 12 }}>{Description}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default class Requests extends Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      nominatedItem: null,
      showOverlay: false,
      haveEmptyField: false,
    };
  }

  componentWillMount() {
    getRequestQuery(apartmentStore.apartmentID);
  }

  render() {
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: 'پیگیری درخواست‌ها',
      },
    };
    return (
      <View style={{ flex: 1 }}>
        <Toolbar customStyle={toolbarStyle} />
        <AndroidBackButton
          onPress={() => {
            if (this.state.loading) {
              return true;
            } else if (this.state.showOverlay) {
              this.dismissAll();
            } else {
              this.onBackPress();
            }
            return true;
          }}
        />
        <FlatList
          keyExtractor={(item, index) => index}
          data={apartmentStore.requests}
          renderItem={({ item }) => (
            <RequestCard
              request={item}
              onLongPress={item => this.onCardLongPress(item)}
            />
          )}
        />
        {this.state.showOverlay && (
          <Overlay onPress={() => this.dismissSavePanel()} catchTouch={true}>
            {apartmentStore.userRole == 'Admin' ||
              (apartmentStore.userRole == 'Manager' && (
                <ConfirmPopUp
                  confirm={price => this.onConfirm(price)}
                  dismiss={() => this.dismissSavePanel()}
                  item={this.state.nominatedItem}
                  onReject={() => this.onReject()}
                  haveEmptyField={this.state.haveEmptyField}
                />
              ))}
          </Overlay>
        )}
        {this.state.loading && (
          <Overlay>
            <LoadingPopUp message="در حال ذخیره تغییرات ..." />
          </Overlay>
        )}
      </View>
    );
  }

  onBackPress() {
    this.props.navigation.goBack();
  }

  dismissSavePanel() {
    this.setState({
      showOverlay: false,
      loading: false,
      nominatedItem: null,
    });
  }

  onCardLongPress(item) {
    if (
      apartmentStore.userRole == 'Admin' ||
      apartmentStore.userRole == 'Manager'
    ) {
      if (item.HasConfirmed) {
        return;
      }
      this.setState({
        showOverlay: true,
        nominatedItem: item,
      });
    }
  }

  async onConfirm(price) {
    console.warn(JSON.stringify(this.state.nominatedItem));
    if (price == '') {
      this.setState({ haveEmptyField: true });
      return;
    }
    console.warn(price);
    this.setState({ loading: true });
    const { ID, Description } = this.state.nominatedItem;
    const newItem = {
      id: ID,
      hasConfirmed: true,
      description: Description,
      price: price,
    };
    const addRequestRes = await addRequestQuery(newItem);
    if (addRequestRes) {
      this.setState({
        loading: false,
        nominatedItem: null,
        showOverlay: false,
        haveEmptyField: false,
      });
    } else {
      //TODO : show proper error
    }
    this.setState({ loading: false });
  }

  async onReject() {
    this.setState({ loading: true });
    const { ID, Description } = this.state.nominatedItem;
    const newItem = {
      id: ID,
      description: Description,
      isDisabled: true,
      apartment_ID: apartmentStore.apartmentID,
    };
    const addRequestRes = await addRequestQuery(newItem);
    if (addRequestRes) {
      this.setState({
        loading: false,
        nominatedItem: null,
        showOverlay: false,
        haveEmptyField: false,
      });
    } else {
      //TODO : show proper error
    }
    this.setState({ loading: false });
  }
}
