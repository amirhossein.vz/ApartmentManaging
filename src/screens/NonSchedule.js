import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Keyboard,
  ToastAndroid,
  Dimensions,
} from 'react-native';
import { MKTextField } from 'react-native-material-kit';
import { observer } from 'mobx-react/native';
var moment = require('moment-jalaali');
const PersianCalendarPicker = require('react-native-persian-calendar-picker');
import DateTimePicker from 'react-native-modal-datetime-picker';

import {
  Toolbar,
  Overlay,
  LoadingPopUp,
  AndroidBackButton,
} from '../components';
import {
  primaryDark,
  primary,
  placeholderTextColor,
  transparent,
} from '../constants/colors';
import images from '@assets/images';
import { apartmentStore } from '../stores';
import { dayOfWeek, monthOfYear } from '../constants/values';
import { addRequestQuery } from '../network/Queries';
import { parseTimeToString } from '../utils';

class NonScheduleRow extends Component {
  render() {
    const { title, data, onPress, dataImage } = this.props;
    return (
      <TouchableOpacity onPress={onPress}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginBottom: 16,
          }}
        >
          <Text style={{ flex: 1, textAlign: 'left' }}>{title}</Text>
          <View style={{ flex: 2 }}>
            {!!data ? (
              <Text style={{ textAlign: 'left', color: 'black', fontSize: 16 }}>
                {data}
              </Text>
            ) : (
              <Image
                source={dataImage}
                style={{ height: 24, width: 24, tintColor: primaryDark }}
              />
            )}
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

@observer
export default class NonSchedule extends Component {
  constructor() {
    super();
    this.state = {
      date: null,
      fromHour: null,
      toHour: null,
      description: '',
      loading: false,

      showDayPicker: false,
      loading: false,
      showTimePicker: false,
    };
  }

  render() {
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: `درخواست رزور ${this.props.navigation.state.params.title}`,
      },
    };

    let initialDate = new Date();
    initialDate.setHours(0);
    initialDate.setMinutes(0);
    const jMoment = moment(this.state.date);
    return (
      <View style={{ flex: 1 }}>
        <DateTimePicker
          isVisible={this.state.showTimePicker}
          onConfirm={time => this.onConfirmTime(time)}
          onCancel={() => this.dismissAll()}
          mode="time"
          is24Hour={true}
          date={initialDate}
        />
        <Toolbar customStyle={toolbarStyle} />
        <AndroidBackButton
          onPress={() => {
            if (this.state.loading) {
              return true;
            } else if (this.state.showOverlay) {
              this.dismissAll();
            } else {
              this.onBackPress();
            }
            return true;
          }}
        />
        <View style={{ flex: 1 }}>
          <View
            style={{
              marginHorizontal: 4,
              marginVertical: 4,
              elevation: 2,
              backgroundColor: 'white',
              paddingVertical: 16,
              paddingHorizontal: 8,
            }}
          >
            <NonScheduleRow
              title="تاریخ رزور"
              data={
                this.state.date
                  ? `${dayOfWeek.find(
                      item => item.english == jMoment.format('dddd'),
                    ).label} ${jMoment.jDate()} ${monthOfYear[jMoment.jMonth()]
                      .label} ماه`
                  : null
              }
              onPress={() =>
                this.setState({
                  showDayPicker: true,
                  loading: false,
                  showTimePicker: false,
                })}
              dataImage={images.calendar_icon}
            />
            <NonScheduleRow
              title="از ساعت"
              data={
                this.state.fromHour
                  ? parseTimeToString(this.state.fromHour)
                  : null
              }
              onPress={() =>
                this.setState({
                  showDayPicker: false,
                  loading: false,
                  showTimePicker: true,
                  timePickerType: 'FROM',
                })}
              dataImage={images.time_start_icon}
            />
            <NonScheduleRow
              title="تا ساعت"
              data={
                this.state.toHour ? parseTimeToString(this.state.toHour) : null
              }
              onPress={() =>
                this.setState({
                  showDayPicker: false,
                  loading: false,
                  showTimePicker: true,
                  timePickerType: 'TO',
                })}
              dataImage={images.time_end_icon}
            />

            <Text style={{ textAlign: 'left', marginBottom: 16 }}>توضیحات</Text>
            <View style={{ flexDirection: 'row' }}>
              <MKTextField
                ref={'textInput'}
                multiline={false}
                keyboardType={'default'}
                returnKeyType={'done'}
                floatingLabelEnable={false}
                tintColor={placeholderTextColor}
                textInputStyle={{
                  color: 'black',
                  fontSize: 14,
                  textAlign: 'center',
                }}
                underlineSize={1}
                placeholder={'توضیحات را در این‌جا وارد کنید'}
                style={{ flex: 1 }}
                onChangeText={text => this.setState({ description: text })}
                highlightColor={primary}
                value={this.state.description}
                onSubmitEditing={() => Keyboard.dismiss()}
              />
            </View>
          </View>
        </View>
        <TouchableOpacity onPress={() => this.onReserveFacility()}>
          <View
            style={{
              backgroundColor: primaryDark,
              height: 48,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Text style={{ color: 'white', fontSize: 16 }}>
              رزرو امکانات رفاهی
            </Text>
          </View>
        </TouchableOpacity>
        {this.state.showDayPicker && (
          <Overlay catchTouch={true} onPress={this.dismissAll.bind(this)}>
            <View style={{ backgroundColor: 'white', elevation: 4 }}>
              <PersianCalendarPicker
                selectedDate={this.state.date ? this.state.date : new Date()}
                onDateChange={this.onDateChange.bind(this)}
                screenWidth={Dimensions.get('window').width}
                selectedBackgroundColor={'#5ce600'}
              />
            </View>
          </Overlay>
        )}

        {this.state.loading && (
          <Overlay catchTouch={true}>
            <LoadingPopUp message="در حال رزرو امکانات رفاهی ..." />
          </Overlay>
        )}
      </View>
    );
  }

  async onReserveFacility() {
    this.setState({ loading: true });
    let reserveDate = this.state.date;
    reserveDate.setHours(6);
    Keyboard.dismiss();
    const newItem = {
      apartment_ID: apartmentStore.apartmentID,
      facility_ID: this.props.navigation.state.params.id,
      requestDate: reserveDate,
      fromTime: parseTimeToString(this.state.fromHour),
      toTime: parseTimeToString(this.state.toHour),
      hasConfirmed: false,
      description: this.state.description,
    };
    const addRequestRes = await addRequestQuery(newItem);
    if (addRequestRes) {
      this.props.navigation.navigate('Requests');
    } else {
      //TODO : show proper error
    }
    this.setState({ loading: false });
  }

  onBackPress() {
    this.props.navigation.goBack();
  }

  dismissAll() {
    this.setState({
      showDayPicker: false,
      loading: false,
      showTimePicker: false,
    });
  }

  async onDateChange(date) {
    await this.setState({
      date: date,
      showDayPicker: false,
      loading: false,
      showTimePicker: false,
    });
  }

  onConfirmTime(time) {
    if (this.state.timePickerType == 'FROM') {
      this.setState({
        fromHour: time,
      });
    } else if ((this.state.timePickerType = 'TO')) {
      this.setState({ toHour: time });
    }

    this.dismissAll();
  }
}
