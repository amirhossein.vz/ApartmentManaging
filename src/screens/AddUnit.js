import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  Dimensions,
  Keyboard,
  ToastAndroid,
  Platform,
} from 'react-native';
import { MKTextField } from 'react-native-material-kit';
import { observer } from 'mobx-react/native';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import {
  CustomTextInput,
  CardView,
  Toolbar,
  Overlay,
  LoadingPopUp,
  AndroidBackButton,
} from '../components';
import {
  primaryDark,
  primary,
  placeholderTextColor,
  transparent,
} from '../constants/colors';
import images from '@assets/images';
import { apartmentStore } from '../stores';
import { addUnitQuery } from '../network/Queries';

class FormUnit extends Component {
  state = { value: '' };

  render() {
    const {
      essentail,
      title,
      placeholder,
      onChangeText,
      keyboardType,
      returnKeyType,
      value,
      isEmpty,
    } = this.props;
    return (
      <View
        style={{
          flexDirection: 'row',
          marginVertical: 3,
          alignItems: 'flex-start',
        }}
      >
        <View style={{ flexDirection: 'row', flex: 1, alignItems: 'flex-end' }}>
          {essentail ? (
            <Text style={{ color: primaryDark, marginRight: 4 }}>*</Text>
          ) : (
            <Text style={{ color: transparent, marginRight: 4 }}>*</Text>
          )}
          <Text
            style={{ color: 'black', fontSize: 14, flex: 1, textAlign: 'left' }}
          >
            {title}
          </Text>
        </View>

        <MKTextField
          ref={'textInput'}
          multiline={false}
          keyboardType={keyboardType}
          returnKeyType={returnKeyType}
          floatingLabelEnable={false}
          tintColor={
            isEmpty && essentail && value == '' ? 'red' : placeholderTextColor
          }
          textInputStyle={{
            color: 'black',
            fontSize: 14,
            textAlign: 'center',
          }}
          underlineSize={1}
          placeholder={placeholder}
          style={{ flex: 1 }}
          onChangeText={text => this.props.onChangeText(text)}
          highlightColor={primary}
          value={value}
        />
      </View>
    );
  }
}

class CheckableRow extends Component {
  render() {
    const { text, state, onCheck } = this.props;
    return (
      <TouchableOpacity onPress={onCheck}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={{ flex: 1, textAlign: 'left' }}>{text}</Text>
          <Image
            source={state ? images.checked_icon : images.unchecked_icon}
            style={{ height: 18, width: 18, tintColor: primary }}
          />
        </View>
      </TouchableOpacity>
    );
  }
}

@observer
export default class AddUnit extends Component {
  constructor() {
    super();
    this.state = {
      editMode: false,
      unitNumber: '',
      floorNumber: '',
      area: '',
      parkingCount: '',
      parkingNumber: '',
      ownerName: '',
      ownerTel: '',
      isEmpty: false,
      isSame: false,
      tenantName: '',
      tenantTel: '',
      tel: '',
      numberOfPeople: '',
      ID: null,
      loading: false,
      isThereEmptyField: false,
    };
  }

  componentWillMount() {
    if (
      this.props.navigation &&
      this.props.navigation.state &&
      this.props.navigation.state.params &&
      this.props.navigation.state.params.unit
    ) {
      const {
        UnitNumber,
        FloorNumber,
        OwnerName,
        OwnerTel,
        TenantName,
        TenantTel,
        Area,
        ParkingCount,
        ParkingNumber,
        IsEmpty,
        IsSame,
        NumberOfPeople,
        Tel,
        ID,
      } = this.props.navigation.state.params.unit;
      this.setState({
        unitNumber: UnitNumber,
        floorNumber: FloorNumber.toString(),
        area: Area.toString(),
        parkingCount: ParkingCount.toString(),
        parkingNumber: ParkingNumber,
        ownerName: OwnerName,
        ownerTel: OwnerTel,
        isEmpty: IsEmpty,
        isSame: IsSame,
        tenantName: TenantName,
        tenantTel: TenantTel,
        tel: Tel,
        numberOfPeople: NumberOfPeople.toString(),
        editMode: true,
        ID: ID,
      });
    }
  }

  render() {
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: 'ایجاد واحد جدید',
      },
    };
    const {
      unitNumber,
      floorNumber,
      area,
      parkingCount,
      parkingNumber,
      ownerName,
      ownerTel,
      isEmpty,
      isSame,
      tenantName,
      tenantTel,
      tel,
      numberOfPeople,
    } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <Toolbar customStyle={toolbarStyle} />
        <AndroidBackButton
          onPress={() => {
            if (this.state.loading) {
              return true;
            } else {
              this.onBackPress();
            }
            return true;
          }}
        />
        <ScrollView>
          <CardView>
            <FormUnit
              essentail={true}
              title="شماره واحد"
              placeholder="۲"
              onChangeText={text => this.setState({ unitNumber: text })}
              value={this.state.unitNumber}
              keyboardType="numeric"
              returnKeyType="next"
              isEmpty={this.state.isThereEmptyField}
            />
            <FormUnit
              essentail={true}
              title="طبقه"
              placeholder="۱"
              onChangeText={text => this.setState({ floorNumber: text })}
              value={floorNumber}
              keyboardType="numeric"
              returnKeyType="next"
              isEmpty={this.state.isThereEmptyField}
            />
            <FormUnit
              essentail={true}
              title="متراژ"
              placeholder="۱۲۰"
              onChangeText={text => this.setState({ area: text })}
              value={area}
              onChangeText={text => this.setState({ area: text })}
              keyboardType="numeric"
              isEmpty={this.state.isThereEmptyField}
              returnKeyType="next"
            />
            <FormUnit
              essentail={true}
              title="تعداد پارکینگ"
              placeholder="۱"
              onChangeText={text => this.setState({ parkingCount: text })}
              value={parkingCount}
              keyboardType="numeric"
              returnKeyType="next"
              isEmpty={this.state.isThereEmptyField}
            />
            <FormUnit
              essentail={false}
              title="شماره پارکینگ"
              placeholder="۴"
              onChangeText={text => this.setState({ parkingNumber: text })}
              value={parkingNumber}
              keyboardType="default"
              returnKeyType="next"
            />
          </CardView>

          <CardView>
            <FormUnit
              essentail={true}
              title="نام مالک"
              placeholder="امید عباسپور"
              onChangeText={text => this.setState({ ownerName: text })}
              value={ownerName}
              keyboardType="default"
              returnKeyType="next"
              isEmpty={this.state.isThereEmptyField}
            />
            <FormUnit
              essentail={true}
              title="موبایل مالک"
              placeholder="۰۹۱۲ *****"
              onChangeText={text => this.setState({ ownerTel: text })}
              value={ownerTel}
              keyboardType="numeric"
              returnKeyType="next"
              isEmpty={this.state.isThereEmptyField}
            />
          </CardView>
          <CardView>
            <Text style={{ flex: 1, textAlign: 'left' }}> وضعیت سکونت</Text>
            <CheckableRow
              text="خالی از سکنه"
              state={isEmpty}
              onCheck={this.changeEmptyState.bind(this, !isEmpty)}
            />
            <CheckableRow
              text="مالک و ساکن یکی هستند"
              state={isSame}
              onCheck={this.changeSameState.bind(this, !isSame)}
            />
          </CardView>
          {!isEmpty && (
            <CardView>
              {!isSame && (
                <View>
                  <FormUnit
                    essentail={true}
                    title="نام ساکن"
                    placeholder="سالار عباسی"
                    onChangeText={text => this.setState({ tenantName: text })}
                    value={tenantName}
                    keyboardType="default"
                    returnKeyType="next"
                    isEmpty={this.state.isThereEmptyField}
                  />
                  <FormUnit
                    essentail={true}
                    title="موبایل ساکن"
                    placeholder="۰۹۱۴ *****"
                    onChangeText={text => this.setState({ tenantTel: text })}
                    value={tenantTel}
                    keyboardType="numeric"
                    returnKeyType="next"
                    isEmpty={this.state.isThereEmptyField}
                  />
                </View>
              )}
              <FormUnit
                essentail={false}
                title="تلفن ثابت"
                placeholder="۰۲۱ ۶۶**‌****"
                onChangeText={text => this.setState({ tel: text })}
                value={tel}
                keyboardType="numeric"
                returnKeyType="next"
                isEmpty={this.state.isThereEmptyField}
              />
              <FormUnit
                essentail={true}
                title="تعداد خانوار"
                placeholder="۴"
                onChangeText={text => this.setState({ numberOfPeople: text })}
                value={numberOfPeople}
                keyboardType="numeric"
                returnKeyType="next"
                isEmpty={this.state.isThereEmptyField}
              />
            </CardView>
          )}
        </ScrollView>
        <TouchableOpacity onPress={this.saveUnit.bind(this)}>
          <View style={{ flexDirection: 'row' }}>
            <View
              style={{
                flex: 1,
                height: 60,
                backgroundColor: primary,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Text style={{ fontSize: 18, color: 'white' }}>ذخیره</Text>
            </View>
          </View>
        </TouchableOpacity>
        {Platform.OS == 'ios' && <KeyboardSpacer />}
        {this.state.loading && (
          <Overlay>
            <LoadingPopUp message="در حال ذخیره واحد جدید ... " />
          </Overlay>
        )}
      </View>
    );
  }

  onBackPress() {
    this.props.navigation.goBack();
  }

  async saveUnit() {
    Keyboard.dismiss();
    this.setState({ isThereEmptyField: false });
    if (this.state.isEmpty) {
      if (
        this.state.unitNumber == '' ||
        this.state.floorNumber == '' ||
        this.state.area == '' ||
        this.state.parkingCount == '' ||
        this.state.ownerName == '' ||
        this.state.ownerTel == ''
      ) {
        this.setState({ isThereEmptyField: true });
        ToastAndroid.show(
          'پر کردن همه فیلدهای اجباری ضروری است',
          ToastAndroid.LONG,
        );
        return;
      }
    } else if (this.state.isSame) {
      if (
        this.state.unitNumber == '' ||
        this.state.floorNumber == '' ||
        this.state.area == '' ||
        this.state.parkingCount == '' ||
        this.state.ownerName == '' ||
        this.state.ownerTel == '' ||
        this.state.numberOfPeople == ''
      ) {
        this.setState({ isThereEmptyField: true });
        ToastAndroid.show(
          'پر کردن همه فیلدهای اجباری ضروری است',
          ToastAndroid.LONG,
        );
        return;
      }
    } else {
      if (
        this.state.unitNumber == '' ||
        this.state.floorNumber == '' ||
        this.state.area == '' ||
        this.state.parkingCount == '' ||
        this.state.ownerName == '' ||
        this.state.ownerTel == '' ||
        this.state.numberOfPeople == '' ||
        this.state.tenantName == '' ||
        this.state.tenantTel == ''
      ) {
        this.setState({ isThereEmptyField: true });
        ToastAndroid.show(
          'پر کردن همه فیلدهای اجباری ضروری است',
          ToastAndroid.LONG,
        );
        return;
      }
    }
    this.setState({ loading: true });
    let data = {
      unitNumber: this.state.unitNumber,
      floorNumber: this.state.floorNumber,
      area: this.state.area,
      parkingCount: this.state.parkingCount,
      parkingNumber: this.state.parkingNumber,
      ownerName: this.state.ownerName,
      ownerTel: this.state.ownerTel,
      isEmpty: this.state.isEmpty,
      isSame: this.state.isSame,
      tel: this.state.tel,
      numberOfPeople: this.state.numberOfPeople,
      apartment_ID: apartmentStore.apartmentID,
      ownerPassword: this.state.ownerTel, // TODO: what is this?
      ownerSalt: 'aijaf', // TODO: what is this?
    };
    if (!this.state.isSame) {
      data = Object.assign(data, {
        tenantName: this.state.tenantName,
        tenantTel: this.state.tenantTel,
        tenantPassword: this.state.tenantTel,
        TenantSalt: 'safafakfa',
      });
    }
    if (this.state.isEmpty) {
      data.numberOfPeople = 0;
    }
    let addUnitRes;
    if (this.state.editMode) {
      addUnitRes = await addUnitQuery(
        Object.assign(data, { id: this.state.ID }),
      );
    } else {
      addUnitRes = await addUnitQuery(data);
    }
    this.setState({ loading: false });
    if (await addUnitRes) {
      this.onBackPress();
    } else {
      //TODO: show error later
    }
  }

  changeEmptyState(state) {
    if (state) {
      this.setState({ isEmpty: true, isSame: false });
    } else {
      this.setState({ isEmpty: false, isSame: false });
    }
  }

  changeSameState(state) {
    if (state) {
      this.setState({ isEmpty: false, isSame: true });
    } else {
      this.setState({ isEmpty: false, isSame: false });
    }
  }
}
