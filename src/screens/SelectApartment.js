import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  Keyboard,
  Dimensions,
  Platform,
} from 'react-native';
import { observer } from 'mobx-react/native';
import _ from 'lodash';
import KeyboardSpacer from 'react-native-keyboard-spacer';

const { height, width } = Dimensions.get('window');
import {
  primary,
  fabColor,
  primaryDark,
  placeholderTextColor,
  transparent,
} from '../constants/colors';
import {
  CostCard,
  Toolbar,
  Fab,
  Overlay,
  LoadingPopUp,
  AndroidBackButton,
} from '../components';
import images from '@assets/images';
import { apartmentStore } from '../stores';
import { getAllApartmentQuery } from '../network/Queries';

@observer
export default class Rules extends Component {
  state = { loading: false };

  componentWillMount() {
    getAllApartmentQuery();
  }

  render() {
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: 'انتخاب آپارتمان ',
      },
      right: {
        onPress: () => this.onBackPress(),
      },
    };
    return (
      <View style={{ flex: 1 }}>
        <Toolbar customStyle={toolbarStyle} />
        <FlatList
          keyExtractor={(item, index) => index}
          data={apartmentStore.allApartments}
          renderItem={({ item }) => (
            <TouchableOpacity onPress={item => this.selectApartment(item)}>
              <View style={{ margin: 4, padding: 16 }}>
                <Text>{item.Name}</Text>
              </View>
            </TouchableOpacity>
          )}
        />
        {this.state.loading && (
          <Overlay>
            <LoadingPopUp message="در حال اتصال به آپارتمان مورد نظر" />
          </Overlay>
        )}
      </View>
    );
  }

  onBackPress() {
    return;
  }

  async selectApartment(item) {
    apartmentStore.apartmentID = item.ID;

    this.setState({ loading: true });

    const apartmentDataRes = await apartmentDataQuery();
    const phoneBookRes = await phoneBookQuery(apartmentStore.apartmentID);
    const facilityRes = await getFacilityQuery(apartmentStore.apartmentID);
    const suggestionRes = await getSuggestionQuery(apartmentStore.apartmentID);
    const ruleRes = await getRuleQuery(apartmentStore.apartmentID);
    const notifRes = await getNotificationQuery();
    const contactUsRes = await getContactUsQuery(apartmentStore.apartmentID);
    const costRes = await getCostQuery(apartmentStore.apartmentID);
    const noticeRes = await getNoticeBoard(apartmentStore.apartmentID);
    const surveyRes = await getSurveyQuery(apartmentStore.apartmentID);
    const costTypeRes = await getCostTypeQuery();
    const invoiceRes = await getInvoiceQuery();
    const facilityTypesRes = await getFacilityTitles();
    const contactUsTypesRes = await getContactUsTypesQuery();
    const requestRes = await getRequestQuery(apartmentStore.apartmentID);
    const unitRes = await unitsQuery(apartmentStore.apartmentID);

    this.props.navigation.navigate('Main');
    this.setState({ loading: false });
  }
}

const styles = StyleSheet.create({
  textInput: {
    paddingHorizontal: 4,
    borderColor: primaryDark,
    borderWidth: 0.5,
    height: 200,
    width: 240,
    borderRadius: 2,
    fontSize: 14,
    textAlignVertical: 'top',
    textAlign: 'right',
  },
});
