import React, { Component } from 'react';
import {
  View,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  Keyboard,
  Platform,
} from 'react-native';
import { observer } from 'mobx-react/native';
import { MKTextField } from 'react-native-material-kit';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import {
  InformCard,
  Toolbar,
  Fab,
  AddMessagePopUp,
  Overlay,
  SurveyCard,
  LoadingPopUp,
  AndroidBackButton,
} from '../components';
import images from '@assets/images';
import { primaryDark, placeholderTextColor } from '../constants/colors';
const BORDER_RADIUS = 20;
import { addNoticeBoardQuery } from '../network/Queries';
import { apartmentStore } from '../stores';
import { submitSurveyQuery, submitSurveyResultQuery } from '../network/Queries';

class DeletePopUp extends Component {
  render() {
    const { confirm, dismiss, title } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          paddingTop: 16,
          paddingBottom: 8,
          paddingHorizontal: 8,
          width: 300,
          height: 120,
          justifyContent: 'space-between',
          borderRadius: 4,
        }}
      >
        <Text style={{ textAlign: 'left' }}>
          آیا از حذف "{title}" مطمئن هستید؟
        </Text>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity onPress={confirm} style={{ paddingHorizontal: 16 }}>
            <Text style={{ color: primaryDark }}>تایید</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={dismiss} style={{ paddingHorizontal: 16 }}>
            <Text style={{ color: primaryDark }}>انصراف</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

class SubmitOpinionPopUp extends Component {
  render() {
    const { confirm, dismiss, title } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          paddingTop: 16,
          paddingBottom: 8,
          paddingHorizontal: 8,
          width: 300,
          height: 120,
          justifyContent: 'space-between',
          borderRadius: 4,
        }}
      >
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text style={{ marginRight: 16, textAlign: 'left' }}>نظر شما:</Text>
          <Text
            style={{ color: 'black', fontSize: 16, flex: 1, textAlign: 'left' }}
          >
            {title}
          </Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity onPress={confirm} style={{ paddingHorizontal: 16 }}>
            <Text style={{ color: primaryDark }}>تایید</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={dismiss} style={{ paddingHorizontal: 16 }}>
            <Text style={{ color: primaryDark }}>انصراف</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

function Button({ text, onClick, isSelected, isRight }) {
  return (
    <View
      style={[
        styles.button,
        isSelected ? styles.selectedButton : styles.unselecetedButton,
        isRight ? styles.rightStyle : styles.leftStyle,
      ]}
    >
      <TouchableOpacity onPress={onClick} style={{ alignItems: 'center' }}>
        <Text
          style={[
            styles.text,
            isSelected ? styles.selectedText : styles.unselectedText,
          ]}
        >
          {text}
        </Text>
      </TouchableOpacity>
    </View>
  );
}

class TypeSelector extends Component {
  render() {
    const { setSwitchValue, switchValue } = this.props;
    return (
      <View
        style={{
          flexDirection: 'row',
          height: 40,
          marginVertical: 8,
          marginHorizontal: 32,
          borderRadius: BORDER_RADIUS,
          borderWidth: 1,
          borderColor: '#00c9cb',
        }}
      >
        <Button
          text={'اعلانات'}
          onClick={() => setSwitchValue('informs')}
          isSelected={switchValue === 'informs'}
          isRight
        />
        <Button
          text={'نظرسنجی'}
          onClick={() => setSwitchValue('polls')}
          isSelected={switchValue === 'polls'}
        />
      </View>
    );
  }
}

class SurveyPopUp extends Component {
  constructor() {
    super();
    this.state = {
      title: '',
      type: 1,
      answerOne: '',
      answerTwo: '',
      question: '',
      answerThree: '',
      answerFour: '',
    };
  }

  render() {
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          width: 300,
          borderRadius: 4,
        }}
      >
        <View style={{ padding: 16 }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginBottom: 16,
            }}
          >
            <Text style={{ marginRight: 16, textAlign: 'left' }}>
              عنوان نظرسنجی
            </Text>
            <MKTextField
              ref="textInput"
              multiline={false}
              keyboardType="default"
              returnKeyType="done"
              floatingLabelEnable={false}
              tintColor={placeholderTextColor}
              textInputStyle={{
                fontSize: 14,
                color: 'black',
                flexGrow: 1,
                textAlign: 'center',
              }}
              underlineSize={1}
              placeholder="نظرسنجی شماره ۱"
              style={{ width: 120 }}
              onChangeText={text => this.setState({ title: text })}
              highlightColor={primaryDark}
              value={this.state.title}
              onSubmitEditing={() => Keyboard.dismiss()}
            />
          </View>

          <View
            style={{
              marginBottom: 16,
            }}
          >
            <Text
              style={{ marginRight: 16, marginBottom: 8, textAlign: 'left' }}
            >
              سوال نظرسنجی
            </Text>
            <MKTextField
              ref="textInput"
              multiline={false}
              keyboardType="default"
              returnKeyType="done"
              floatingLabelEnable={false}
              tintColor={placeholderTextColor}
              textInputStyle={{
                fontSize: 14,
                color: 'black',
                flexGrow: 1,
                textAlign: 'right',
              }}
              underlineSize={1}
              placeholder="لطفا سوال نظرسنجی را در اینجا وارد کنید"
              style={{ width: 280 }}
              onChangeText={text => this.setState({ question: text })}
              highlightColor={primaryDark}
              value={this.state.question}
            />
          </View>
          <View style={{ flexDirection: 'row', marginVertical: 16 }}>
            <TouchableOpacity onPress={() => this.setState({ type: 1 })}>
              <View style={{ flexDirection: 'row', marginRight: 16 }}>
                <Text>دو گزینه‌ای</Text>
                <Image
                  source={
                    this.state.type == 1
                      ? images.checked_icon
                      : images.unchecked_icon
                  }
                  style={{ tintColor: primaryDark, height: 18, width: 18 }}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this.setState({ type: 0 })}>
              <View style={{ flexDirection: 'row' }}>
                <Text>چند گزینه‌ای</Text>
                <Image
                  source={
                    this.state.type == 0
                      ? images.checked_icon
                      : images.unchecked_icon
                  }
                  style={{ tintColor: primaryDark, height: 18, width: 18 }}
                />
              </View>
            </TouchableOpacity>
          </View>
          {this.state.type == 0 && (
            <View>
              <View style={{ marginVertical: 8 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={{ marginRight: 16, textAlign: 'left' }}>
                    گزینه اول
                  </Text>
                  <MKTextField
                    ref="textInput"
                    multiline={false}
                    keyboardType="default"
                    returnKeyType="done"
                    floatingLabelEnable={false}
                    tintColor={placeholderTextColor}
                    textInputStyle={{
                      fontSize: 14,
                      color: 'black',
                      flexGrow: 1,
                      textAlign: 'center',
                    }}
                    underlineSize={1}
                    placeholder="نوشته گزینه اول"
                    style={{ flex: 1 }}
                    onChangeText={text => this.setState({ answerOne: text })}
                    highlightColor={primaryDark}
                    value={this.state.answerOne}
                    onSubmitEditing={() => Keyboard.dismiss()}
                  />
                </View>
              </View>

              <View style={{ marginVertical: 8 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={{ marginRight: 16, textAlign: 'left' }}>
                    گزینه دوم
                  </Text>
                  <MKTextField
                    ref="textInput"
                    multiline={false}
                    keyboardType="default"
                    returnKeyType="done"
                    floatingLabelEnable={false}
                    tintColor={placeholderTextColor}
                    textInputStyle={{
                      fontSize: 14,
                      color: 'black',
                      flexGrow: 1,
                      textAlign: 'center',
                    }}
                    underlineSize={1}
                    placeholder="نوشته گزینه دوم"
                    style={{ flex: 1 }}
                    onChangeText={text => this.setState({ answerTwo: text })}
                    highlightColor={primaryDark}
                    value={this.state.answerTwo}
                    onSubmitEditing={() => Keyboard.dismiss()}
                  />
                </View>
              </View>

              <View style={{ marginVertical: 8 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={{ marginRight: 16, textAlign: 'left' }}>
                    گزینه سوم
                  </Text>
                  <MKTextField
                    ref="textInput"
                    multiline={false}
                    keyboardType="default"
                    returnKeyType="done"
                    floatingLabelEnable={false}
                    tintColor={placeholderTextColor}
                    textInputStyle={{
                      fontSize: 14,
                      color: 'black',
                      flexGrow: 1,
                      textAlign: 'center',
                    }}
                    underlineSize={1}
                    placeholder="نوشته گزینه سوم"
                    style={{ flex: 1 }}
                    onChangeText={text => this.setState({ answerThree: text })}
                    highlightColor={primaryDark}
                    value={this.state.answerThree}
                    onSubmitEditing={() => Keyboard.dismiss()}
                  />
                </View>
              </View>

              <View style={{ marginVertical: 8 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Text style={{ marginRight: 16, textAlign: 'left' }}>
                    گزینه چهارم
                  </Text>
                  <MKTextField
                    ref="textInput"
                    multiline={false}
                    keyboardType="default"
                    returnKeyType="done"
                    floatingLabelEnable={false}
                    tintColor={placeholderTextColor}
                    textInputStyle={{
                      fontSize: 14,
                      color: 'black',
                      flexGrow: 1,
                      textAlign: 'center',
                    }}
                    underlineSize={1}
                    placeholder="نوشته گزینه چهارم"
                    style={{ flex: 1 }}
                    onChangeText={text => this.setState({ answerFour: text })}
                    highlightColor={primaryDark}
                    value={this.state.answerFour}
                    onSubmitEditing={() => Keyboard.dismiss()}
                  />
                </View>
              </View>
            </View>
          )}
        </View>

        <TouchableOpacity onPress={this.props.onSave.bind(this, this.state)}>
          <View style={{ flexDirection: 'row' }}>
            <View
              style={{
                flex: 1,
                height: 48,
                backgroundColor: primaryDark,
                alignItems: 'center',
                justifyContent: 'center',
                borderBottomLeftRadius: 4,
                borderBottomRightRadius: 4,
              }}
            >
              <Text style={{ fontSize: 18, color: 'white' }}>ذخیره</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

@observer
export default class Informs extends Component {
  constructor() {
    super();
    this.state = {
      showOverlay: false,
      showDeletePopUp: false,
      showEditPopUp: false,
      showAddNewLabelPopUp: false,
      nominatedToDeleteItem: null,
      nominateToEditItem: null,
      switchValue: 'informs',
      showOpinionSubmitPopUp: false,
      nominatedOpinion: null,
      loading: false,
      loadingMessage: '',
    };
  }

  render() {
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: 'اعلانات ساختمان',
      },
    };
    console.warn(this.state.switchValue);
    return (
      <View style={{ flex: 1 }}>
        <Toolbar customStyle={toolbarStyle} />
        <AndroidBackButton
          onPress={() => {
            if (this.state.loading) {
              return true;
            } else if (
              this.state.showAddNewLabelPopUp ||
              this.state.showEditPopUp
            ) {
              this.setState({
                showAddNewLabelPopUp: false,
                showOverlay: false,
                showEditPopUp: false,
                nominateToEditItem: null,
              });
            } else if (this.state.showDeletePopUp) {
              this.setState({
                showOverlay: false,
                showDeletePopUp: false,
                nominatedToDeleteItem: null,
              });
            } else if (this.state.showOpinionSubmitPopUp) {
              this.setState({
                showOpinionSubmitPopUp: false,
                nominatedOpinion: null,
                showOverlay: false,
              });
            } else {
              this.onBackPress();
            }
            return true;
          }}
        />
        <View style={{ backgroundColor: 'white', elevation: 1 }}>
          <TypeSelector
            setSwitchValue={type => this.setState({ switchValue: type })}
            switchValue={this.state.switchValue}
          />
        </View>
        {this.state.switchValue == 'informs' ? (
          <FlatList
            keyExtractor={(item, index) => index}
            data={apartmentStore.noticeBoard}
            renderItem={({ item }) => (
              <InformCard
                item={item}
                delete={item => this.showDeletePopUp(item)}
                navigateToEdit={item => this.showEditPopUp(item)}
              />
            )}
          />
        ) : (
          <FlatList
            keyExtractor={(item, index) => index}
            data={apartmentStore.survey}
            renderItem={({ item }) => (
              <SurveyCard
                item={item}
                delete={title => this.showDeletePopUp(title)}
                navigateToEdit={item => this.showEditPopUp(item)}
                onOpinionPress={item => this.onOpinionPress(item)}
              />
            )}
          />
        )}
        {(apartmentStore.userRole == 'Admin' ||
          apartmentStore.userRole == 'Manager') && (
          <View
            pointerEvents="box-none"
            style={{
              ...StyleSheet.absoluteFillObject,
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
              flex: 1,
              padding: 16,
            }}
          >
            <Fab
              bgColor={primaryDark}
              onPress={() => this.showAddNewLabelPopUp()}
              icon={images.plus_icon}
              iconColor="white"
            />
          </View>
        )}
        {this.state.showOverlay && (
          <Overlay catchTouch={true} onPress={this.dismissAll.bind(this)}>
            {this.state.showDeletePopUp && (
              <DeletePopUp
                confirm={
                  this.state.switchValue == 'informs'
                    ? this.onConfirmDelete.bind(this)
                    : this.onConfirmDeleteSurvey.bind(this)
                }
                dismiss={this.dismissAll.bind(this)}
                title={this.state.nominatedToDeleteItem.Title}
              />
            )}
            {this.state.showAddNewLabelPopUp &&
              (this.state.switchValue == 'informs' ? (
                <View>
                  <AddMessagePopUp
                    onPress={item => this.onConfirmNewLabel(item)}
                  />
                  {Platform.OS == 'ios' && <KeyboardSpacer />}
                </View>
              ) : (
                <View>
                  <SurveyPopUp onSave={item => this.submitSurvey(item)} />
                  {Platform.OS == 'ios' && <KeyboardSpacer />}
                </View>
              ))}
            {this.state.showEditPopUp && (
              <View>
                <AddMessagePopUp
                  onPress={this.onConfirmNewLabel.bind(this)}
                  item={this.state.nominateToEditItem}
                />
                {Platform.OS == 'ios' && <KeyboardSpacer />}
              </View>
            )}
            {this.state.showOpinionSubmitPopUp && (
              <SubmitOpinionPopUp
                confirm={this.submitSurveyOpinion.bind(this)}
                dismiss={this.dismissAll.bind(this)}
                title={this.state.nominatedOpinion.title}
              />
            )}
          </Overlay>
        )}
        {this.state.loading && (
          <Overlay>
            <LoadingPopUp message={this.state.loadingMessage} />
          </Overlay>
        )}
      </View>
    );
  }

  onBackPress() {
    this.props.navigation.goBack();
  }

  async onConfirmNewLabel(item) {
    Keyboard.dismiss();
    this.setState({ loading: true, loadingMessage: 'در حال ذخیره ...' });
    const addNoticeRes = await addNoticeBoardQuery(
      Object.assign(item, { apartment_ID: apartmentStore.apartmentID }),
    );
    console.warn(addNoticeRes);
    if (addNoticeRes) {
      this.setState({
        showOverlay: false,
        showDeletePopUp: false,
        showEditPopUp: false,
        showAddNewLabelPopUp: false,
        nominatedToDeleteItem: '',
        nominateToEditItem: null,
        showOpinionSubmitPopUp: false,
      });
    } else {
      //TODO: show proper error
    }
    this.setState({ loading: false });
  }

  async onConfirmDeleteSurvey() {
    this.setState({ loading: true, loadingMessage: 'در حال حذف ...' });
    const {
      Title,
      Question,
      QuestionType,
      AnswerOne,
      AnswerTwo,
      AnswerThree,
      ID,
      AnswerFour,
    } = this.state.nominatedToDeleteItem;
    let newItem;
    if (QuestionType == 1) {
      newItem = {
        id: ID,
        apartment_ID: apartmentStore.apartmentID,
        question: Question,
        questionType: QuestionType,
        title: Title,
        answerOne: '',
        answerTwo: '',
        answerThree: '',
        answerFour: '',
        isDisabled: true,
      };
    } else {
      newItem = {
        id: ID,
        apartment_ID: apartmentStore.apartmentID,
        question: Question,
        questionType: QuestionType,
        title: Title,
        answerOne: AnswerOne,
        answerTwo: AnswerTwo,
        answerThree: AnswerThree,
        answerFour: AnswerFour,
        isDisabled: true,
      };
    }

    const surveyRes = await submitSurveyQuery(newItem);
    if (surveyRes) {
      this.dismissAll();
    } else {
      //TODO: show proper error
    }
    this.setState({ loading: false });
  }

  async onConfirmDelete() {
    this.setState({ loading: true, loadingMessage: 'در حال حذف ...' });
    const { Title, ID, Description } = this.state.nominatedToDeleteItem;
    const deleteLabelRes = await addNoticeBoardQuery({
      title: Title,
      id: ID,
      description: Description,
      isDisabled: true,
    });
    if (deleteLabelRes) {
      this.dismissAll();
    } else {
      // TODO: show proper error
    }
    this.setState({ loading: false });
  }

  showDeletePopUp(item) {
    this.setState({
      showOverlay: true,
      showDeletePopUp: true,
      nominatedToDeleteItem: item,
      showOpinionSubmitPopUp: false,
    });
  }

  showEditPopUp(item) {
    this.setState({
      showOverlay: true,
      showEditPopUp: true,
      nominateToEditItem: item,
      showOpinionSubmitPopUp: false,
    });
  }

  showAddNewLabelPopUp() {
    this.setState({
      showOverlay: true,
      showAddNewLabelPopUp: true,
      showOpinionSubmitPopUp: false,
    });
  }

  async submitSurvey(item) {
    Keyboard.dismiss();
    this.setState({
      loading: true,
      loadingMessage: 'در حال ذخیره نظرسنجی ...',
    });
    const {
      title,
      type,
      answerOne,
      answerTwo,
      answerThree,
      question,
      answerFour,
    } = item;
    let newItem;
    if (type == 1) {
      newItem = {
        apartment_ID: apartmentStore.apartmentID,
        question: question,
        questionType: type,
        title: title,
        answerOne: '',
        answerTwo: '',
        answerThree: '',
        answerFour: '',
      };
    } else {
      newItem = {
        apartment_ID: apartmentStore.apartmentID,
        question: question,
        questionType: type,
        answerOne: answerOne,
        answerTwo: answerTwo,
        answerThree: answerThree,
        answerFour: answerFour,
        title: title,
      };
    }

    const surveyRes = await submitSurveyQuery(newItem);
    if (surveyRes) {
      this.dismissAll();
    } else {
      //TODO: show proper error
    }
    this.setState({ loading: false });
  }

  dismissAll() {
    this.setState({
      showOverlay: false,
      showDeletePopUp: false,
      showEditPopUp: false,
      showAddNewLabelPopUp: false,
      nominatedToDeleteItem: null,
      nominateToEditItem: null,
      showOpinionSubmitPopUp: false,
    });
  }

  onOpinionPress(item) {
    this.setState({
      nominatedOpinion: item,
      showOverlay: true,
      showOpinionSubmitPopUp: true,
    });
  }

  async submitSurveyOpinion() {
    this.setState({ loading: true, loadingMessage: 'در حال ثبت نظر شما ...' });
    const { survey_ID, answer } = this.state.nominatedOpinion;
    const opinion = {
      answer: answer,
      survey_ID: survey_ID,
    };

    const sumbitOpinionRes = await submitSurveyResultQuery(opinion, survey_ID);
    if (sumbitOpinionRes) {
      this.dismissAll();
    } else {
      //TODO: show proper error
    }
    this.setState({ loading: false });
  }
}

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  rightStyle: {
    borderBottomRightRadius: BORDER_RADIUS,
    borderTopRightRadius: BORDER_RADIUS,
  },
  leftStyle: {
    borderBottomLeftRadius: BORDER_RADIUS,
    borderTopLeftRadius: BORDER_RADIUS,
  },
  selectedButton: {
    backgroundColor: '#00c9cb',
  },
  unselecetedButton: {
    backgroundColor: 'white',
  },
  text: {
    fontSize: 14,
  },
  selectedText: {
    color: 'white',
  },
  unselectedText: { color: '#00c9cb' },
});
