//@flow
import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { Toolbar, MainItemCard } from '../components';
import {
  bgColor,
  fabColor,
  darkGrey,
  lightGrey,
  primaryDark,
  primary,
} from '../constants/colors';
import { persistStore, apartmentStore } from '../stores';
import { MainContent, Contacts, Notifications } from './index';
import images from '@assets/images';

const { height, width } = Dimensions.get('window');

const HOME_TYPE = 'HOME';
const PHONE_BOOK_TYPE = 'PHONE_BOOK';
const NOTIFICATION_TYPE = 'NOTIFICATION_TYPE';

export default class MainScreen extends Component {
  constructor() {
    super();
    this.state = {
      selected: HOME_TYPE,
    };
  }

  render() {
    const toolbarStyle = {
      left: {
        onPress: this.onPressMenu.bind(this),
        content: images.menu,
      },
      center: {
        isImage: false,
        content: 'APAMAN',
      },
    };
    return (
      <View style={styles.container}>
        <Toolbar customStyle={toolbarStyle} />
        <View
          style={{
            flex: 1,
            paddingTop: 4,
          }}
        >
          <View style={{ flex: 1 }}>
            {this.state.selected == HOME_TYPE ? (
              <MainContent
                onCostPress={() => this.props.navigation.navigate('Costs')}
                onFacilityPress={() =>
                  this.props.navigation.navigate('Facilities', {
                    nextPage: 'SPECIAL',
                  })}
                onInformsPress={() => this.props.navigation.navigate('Informs')}
                onReportPress={() => this.props.navigation.navigate('Reports')}
              />
            ) : this.state.selected == PHONE_BOOK_TYPE ? (
              <Contacts />
            ) : (
              <Notifications />
            )}
          </View>
          <LinearGradient
            colors={['white', lightGrey]}
            locations={[0, 1]}
            style={{
              height: 52,
              width: width,
              alignItems: 'center',
              flexDirection: 'row',
              backgroundColor: 'white',
              elevation: 8,
            }}
          >
            <TouchableOpacity
              style={{ flex: 1 }}
              onPress={() => {
                this.setState({ selected: NOTIFICATION_TYPE });
                this.forceUpdate();
              }}
            >
              <View
                style={{
                  height: this.state.selected == NOTIFICATION_TYPE ? 4 : 0,
                  width: width / 3,
                  backgroundColor: fabColor,
                }}
              />
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Image
                  source={images.notification_icon}
                  style={{
                    tintColor:
                      this.state.selected == NOTIFICATION_TYPE
                        ? fabColor
                        : darkGrey,
                    height: 28,
                    width: 28,
                    resizeMode: 'contain',
                  }}
                />
                {apartmentStore.notifCount > 0 && (
                  <View
                    style={{
                      ...StyleSheet.absoluteFillObject,
                    }}
                  >
                    <View
                      style={{
                        backgroundColor: 'red',
                        height: 24,
                        width: 24,
                        padding: 8,
                        borderRadius: 32,
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginTop: 8,
                        marginLeft: 32,
                      }}
                    >
                      <Text style={{ color: 'white', fontSize: 10 }}>
                        {apartmentStore.notifCount}
                      </Text>
                    </View>
                  </View>
                )}
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ flex: 1 }}
              onPress={() => this.setState({ selected: PHONE_BOOK_TYPE })}
            >
              <View
                style={{
                  height: this.state.selected == PHONE_BOOK_TYPE ? 4 : 0,
                  width: width / 3,
                  backgroundColor: fabColor,
                }}
              />
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Image
                  source={images.phonebook_icon}
                  style={{
                    tintColor:
                      this.state.selected == PHONE_BOOK_TYPE
                        ? fabColor
                        : darkGrey,
                    height: 28,
                    width: 28,
                    resizeMode: 'contain',
                  }}
                />
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ flex: 1 }}
              onPress={() => this.setState({ selected: HOME_TYPE })}
            >
              <View
                style={{
                  height: this.state.selected == HOME_TYPE ? 4 : 0,
                  width: width / 3,
                  backgroundColor: fabColor,
                }}
              />
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <Image
                  source={images.home_icon}
                  style={{
                    tintColor:
                      this.state.selected == HOME_TYPE ? fabColor : darkGrey,
                    height: 28,
                    width: 28,
                    resizeMode: 'contain',
                  }}
                />
              </View>
            </TouchableOpacity>
          </LinearGradient>
        </View>
      </View>
    );
  }

  onPressMenu() {
    this.props.navigation.navigate('DrawerOpen');
  }
}

const styles = StyleSheet.create({
  container: { flex: 1 },
});
