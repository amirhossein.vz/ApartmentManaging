import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, AsyncStorage } from 'react-native';
import { create } from 'mobx-persist';
import { NavigationActions } from 'react-navigation';

import images from '@assets/images';
import { primaryBg } from '../constants/colors';
import { persistStore, apartmentStore } from '../stores';
import {
  loginQuery,
  apartmentDataQuery,
  unitsQuery,
  phoneBookQuery,
  getContactUsQuery,
  getSuggestionQuery,
  getNotificationQuery,
  getFacilityQuery,
  getFacilityDetailsQuery,
  userInfoQuery,
  addContactUsQuery,
  getRuleQuery,
  addRuleQuery,
  getCostQuery,
  getSurveyQuery,
  getNoticeBoard,
  getCostTypeQuery,
  getInvoiceQuery,
  getFacilityTitles,
  getContactUsTypesQuery,
  getRequestQuery,
} from '../network/Queries';

export default class Splash extends Component {
  async componentWillMount() {
    await this.fetchStore();
    if (persistStore.token) {
      try {
        const userInfoRes = await userInfoQuery();
        const apartmentDataRes = await apartmentDataQuery();
        const phoneBookRes = await phoneBookQuery(apartmentStore.apartmentID);
        const facilityRes = await getFacilityQuery(apartmentStore.apartmentID);
        const suggestionRes = await getSuggestionQuery(
          apartmentStore.apartmentID,
        );
        const ruleRes = await getRuleQuery(apartmentStore.apartmentID);
        const notifRes = await getNotificationQuery();
        const contactUsRes = await getContactUsQuery(
          apartmentStore.apartmentID,
        );
        const costRes = await getCostQuery(apartmentStore.apartmentID);
        const noticeRes = await getNoticeBoard(apartmentStore.apartmentID);
        const surveyRes = await getSurveyQuery(apartmentStore.apartmentID);
        const costTypeRes = await getCostTypeQuery();
        const invoiceRes = await getInvoiceQuery();
        const facilityTypesRes = await getFacilityTitles();
        const contactUsTypesRes = await getContactUsTypesQuery();
        const requestRes = await getRequestQuery(apartmentStore.apartmentID);
        // for (let i = 0; i < apartmentStore.facility.length; i++) {
        //   let facilityDetailsRes = await getFacilityDetailsQuery(
        //     apartmentStore.apartmentID,
        //     '1',
        //   );
        let unitRes;
        if (
          apartmentStore.userRole == 'Manager' ||
          apartmentStore.userRole == 'Admin'
        ) {
          unitRes = await unitsQuery(apartmentStore.apartmentID);
        }

        if ((await userInfoRes) && (await apartmentDataRes)) {
          if (
            apartmentStore.userRole == 'MANAGER' ||
            apartmentStore.userRole == 'ADMIN'
          ) {
            if (await unitRes) {
              this.navigateToMain();
            } else {
              this.navigateToLogin();
            }
          } else {
            this.navigateToMain();
          }
        } else {
          this.navigateToLogin();
        }
      } catch (error) {
        this.navigateToLogin();
      }
    } else {
      this.navigateToLogin();
    }
  }

  navigateToMain() {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Main' })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  navigateToLogin() {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Login' })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={images.bg} style={styles.background}>
          <View style={styles.logoContainer}>
            <Image source={images.logo} style={styles.logo} />
          </View>
        </Image>
      </View>
    );
  }

  async fetchStore() {
    const hydrate = create({
      storage: AsyncStorage,
      jsonify: true,
    });
    await hydrate('persistStore', persistStore);
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: primaryBg },
  background: {
    flex: 1,
    width: null,
    height: null,
    alignItems: 'center',
    resizeMode: 'stretch',
    marginBottom: 16,
  },
  logoContainer: {
    flex: 1,
    alignItems: 'flex-start',
    marginTop: 72,
    alignItems: 'center',
  },
  logo: {
    height: 204,
    width: 204,
    resizeMode: 'contain',
  },
});
