import React, { Component } from 'react';
import {
  View,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import { observer } from 'mobx-react/native';

import { primary, fabColor, primaryDark } from '../constants/colors';
import {
  UnitCard,
  Toolbar,
  Fab,
  Overlay,
  LoadingPopUp,
  AndroidBackButton,
} from '../components';
import images from '@assets/images';
import { apartmentStore } from '../stores';
import { addUnitQuery } from '../network/Queries';

class DeletePopUp extends Component {
  render() {
    const { confirm, dismiss } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          paddingTop: 16,
          paddingBottom: 8,
          paddingHorizontal: 8,
          width: 300,
          height: 120,
          justifyContent: 'space-between',
          borderRadius: 4,
        }}
      >
        <Text style={{ textAlign: 'left' }}>
          آیا از حذف واحد {this.props.unitNo} مطمئن هستید؟!
        </Text>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity onPress={confirm} style={{ paddingHorizontal: 16 }}>
            <Text style={{ color: primaryDark }}>تایید</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={dismiss} style={{ paddingHorizontal: 16 }}>
            <Text style={{ color: primaryDark }}>انصراف</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

@observer
export default class Units extends Component {
  constructor() {
    super();
    this.state = {
      showOverlay: false,
      nominatedToDeleteItem: null,
    };
  }

  render() {
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: 'واحدها',
      },
    };
    return (
      <View style={{ flex: 1 }}>
        <Toolbar customStyle={toolbarStyle} />
        <AndroidBackButton
          onPress={() => {
            if (this.state.showOverlay) {
              this.setState({
                showOverlay: false,
                nominatedToDeleteItem: null,
              });
            } else {
              this.onBackPress();
            }
            return true;
          }}
        />
        <FlatList
          keyExtractor={(item, index) => index}
          data={apartmentStore.units}
          renderItem={({ item }) => (
            <UnitCard
              unit={item}
              navigateToEdit={unit => this.goToEditUnit(unit)}
              deleteUnit={this.showSavePanel.bind(this)}
            />
          )}
        />
        <View
          pointerEvents="box-none"
          style={{
            ...StyleSheet.absoluteFillObject,
            alignItems: 'flex-end',
            justifyContent: 'flex-end',
            flex: 1,
            padding: 16,
          }}
        >
          <Fab
            bgColor={primaryDark}
            onPress={this.addNewUnit.bind(this)}
            icon={images.plus_icon}
            iconColor="white"
          />
        </View>
        {this.state.showOverlay && (
          <Overlay
            catchTouch={true}
            onPress={this.dismissDeletePanel.bind(this)}
          >
            <DeletePopUp
              confirm={this.confirmDelete.bind(this)}
              dismiss={this.dismissDeletePanel.bind(this)}
              unitNo={this.state.nominatedToDeleteItem.UnitNumber}
            />
          </Overlay>
        )}
        {this.state.loading && (
          <Overlay>
            <LoadingPopUp message={this.state.loadingMessage} />
          </Overlay>
        )}
      </View>
    );
  }

  onBackPress() {
    this.props.navigation.goBack();
  }

  goToEditUnit(unit) {
    this.props.navigation.navigate('AddUnit', { unit: unit });
  }

  showSavePanel(item) {
    this.setState({ showOverlay: true, nominatedToDeleteItem: item });
  }

  dismissDeletePanel() {
    this.setState({
      showOverlay: false,
      nominatedToDeleteItem: null,
    });
  }

  addNewUnit() {
    this.props.navigation.navigate('AddUnit');
  }

  async confirmDelete() {
    this.setState({ loading: true, loadingMessage: 'در حال حذف واحد ...' });
    const {
      ID,
      Apartment_ID,
      UnitNumber,
      FloorNumber,
      OwnerName,
      OwnerTel,
      TenantName,
      TenantTel,
      Area,
      ParkingCount,
      ParkingNumber,
      Tel,
      NumberOfPeople,
      IsSame,
      IsEmpty,
    } = this.state.nominatedToDeleteItem;

    const item = {
      id: ID,
      apartment_ID: Apartment_ID,
      unitNumber: UnitNumber,
      floorNumber: FloorNumber,
      ownerName: OwnerName,
      ownerTel: OwnerTel,
      tenantName: TenantName,
      tenantTel: TenantTel,
      area: Area,
      parkingCount: ParkingCount,
      parkingNumber: ParkingNumber,
      tel: Tel,
      numberOfPeople: NumberOfPeople,
      isSame: IsSame,
      isEmpty: IsEmpty,
      isDisabled: true,
    };

    const addUnitRes = await addUnitQuery(item); // TODO: not working
    if (addUnitRes) {
      this.setState({
        showOverlay: false,
        nominatedToDeleteItem: null,
      });
    } else {
      //TODO: show proper message
    }
    this.setState({ loading: false });
  }
}
