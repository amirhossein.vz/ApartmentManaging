import React, { Component } from 'react';
import {
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  Text,
  Keyboard,
  Platform,
} from 'react-native';
import { observer } from 'mobx-react/native';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import {
  AddSansRow,
  Toolbar,
  Fab,
  Overlay,
  SansCard,
  AddSansPopUp,
  LoadingPopUp,
  AndroidBackButton,
} from '../components';
import images from '@assets/images';
import { primaryDark } from '../constants/colors';
import { apartmentStore } from '../stores';
import { getFacilityDetailsQuery } from '../network/Queries';
import { facilityStatusTypes } from '../constants/values';
import { addSansQuery } from '../network/Queries';

const MALE = 'MALE';
const FEMALE = 'FEMALE';
const BOTH = 'BOTH';
const NONE = 'NONE';

const GENERAL_TYPE = 'GENERAL';
const SPECIAL_TYPE = 'SPECIAL';
const OFF_TYPE = 'OFF';

const BORDER_RADIUS = 8;

class DeletePopUp extends Component {
  render() {
    const { onConfirm, onDismiss, message } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          paddingTop: 16,
          paddingBottom: 8,
          paddingHorizontal: 8,
          width: 300,
          height: 120,
          justifyContent: 'space-between',
          borderRadius: 4,
        }}
      >
        <Text style={{ textAlign: 'left' }}>{message}</Text>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            onPress={onConfirm}
            style={{ paddingHorizontal: 16 }}
          >
            <Text style={{ color: primaryDark }}>تایید</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={onDismiss}
            style={{ paddingHorizontal: 16 }}
          >
            <Text style={{ color: primaryDark }}>انصراف</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

function Button({ text, onClick, isSelected, isRight, isLeft, isCenter }) {
  return (
    <View
      style={[
        styles.button,
        isSelected ? styles.selectedButton : styles.unselecetedButton,
        isRight ? styles.rightStyle : {},
        isLeft ? styles.leftStyle : {},
        ,
        {
          borderRightWidth: 1,
          borderLeftWidth: 1,
          borderRightColor: '#00c9cb',
          borderLeftColor: '#00c9cb',
        },
      ]}
    >
      <TouchableOpacity onPress={onClick} style={{ alignItems: 'center' }}>
        <Text
          style={[
            styles.text,
            isSelected ? styles.selectedText : styles.unselectedText,
          ]}
        >
          {text}
        </Text>
      </TouchableOpacity>
    </View>
  );
}

class TypeSelector extends Component {
  render() {
    const { setSwitchValue, switchValue } = this.props;
    return (
      <View
        style={{
          flexDirection: 'row',
          height: 40,
          marginVertical: 8,
          marginHorizontal: 32,
          borderRadius: BORDER_RADIUS,
          borderWidth: 1,
          borderColor: '#00c9cb',
        }}
      >
        <Button
          text={'عمومی'}
          onClick={() => setSwitchValue(GENERAL_TYPE)}
          isSelected={switchValue === GENERAL_TYPE}
          isRight
        />
        <Button
          text={'خصوصی'}
          onClick={() => setSwitchValue(SPECIAL_TYPE)}
          isSelected={switchValue === SPECIAL_TYPE}
          isCenter
        />
        <Button
          text={'تعطیلات'}
          onClick={() => setSwitchValue(OFF_TYPE)}
          isSelected={switchValue === OFF_TYPE}
          isLeft
        />
      </View>
    );
  }
}

@observer
export default class AddSans extends Component {
  constructor() {
    super();
    this.state = {
      switchValue: GENERAL_TYPE,
      showOverlay: false,
      data: [],
      loading: false,
      showDeletePopUp: false,
      haveEmptyField: false,
    };
  }

  async componentWillMount() {
    const facilityDetailsRes = await getFacilityDetailsQuery(
      apartmentStore.apartmentID,
      this.props.navigation.state.params.id,
    );
    if (facilityDetailsRes.status) {
      this.setState({
        data: facilityDetailsRes.data.filter(
          item =>
            item.DayOfWeek == this.props.navigation.state.params.day.value,
        ),
      });
    } else {
      //TODO: show proper error
    }
  }

  render() {
    console.warn(JSON.stringify(this.props.navigation.state.params.day));
    const title =
      this.props.navigation &&
      this.props.navigation.state &&
      this.props.navigation.state.params
        ? 'برنامه ' +
          this.props.navigation.state.params.facilityName +
          ' روز ' +
          this.props.navigation.state.params.day.label
        : 'برنامه امکانات رفاهی';
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: title,
      },
    };
    console.warn(this.state.switchValue);
    return (
      <View style={{ flex: 1 }}>
        <Toolbar customStyle={toolbarStyle} />
        <AndroidBackButton
          onPress={() => {
            if (this.state.loading) {
              return true;
            } else if (this.state.showOverlay) {
              this.dismissSavePanel();
              return true;
            } else {
              this.onBackPress();
              return true;
            }
            return true;
          }}
        />
        <View style={{ backgroundColor: 'white', elevation: 1 }}>
          <TypeSelector
            setSwitchValue={type => this.setState({ switchValue: type })}
            switchValue={this.state.switchValue}
          />
        </View>
        <View style={{ flex: 1, marginTop: 2 }}>
          <FlatList
            data={this.state.data.filter(
              item =>
                item.Status ==
                  facilityStatusTypes.find(
                    newItem => newItem.value == this.state.switchValue,
                  ).id &&
                item.DayOfWeek == this.props.navigation.state.params.day.value,
            )}
            extraData={this.state.switchValue}
            renderItem={({ item }) => (
              <AddSansRow
                data={item}
                keyExtractor={(item, index) => item.id}
                onLongPress={
                  apartmentStore.userRole == 'Admin' ||
                  apartmentStore.userRole == 'Manager'
                    ? item => this.showDeletePopUp(item)
                    : item => null
                }
              />
            )}
            keyExtractor={(item, index) => index}
          />
        </View>
        {(apartmentStore.userRole == 'Admin' ||
          apartmentStore.userRole == 'Manager') && (
          <View
            pointerEvents="box-none"
            style={{
              ...StyleSheet.absoluteFillObject,
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
              flex: 1,
              padding: 16,
            }}
          >
            <Fab
              bgColor={primaryDark}
              onPress={this.showSavePanel.bind(this)}
              icon={images.plus_icon}
              iconColor="white"
            />
          </View>
        )}
        {this.state.showOverlay && (
          <Overlay catchTouch={true} onPress={this.dismissSavePanel.bind(this)}>
            {this.state.showDeletePopUp ? (
              <DeletePopUp
                message={'آیا از حذف سانس مطمئن هستید؟'}
                onConfirm={() => this.confirmDelete()}
                onDismiss={this.dismissSavePanel.bind(this)}
              />
            ) : (
              <View>
                <AddSansPopUp
                  day={this.props.navigation.state.params.day}
                  id={this.props.navigation.state.params.id}
                  onPress={this.dismissSavePanel.bind(this)}
                  onRetrieveData={data => this.updateData(data)}
                  onChangeLoading={state => this.changeLoadingState(state)}
                  haveEmptyField={this.state.haveEmptyField}
                  setEmptyField={state =>
                    this.setState({ haveEmptyField: state })}
                />
                {Platform.OS == 'ios' && <KeyboardSpacer />}
              </View>
            )}
          </Overlay>
        )}
        {this.state.loading && (
          <Overlay>
            <LoadingPopUp message="در حال ذخیره سانس جدید ..." />
          </Overlay>
        )}
      </View>
    );
  }

  onBackPress() {
    this.props.navigation.state.params.updateFacilityDetails();
    this.props.navigation.goBack();
  }

  showSavePanel() {
    this.setState({ showOverlay: true, showDeletePopUp: false });
  }

  dismissSavePanel() {
    this.setState({
      showOverlay: false,
      showDeletePopUp: false,
      nominatedToDeleteItem: null,
    });
  }

  updateData(data) {
    this.setState({ data: data });
  }

  changeLoadingState(state) {
    this.setState({ loading: state });
  }

  showDeletePopUp(item) {
    this.setState({
      showOverlay: true,
      showDeletePopUp: true,
      nominatedToDeleteItem: item,
    });
  }

  async confirmDelete() {
    this.setState({ loading: true });
    const {
      ID,
      DayOfWeek,
      FromHour,
      ToHour,
      Status,
      Price,
      GenderType,
      Description,
    } = this.state.nominatedToDeleteItem;
    const newItem = {
      facilityDetail_ID: ID,
      dayOfWeek: DayOfWeek,
      fromHour: FromHour,
      toHour: ToHour,
      status: Status,
      price: Price,
      genderType: GenderType,
      description: Description,
      isDisabled: true,
      apartment_ID: apartmentStore.apartmentID,
      facility_ID: this.props.navigation.state.params.id,
    };
    const addSansRes = await addSansQuery(newItem);
    if (addSansRes.status) {
      this.updateData(addSansRes.data);
      this.setState({
        showOverlay: false,
        nominatedToDeleteItem: null,
        showDeletePopUp: false,
        loading: false,
      });
    } else {
      //TODO: show proper error
      this.setState({ loading: false });
    }
  }
}

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  rightStyle: {
    borderBottomRightRadius: BORDER_RADIUS,
    borderTopRightRadius: BORDER_RADIUS,
    borderRightWidth: 1,
    borderRightColor: '#00c9cb',
  },
  leftStyle: {
    borderBottomLeftRadius: BORDER_RADIUS,
    borderTopLeftRadius: BORDER_RADIUS,
    borderRightWidth: 1,
    borderRightColor: '#00c9cb',
  },
  selectedButton: {
    backgroundColor: '#00c9cb',
  },
  unselecetedButton: {
    backgroundColor: 'white',
  },
  text: {
    fontSize: 14,
  },
  selectedText: {
    color: 'white',
  },
  unselectedText: { color: '#00c9cb' },
});
