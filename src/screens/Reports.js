import React, { Component } from 'react';
import {
  View,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  Linking,
  ToastAndroid,
} from 'react-native';
import { observer } from 'mobx-react/native';

import { primary, fabColor, primaryDark } from '../constants/colors';
import {
  CostCard,
  Toolbar,
  Fab,
  Overlay,
  AndroidBackButton,
} from '../components';
import images from '@assets/images';
import {
  monthOfYear,
  ownerDataTypes,
  divisionByUnitDataTypes,
  divisionParameterDataTypes,
} from '../constants/values';
import {
  getInvoiceDetailsQuery,
  getInvoiceURL,
  getInvoiceQuery,
} from '../network/Queries';
import { apartmentStore, persistStore } from '../stores';

const BORDER_RADIUS = 20;
function Button({ text, onClick, isSelected, isRight }) {
  return (
    <View
      style={[
        styles.button,
        isSelected ? styles.selectedButton : styles.unselecetedButton,
        isRight ? styles.rightStyle : styles.leftStyle,
      ]}
    >
      <TouchableOpacity onPress={onClick} style={{ alignItems: 'center' }}>
        <Text
          style={[
            styles.text,
            isSelected ? styles.selectedText : styles.unselectedText,
          ]}
        >
          {text}
        </Text>
      </TouchableOpacity>
    </View>
  );
}

class TypeSelector extends Component {
  render() {
    const { setSwitchValue, switchValue } = this.props;
    return (
      <View
        style={{
          flexDirection: 'row',
          height: 40,
          marginVertical: 8,
          marginHorizontal: 32,
          borderRadius: BORDER_RADIUS,
          borderWidth: 1,
          borderColor: '#00c9cb',
        }}
      >
        <Button
          text={'پرداخت نشده'}
          onClick={() => setSwitchValue('unPaid')}
          isSelected={switchValue === 'unPaid'}
          isRight
        />
        <Button
          text={'پرداخت شده'}
          onClick={() => setSwitchValue('paid')}
          isSelected={switchValue === 'paid'}
        />
      </View>
    );
  }
}

class ConfirmToPayPopUp extends Component {
  render() {
    const { confirm, dismiss, item } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          paddingTop: 16,
          paddingBottom: 8,
          paddingHorizontal: 8,
          width: 300,
          height: 180,
          justifyContent: 'space-between',
          borderRadius: 4,
        }}
      >
        {apartmentStore.userRole == 'User' ? (
          <View>
            <Text style={{ textAlign: 'left', marginBottom: 8 }}>
              آیا از رفتن به درگاه برای پرداخت صورتحساب مطمئن هستید؟
            </Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ textAlign: 'left', flex: 1 }}>عنوان هزینه</Text>
              <Text
                style={{
                  textAlign: 'left',
                  flex: 2,
                  color: 'black',
                  fontSize: 16,
                }}
              >
                {item.Title}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ textAlign: 'left', flex: 1 }}>مبلغ هزینه</Text>
              <Text
                style={{
                  textAlign: 'left',
                  flex: 2,
                  color: 'black',
                  fontSize: 16,
                }}
              >
                {item.UnitTotalPrice}
              </Text>
            </View>
          </View>
        ) : (
          <View>
            <Text style={{ textAlign: 'left', marginBottom: 8 }}>
              آیا از تایید پرداخت نقدی مطمئن هستید؟‌
            </Text>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ textAlign: 'left', flex: 1 }}>عنوان هزینه:</Text>
              <Text
                style={{
                  textAlign: 'left',
                  flex: 2,
                  color: 'black',
                  fontSize: 16,
                }}
              >
                {item.Title}
              </Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ textAlign: 'left', flex: 1 }}>مبلغ هزینه:</Text>
              <Text
                style={{
                  textAlign: 'left',
                  flex: 2,
                  color: 'black',
                  fontSize: 16,
                }}
              >
                {item.UnitTotalPrice}
              </Text>
            </View>
          </View>
        )}
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity onPress={confirm} style={{ paddingHorizontal: 16 }}>
            <Text style={{ color: primaryDark }}>تایید</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={dismiss} style={{ paddingHorizontal: 16 }}>
            <Text style={{ color: primaryDark }}>انصراف</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

class ReportCard extends Component {
  constructor() {
    super();
    this.state = {
      isExpand: false,
      data: [],
    };
  }

  async getDetalis() {
    if (this.state.isExpand) {
      this.setState({ isExpand: false });
    } else {
      if (this.state.length > 0) {
        this.setState({ isExpand: true });
      } else {
        const { ID } = this.props.report;
        const data = {
          id: ID,
        };
        const queryRes = await getInvoiceDetailsQuery(data);
        if (queryRes.status) {
          this.setState({ isExpand: true, data: queryRes.data });
        }
      }
    }
  }

  render() {
    console.warn(JSON.stringify(this.props.report));
    const {
      Title,
      UnitNumber,
      UnitTotalPrice,
      Mah,
      HasPaied,
      IsOnline = false,
    } = this.props.report;
    let statusText = '';
    if (HasPaied) {
      statusText = statusText + 'پرداخت شده';
      if (IsOnline) {
        statusText = statusText + '، بصورت آنلاین';
      } else {
        statusText = statusText + '، بصورت نقدی';
      }
    } else {
      statusText = statusText + 'پرداخت نشده';
    }
    const changeExpandText = this.state.isExpand
      ? 'بستن جزئیات'
      : 'مشاهده جزئیات';
    return (
      <TouchableOpacity
        onLongPress={() => this.props.onLongPress(this.props.report)}
      >
        <View
          style={{
            marginVertical: 4,
            marginHorizontal: 8,
            backgroundColor: 'white',
            elevation: 2,
            padding: 8,
          }}
        >
          <Text
            style={{
              color: primaryDark,
              fontSize: 18,
              marginBottom: 8,
              textAlign: 'left',
            }}
          >
            هزینه {Title} واحد {UnitNumber}
          </Text>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Text style={{ flex: 2, textAlign: 'left' }}>مبلغ کل</Text>
            <Text
              style={{
                fontSize: 16,
                color: 'black',
                flex: 4,
                textAlign: 'left',
              }}
            >
              {UnitTotalPrice} تومان
            </Text>
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Text style={{ flex: 2, textAlign: 'left' }}>وضعیت پرداخت</Text>
            <Text
              style={{
                fontSize: 16,
                color: HasPaied ? primaryDark : 'red',
                flex: 4,
                textAlign: 'left',
              }}
            >
              {statusText}
            </Text>
          </View>
          {Mah && (
            <TouchableOpacity
              style={{ alignItems: 'center', paddingVertical: 8 }}
              onPress={() => {
                this.getDetalis();
              }}
            >
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ color: primary, marginRight: 8 }}>
                  {changeExpandText}
                </Text>
                <Image
                  source={
                    this.state.isExpand
                      ? images.collapse_icon
                      : images.expand_icon
                  }
                  style={{ tintColor: primary, height: 16, width: 16 }}
                />
              </View>
            </TouchableOpacity>
          )}
          {this.state.isExpand &&
            this.state.data.map(item => (
              <View
                style={{
                  borderRadius: 2,
                  borderWidth: 1,
                  borderColor: primaryDark,
                  marginVertical: 4,
                  padding: 8,
                }}
              >
                <View style={{ flexDirection: 'row' }}>
                  <Text style={{ color: 'black' }}>
                    {apartmentStore.costTypes.find(item => item.ID).Name}
                  </Text>
                  <Text style={{ marginLeft: 8, color: 'black' }}>
                    {item.UnitPrice} تومان
                  </Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={{ fontSize: 12 }}>
                    برای{' '}
                    {
                      ownerDataTypes.find(
                        myItem => myItem.ID == item.OwnerOfCost,
                      ).Name
                    }{' '}
                    ساختمان،
                  </Text>
                  <Text style={{ fontSize: 12, marginLeft: 4 }}>
                    برحسب{' '}
                    {
                      divisionParameterDataTypes.find(
                        myItem => myItem.ID == item.CalculateType_ID,
                      ).Name
                    }،
                  </Text>
                  <Text style={{ fontSize: 12, marginLeft: 4 }}>
                    برای{' '}
                    {
                      divisionByUnitDataTypes.find(
                        myItem => myItem.ID == item.ForUnit,
                      ).Name
                    }
                  </Text>
                </View>
              </View>
            ))}
        </View>
      </TouchableOpacity>
    );
  }
}

@observer
export default class Reports extends Component {
  constructor() {
    super();
    this.state = {
      showOverlay: false,
      switchValue: 'unPaid',
      nominatedToPayItem: null,
    };
  }

  componentWillMount() {
    getInvoiceQuery();
  }

  render() {
    console.warn(this.state.switchValue);
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: 'صورت‌حساب',
      },
      right: {
        onPress: () => this.onBackPress(),
      },
    };
    return (
      <View style={{ flex: 1 }}>
        <Toolbar customStyle={toolbarStyle} />
        <AndroidBackButton
          onPress={() => {
            this.onBackPress();
            return true;
          }}
        />
        <View style={{ backgroundColor: 'white', elevation: 1 }}>
          <TypeSelector
            setSwitchValue={type => this.setState({ switchValue: type })}
            switchValue={this.state.switchValue}
          />
        </View>
        <FlatList
          disableVirtualization={true}
          keyExtractor={(item, index) => index}
          data={
            this.state.switchValue == 'unPaid'
              ? apartmentStore.invoice.filter(item => !item.HasPaied)
              : apartmentStore.invoice.filter(item => item.HasPaied)
          }
          renderItem={({ item }) => (
            <ReportCard
              report={item}
              onLongPress={item => this.onCardLongPress(item)}
            />
          )}
          extraData={this.state.switchValue}
          initialNumToRender={2}
        />

        {this.state.showOverlay && (
          <Overlay onPress={() => this.onDismissOverlay()} catchTouch={true}>
            <ConfirmToPayPopUp
              confirm={() => this.onConfirmPayment()}
              dismiss={() => this.onDismissOverlay()}
              item={this.state.nominatedToPayItem}
            />
          </Overlay>
        )}
      </View>
    );
  }

  onBackPress() {
    this.props.navigation.goBack();
  }

  onCardLongPress(item) {
    this.setState({ nominatedToPayItem: item, showOverlay: true });
  }

  onDismissOverlay() {
    this.setState({ nominatedToPayItem: null, showOverlay: false });
  }

  async onConfirmPayment() {
    if (apartmentStore.userRole == 'User') {
      const urlRes = await getInvoiceURL(this.state.nominatedToPayItem.ID);
      if (urlRes && urlRes.status && urlRes.data && urlRes.data[0].Url) {
        Linking.openURL(urlRes.data[0].Url).catch(err =>
          console.error('An error occurred', err),
        );
      } else {
        ToastAndroid.show('خطا در اتصال به درگاه بانکی', ToastAndroid.LONG);
      }
    }
  }
}

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  rightStyle: {
    borderBottomRightRadius: BORDER_RADIUS,
    borderTopRightRadius: BORDER_RADIUS,
  },
  leftStyle: {
    borderBottomLeftRadius: BORDER_RADIUS,
    borderTopLeftRadius: BORDER_RADIUS,
  },
  selectedButton: {
    backgroundColor: '#00c9cb',
  },
  unselecetedButton: {
    backgroundColor: 'white',
  },
  text: {
    fontSize: 14,
  },
  selectedText: {
    color: 'white',
  },
  unselectedText: { color: '#00c9cb' },
});
