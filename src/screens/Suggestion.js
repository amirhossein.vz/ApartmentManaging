import React, { Component } from 'react';
import {
  View,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import { observer } from 'mobx-react/native';

import {
  SuggestionCard,
  Toolbar,
  Fab,
  AddMessagePopUp,
  Overlay,
  LoadingPopUp,
  AndroidBackButton,
} from '../components';
import images from '@assets/images';
import { primaryDark } from '../constants/colors';
import { apartmentStore } from '../stores';
import { addSuggestionQuery, getSuggestionQuery } from '../network/Queries';

class DeletePopUp extends Component {
  render() {
    const { confirm, dismiss, title } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          paddingTop: 16,
          paddingBottom: 8,
          paddingHorizontal: 8,
          width: 300,
          height: 120,
          justifyContent: 'space-between',
          borderRadius: 4,
        }}
      >
        <Text style={{ textAlign: 'left' }}>
          آیا از حذف "{title}" مطمئن هستید؟
        </Text>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity onPress={confirm} style={{ paddingHorizontal: 16 }}>
            <Text style={{ color: primaryDark }}>تایید</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={dismiss} style={{ paddingHorizontal: 16 }}>
            <Text style={{ color: primaryDark }}>انصراف</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

@observer
export default class Suggestion extends Component {
  constructor() {
    super();
    this.state = {
      showOverlay: false,
      showDeletePopUp: false,
      showEditPopUp: false,
      showAddSuggestionPopup: false,
      nominatedToDeleteItem: null,
      nominateToEditItem: null,
      loading: false,
      loadingMessage: '',
    };
  }

  componentWillMount() {
    getSuggestionQuery(apartmentStore.apartmentID);
  }

  render() {
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: 'انتقادات و پیشنهادات',
      },
    };
    return (
      <View style={{ flex: 1 }}>
        <Toolbar customStyle={toolbarStyle} />
        <AndroidBackButton
          onPress={() => {
            if (this.state.loading) {
              return true;
            } else if (this.state.showOverlay) {
              this.setState({
                showOverlay: false,
                showDeletePopUp: false,
                showEditPopUp: false,
                showAddSuggestionPopup: false,
                nominatedToDeleteItem: null,
                nominateToEditItem: null,
              });
            } else {
              this.onBackPress();
            }
            return true;
          }}
        />
        <FlatList
          keyExtractor={(item, index) => index}
          data={apartmentStore.suggestion}
          renderItem={({ item }) => (
            <SuggestionCard
              item={item}
              delete={item => this.showDeletePopUp(item)}
              navigateToEdit={item => this.showEditPopUp(item)}
            />
          )}
        />
        {apartmentStore.userRole == 'User' && (
          <View
            pointerEvents="box-none"
            style={{
              ...StyleSheet.absoluteFillObject,
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
              flex: 1,
              padding: 16,
            }}
          >
            <Fab
              bgColor={primaryDark}
              onPress={() => this.showAddNewLabelPopUp()}
              icon={images.plus_icon}
              iconColor="white"
            />
          </View>
        )}
        {this.state.showOverlay && (
          <Overlay catchTouch={true} onPress={this.dismissAll.bind(this)}>
            {this.state.showDeletePopUp && (
              <DeletePopUp
                confirm={this.deleteLabel.bind(this)}
                dismiss={this.dismissDeletePopUp.bind(this)}
                title={this.state.nominatedToDeleteItem.Title}
              />
            )}
            {this.state.showAddSuggestionPopup && (
              <AddMessagePopUp onPress={data => this.confirmNewLabel(data)} />
            )}
            {this.state.showEditPopUp && (
              <AddMessagePopUp
                onPress={data => this.confirmNewLabel(data)}
                item={this.state.nominateToEditItem}
              />
            )}
          </Overlay>
        )}
        {this.state.loading && (
          <Overlay>
            <LoadingPopUp message={this.state.loadingMessage} />
          </Overlay>
        )}
      </View>
    );
  }

  onBackPress() {
    this.props.navigation.goBack();
  }

  showDeletePopUp(item) {
    this.setState({
      showOverlay: true,
      showDeletePopUp: true,
      nominatedToDeleteItem: item,
    });
  }

  showEditPopUp(item) {
    this.setState({
      showOverlay: true,
      showEditPopUp: true,
      nominateToEditItem: item,
    });
  }

  showAddNewLabelPopUp() {
    this.setState({
      showOverlay: true,
      showEditPopUp: true,
    });
  }

  dismissDeletePopUp() {
    this.setState({
      showOverlay: false,
      showDeletePopUp: false,
      nominatedToDeleteItem: null,
    });
  }

  dismissEditPopUp() {
    this.setState({
      showOverlay: false,
      showEditPopUp: false,
      nominateToEditItem: null,
    });
  }

  dismissNewLabelPopUp() {
    this.setState({
      showOverlay: false,
      showEditPopUp: false,
    });
  }

  dismissAll() {
    this.setState({
      showOverlay: false,
      showDeletePopUp: false,
      showEditPopUp: false,
      showAddSuggestionPopup: false,
      nominatedToDeleteItem: null,
      nominateToEditItem: null,
    });
  }

  async confirmNewLabel(newLabel) {
    Keyboard.dismiss();
    this.setState({ loading: true, loadingMessage: 'در حال ذخیره ...' });
    let response;

    if (this.state.showEditPopUp && this.state.nominateToEditItem) {
      response = await addSuggestionQuery(
        Object.assign(newLabel, {
          apartment_ID: apartmentStore.apartmentID,
          id: this.state.nominateToEditItem.ID,
        }),
      );
    } else {
      response = await addSuggestionQuery(
        Object.assign(newLabel, { apartment_ID: apartmentStore.apartmentID }),
      );
    }
    if (await response) {
      this.dismissEditPopUp();
    } else {
      //TODO: show error falied
    }
    this.setState({ loading: false });
  }

  async deleteLabel() {
    this.setState({ loading: true, loadingMessage: 'در حال حذف ...' });
    const { Title, ID, Description } = this.state.nominatedToDeleteItem;
    const response = await addSuggestionQuery({
      id: ID,
      isDisabled: true,
      title: Title,
      description: Description,
      apartment_ID: apartmentStore.apartmentID,
    });

    if (await response) {
      this.dismissDeletePopUp();
    } else {
      //TODO: show error falied
    }
    this.setState({ loading: false });
  }
}
