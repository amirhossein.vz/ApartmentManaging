import React, { Component } from 'react';
import { View, TouchableOpacity, Image, Text, Dimensions } from 'react-native';
import images from '@assets/images';
import {
  bgColor,
  fabColor,
  darkGrey,
  lightGrey,
  primaryDark,
} from '../constants/colors';
import { observer } from 'mobx-react/native';

const { height, width } = Dimensions.get('window');

@observer
export default class MainContent extends Component {
  render() {
    const {
      onCostPress,
      onFacilityPress,
      onInformsPress,
      onReportPress,
    } = this.props;
    return (
      <View
        style={{
          flex: 1,
          width: width - 8,
          marginBottom: 4,
        }}
      >
        <TouchableOpacity
          style={{
            flex: 2,
            elevation: 4,
            marginBottom: 4,
          }}
          activeOpacity={0.4}
          onPress={onCostPress}
        >
          <Image
            source={images.cost_icon}
            style={{
              width: width - 8,
              flex: 1,
              resizeMode: 'cover',
              justifyContent: 'flex-end',
            }}
          >
            <View
              style={{
                backgroundColor: 'rgba(255,255,255,0.6)',
                height: 40,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Text style={{ color: primaryDark, fontSize: 18 }}>هـزینــه</Text>
            </View>
          </Image>
        </TouchableOpacity>
        <View
          style={{
            flex: 3,
            flexDirection: 'row',
          }}
        >
          <TouchableOpacity
            style={{ flex: 1, marginRight: 4, elevation: 2 }}
            activeOpacity={0.4}
            onPress={onReportPress}
          >
            <Image
              source={images.report_icon}
              style={{
                width: (width - 12) / 2,
                flex: 1,
                resizeMode: 'cover',
                justifyContent: 'flex-end',
              }}
            >
              <View
                style={{
                  backgroundColor: 'rgba(255,255,255,0.6)',
                  height: 40,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <Text style={{ color: primaryDark, fontSize: 18 }}>
                  صـورت‌ حسـاب
                </Text>
              </View>
            </Image>
          </TouchableOpacity>
          <View style={{ flex: 1, elevation: 2 }}>
            <TouchableOpacity
              style={{ marginBottom: 4, flex: 3 }}
              activeOpacity={0.4}
              onPress={onFacilityPress}
            >
              <Image
                source={images.facility_icon}
                style={{
                  width: (width - 12) / 2,
                  flex: 1,
                  resizeMode: 'cover',
                  justifyContent: 'flex-end',
                }}
              >
                <View
                  style={{
                    backgroundColor: 'rgba(255,255,255,0.6)',
                    height: 40,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <Text style={{ color: primaryDark, fontSize: 18 }}>
                    امکانات رفاهی
                  </Text>
                </View>
              </Image>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ flex: 2, elevation: 2 }}
              activeOpacity={0.4}
              onPress={onInformsPress}
            >
              <Image
                source={images.info_icon}
                style={{
                  width: (width - 12) / 2,
                  flex: 1,
                  resizeMode: 'stretch',
                  justifyContent: 'flex-end',
                }}
              >
                <View
                  style={{
                    backgroundColor: 'rgba(255,255,255,0.6)',
                    height: 40,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <Text style={{ color: primaryDark, fontSize: 18 }}>
                    اعلانات
                  </Text>
                </View>
              </Image>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
