import React, { Component } from 'react';
import { View, FlatList, Text, TouchableOpacity } from 'react-native';
import { observer } from 'mobx-react/native';
import { observable } from 'mobx';

import {
  ContactCard,
  Toolbar,
  Overlay,
  LoadingPopUp,
  AndroidBackButton,
} from '../components';
import images from '@assets/images';
import { apartmentStore } from '../stores';
import { primaryDark } from '../constants/colors';
import { deletePhoneBookQuery, phoneBookQuery } from '../network/Queries';

class DeletePopUp extends Component {
  render() {
    const { onConfirm, onDismiss, message } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          paddingTop: 16,
          paddingBottom: 8,
          paddingHorizontal: 8,
          width: 300,
          height: 120,
          justifyContent: 'space-between',
          borderRadius: 4,
        }}
      >
        <Text style={{ textAlign: 'left' }}>{message}</Text>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            onPress={onConfirm}
            style={{ paddingHorizontal: 16 }}
          >
            <Text style={{ color: primaryDark }}>تایید</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={onDismiss}
            style={{ paddingHorizontal: 16 }}
          >
            <Text style={{ color: primaryDark }}>انصراف</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

@observer
export default class Contacts extends Component {
  componentWillMount() {
    phoneBookQuery(apartmentStore.apartmentID);
  }

  constructor() {
    super();
    this.state = {
      selectedPhone: null,
      showOverlay: false,
      nominatedToDeleteItem: null,
    };
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <AndroidBackButton
          onPress={() => {
            if (this.state.loading) {
              return true;
            } else if (this.state.showOverlay) {
              this.dismissOverlay();
            } else if (this.state.selectedPhone) {
              this.setState({ selectedPhone: null });
            }
            return true;
          }}
        />
        {this.state.selectedPhone ? (
          <ContactCard
            contact={apartmentStore.phoneBook.find(
              item => item.Tel == this.state.selectedPhone,
            )}
            onPress={tel => this.onCardPress(tel)}
            selectedPhone={this.state.selectedPhone}
          />
        ) : (
          <View style={{ marginTop: 4 }}>
            <FlatList
              data={apartmentStore.phoneBook}
              extraData={this.state.selectedPhone}
              renderItem={({ item }) => (
                <ContactCard
                  contact={item}
                  onPress={tel => this.onCardPress(tel)}
                  onLongPress={item => {
                    if (
                      apartmentStore.userRole == 'Admin' ||
                      apartmentStore.userRole == 'Manager'
                    ) {
                      this.setState({
                        nominatedToDeleteItem: item,
                        showOverlay: true,
                      });
                    }
                  }}
                />
              )}
              keyExtractor={(item, index) => index}
              initialNumToRender={2}
              disableVirtualization={true}
            />
          </View>
        )}
        {this.state.showOverlay && (
          <Overlay catchTouch={true} onPress={() => this.dismissOverlay()}>
            <DeletePopUp
              message={`آیا از حذف ${this.state.nominatedToDeleteItem
                .Name} مطمئن هستید؟`}
              onConfirm={item => this.confirmDelete(item)}
              onDismiss={this.dismissOverlay.bind(this)}
            />
          </Overlay>
        )}
        {this.state.loading && (
          <Overlay>
            <LoadingPopUp message="در حال حذف آدرس ..." />
          </Overlay>
        )}
      </View>
    );
  }

  onBackPress() {
    this.props.navigation.goBack();
  }

  async confirmDelete() {
    this.setState({ loading: true });
    const {
      ID,
      Name,
      Address,
      Tel,
      Lat,
      Lng,
    } = this.state.nominatedToDeleteItem;
    //
    const item = {
      apartment_ID: apartmentStore.apartmentID,
      name: Name,
      address: Address,
      tel: Tel,
      lat: Lat,
      lng: Lng,
      id: ID,
      isDisabled: true,
    };
    const removePhoneBookRes = await deletePhoneBookQuery(ID);
    if (removePhoneBookRes) {
      this.setState({
        nominatedToDeleteItem: null,
        showOverlay: false,
      });
    } else {
      //TODO: show proper error
    }
    this.setState({ loading: false });
  }

  dismissOverlay() {
    this.setState({
      showOverlay: false,
    });
  }

  onCardPress(phone) {
    if (phone == this.state.selectedPhone) {
      this.setState({ selectedPhone: null });
    } else {
      this.setState({ selectedPhone: phone });
    }
  }
}
