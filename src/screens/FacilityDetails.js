import React, { Component } from 'react';
import {
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  Dimensions,
} from 'react-native';
import { observer } from 'mobx-react/native';

import {
  FacilityCard,
  Toolbar,
  Fab,
  Overlay,
  AddFacilityPopUp,
  FacilityDetailsCard,
} from '../components';
import images from '@assets/images';
import { primaryDark, fabColor } from '../constants/colors';
import { getFacilityDetailsQuery } from '../network/Queries';
import { apartmentStore } from '../stores';
import { facilityStatusTypes, dayOfWeek } from '../constants/values';

const { height, width } = Dimensions.get('window');

class ScheduleCard extends Component {
  render() {
    const {
      onPress,
      day,
      generalSansCount,
      specialSansCount,
      offSansCount,
      fitToWidth,
    } = this.props;
    return (
      <TouchableOpacity onPress={() => onPress(day)}>
        <View
          style={{
            width: width / 2 - 16,
            height: height / 4 - 36,
            backgroundColor: 'white',
            elevation: 2,
            borderRadius: 8,
            borderTopLeftRadius: 16,
          }}
        >
          <View
            style={{
              flex: 2,
              borderTopLeftRadius: 16,
              backgroundColor: primaryDark,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            <Text style={{ fontSize: 16, color: 'white' }}>{day.label}</Text>
            <View
              pointerEvents="box-none"
              style={{ ...StyleSheet.absoluteFillObject, flex: 1, padding: 6 }}
            >
              <Image
                source={images.pin_omid}
                style={{
                  height: 14,
                  width: 14,
                  tintColor: 'black',
                  resizeMode: 'cover',
                }}
              />
            </View>
          </View>
          <View
            style={{
              flex: 3,
              paddingVertical: 4,
              paddingHorizontal: 8,
              alignItems: 'center',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}
          >
            <View style={{ alignItems: 'center', paddingHorizontal: 4 }}>
              <View
                style={{
                  paddingBottom: 2,
                  borderBottomWidth: 1,
                  borderBottomColor: primaryDark,
                }}
              >
                <Text style={{ fontSize: 12 }}>عمومی</Text>
              </View>
              <Text style={{ color: 'black', fontSize: 24 }}>
                {generalSansCount}
              </Text>
            </View>
            <View />
            <View
              style={{
                alignItems: 'center',
                paddingHorizontal: 4,
                borderRightWidth: 1,
                borderRightColor: primaryDark,
              }}
            >
              <View
                style={{
                  paddingBottom: 2,
                  borderBottomWidth: 1,
                  borderBottomColor: primaryDark,
                }}
              >
                <Text style={{ fontSize: 12 }}>خصوصی</Text>
              </View>
              <Text style={{ color: 'black', fontSize: 24 }}>
                {specialSansCount}
              </Text>
            </View>
            <View
              style={{
                alignItems: 'center',
                paddingHorizontal: 4,
                borderRightWidth: 1,
                borderRightColor: primaryDark,
              }}
            >
              <View
                style={{
                  paddingBottom: 2,
                  borderBottomWidth: 1,
                  borderBottomColor: primaryDark,
                }}
              >
                <Text style={{ fontSize: 12 }}>تعطیلات</Text>
              </View>
              <Text style={{ color: 'black', fontSize: 24 }}>
                {offSansCount}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

@observer
export default class FacilityDetails extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
    };
  }

  componentWillMount() {
    this.getAllSans();
  }

  async getAllSans() {
    const facilityDetailsRes = await getFacilityDetailsQuery(
      apartmentStore.apartmentID,
      this.props.navigation.state.params.id,
    );
    if (facilityDetailsRes.status) {
      this.setState({ data: facilityDetailsRes.data });
    } else {
      //TODO: show proper error
    }
  }

  render() {
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: `برنامه ${this.props.navigation.state.params.title}`,
      },
    };
    return (
      <View style={{ flex: 1 }}>
        <Toolbar customStyle={toolbarStyle} />
        <View
          style={{
            flexDirection: 'row',
            paddingVertical: 8,
            paddingHorizontal: 8,
            flex: 1,
            justifyContent: 'space-between',
          }}
        >
          <ScheduleCard
            day={dayOfWeek[0]}
            generalSansCount={this.getSansCount(dayOfWeek[0].value, 'GENERAL')}
            specialSansCount={this.getSansCount(dayOfWeek[0].value, 'SPECIAL')}
            offSansCount={this.getSansCount(dayOfWeek[0].value, 'OFF')}
            onPress={day => this.onDayPress(day)}
          />
          <ScheduleCard
            day={dayOfWeek[1]}
            generalSansCount={this.getSansCount(dayOfWeek[1].value, 'GENERAL')}
            specialSansCount={this.getSansCount(dayOfWeek[1].value, 'SPECIAL')}
            offSansCount={this.getSansCount(dayOfWeek[1].value, 'OFF')}
            onPress={day => this.onDayPress(day)}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            paddingVertical: 8,
            justifyContent: 'space-between',
            paddingHorizontal: 8,
            flex: 1,
          }}
        >
          <ScheduleCard
            day={dayOfWeek[2]}
            generalSansCount={this.getSansCount(dayOfWeek[2].value, 'GENERAL')}
            specialSansCount={this.getSansCount(dayOfWeek[2].value, 'SPECIAL')}
            offSansCount={this.getSansCount(dayOfWeek[2].value, 'OFF')}
            onPress={day => this.onDayPress(day)}
          />
          <ScheduleCard
            day={dayOfWeek[3]}
            generalSansCount={this.getSansCount(dayOfWeek[3].value, 'GENERAL')}
            specialSansCount={this.getSansCount(dayOfWeek[3].value, 'SPECIAL')}
            offSansCount={this.getSansCount(dayOfWeek[3].value, 'OFF')}
            onPress={day => this.onDayPress(day)}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            paddingVertical: 8,
            justifyContent: 'space-between',
            paddingHorizontal: 8,
            flex: 1,
          }}
        >
          <ScheduleCard
            day={dayOfWeek[4]}
            generalSansCount={this.getSansCount(dayOfWeek[4].value, 'GENERAL')}
            specialSansCount={this.getSansCount(dayOfWeek[4].value, 'SPECIAL')}
            offSansCount={this.getSansCount(dayOfWeek[4].value, 'OFF')}
            onPress={day => this.onDayPress(day)}
          />
          <ScheduleCard
            day={dayOfWeek[5]}
            generalSansCount={this.getSansCount(dayOfWeek[5].value, 'GENERAL')}
            specialSansCount={this.getSansCount(dayOfWeek[5].value, 'SPECIAL')}
            offSansCount={this.getSansCount(dayOfWeek[5].value, 'OFF')}
            onPress={day => this.onDayPress(day)}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            paddingVertical: 8,
            justifyContent: 'center',
            paddingHorizontal: 8,
            flex: 1,
          }}
        >
          <ScheduleCard
            day={dayOfWeek[6]}
            generalSansCount={this.getSansCount(dayOfWeek[6].value, 'GENERAL')}
            specialSansCount={this.getSansCount(dayOfWeek[6].value, 'SPECIAL')}
            offSansCount={this.getSansCount(dayOfWeek[6].value, 'OFF')}
            onPress={day => this.onDayPress(day)}
          />
        </View>
      </View>
    );
  }

  onBackPress() {
    this.props.navigation.goBack();
  }

  onDayPress(day) {
    this.props.navigation.navigate('AddSans', {
      day: day,
      facilityName: this.props.navigation.state.params.title,
      id: this.props.navigation.state.params.id,
      updateFacilityDetails: () => this.getAllSans(),
    });
  }

  getSansCount(day, type) {
    const data = this.state.data.filter(
      item =>
        item.DayOfWeek == day &&
        item.Status == facilityStatusTypes.find(item => item.value == type).id,
    );
    if (data) {
      return data.length;
    }
    return 0;
  }
}
