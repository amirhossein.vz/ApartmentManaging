import React, { Component } from 'react';
import { View, FlatList, Text, TouchableOpacity } from 'react-native';
import { observer } from 'mobx-react/native';
import { observable } from 'mobx';

import { ContactCard, Toolbar, Overlay } from '../components';
import images from '@assets/images';
import { apartmentStore } from '../stores';
import { primaryDark } from '../constants/colors';

class NotificationRow extends Component {
  render() {
    return (
      <View
        style={{
          backgroundColor: 'white',
          margin: 4,
          elevation: 2,
          padding: 8,
        }}
      >
        <Text>{this.props.item.item.Text}</Text>
      </View>
    );
  }
}
@observer
export default class Notifications extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          data={apartmentStore.notification}
          renderItem={item => <NotificationRow item={item} />}
        />
      </View>
    );
  }
}
