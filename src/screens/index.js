import Main from './Main';
import Splash from './Splash';
import Login from './Login';
import Contacts from './Contacts';
import ContactUs from './ContactUs';
import Facilities from './Facilities';
import Informs from './Informs';
import AddUnit from './AddUnit';
import Units from './Units';
import AddCost from './AddCost';
import Costs from './Costs';
import FacilityDetails from './FacilityDetails';
import AddSans from './AddSans';
import MainContent from './MainContent';
import Suggestion from './Suggestion';
import Lobby from './Lobby';
import Rules from './Rules';
import AddContact from './AddContact';
import Reports from './Reports';
import Notifications from './Notifications';
import FacilityManaging from './FacilityManaging';
import NonSchedule from './NonSchedule';
import Requests from './Requests';
import SelectApartment from './SelectApartment';

export {
  Main,
  Splash,
  Login,
  Contacts,
  ContactUs,
  Facilities,
  Informs,
  AddUnit,
  Units,
  AddCost,
  Costs,
  FacilityDetails,
  AddSans,
  MainContent,
  Suggestion,
  Lobby,
  Rules,
  AddContact,
  Reports,
  Notifications,
  FacilityManaging,
  NonSchedule,
  Requests,
  SelectApartment,
};
