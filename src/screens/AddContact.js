import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Keyboard,
  ToastAndroid,
  Platform,
} from 'react-native';
import { observable } from 'mobx';
import { observer } from 'mobx-react/native';
import MapView, { Marker } from 'react-native-maps';
import { MKTextField } from 'react-native-material-kit';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import images from '@assets/images';
import { primaryDark, placeholderTextColor } from '../constants/colors';
import {
  Toolbar,
  Overlay,
  LoadingPopUp,
  AndroidBackButton,
} from '../components';
import { apartmentStore } from '../stores';
import { addPhoneBookQuery } from '../network/Queries';

class NewContactBottomSheetRow extends Component {
  render() {
    const {
      iconImage,
      placeholder,
      onChange,
      value,
      numericKeyPad,
      title,
      isEssential,
      haveEmptyField,
    } = this.props;
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginBottom: 8,
        }}
      >
        <Image
          source={iconImage}
          style={{
            height: 16,
            width: 16,
            marginHorizontal: 8,
            marginVertical: 7,
            tintColor: primaryDark,
          }}
        />
        <Text
          style={{
            flex: 0.3,
            color: primaryDark,
            fontSize: 12,
            textAlign: 'left',
          }}
        >
          {title}
        </Text>
        <MKTextField
          ref="textInput"
          multiline={false}
          keyboardType={numericKeyPad ? 'phone-pad' : 'default'}
          floatingLabelEnable={false}
          tintColor={
            haveEmptyField && value == '' ? 'red' : placeholderTextColor
          }
          textInputStyle={{
            color: 'black',
            fontSize: 12,
            textAlign: 'center',
          }}
          underlineSize={1}
          placeholder={placeholder}
          style={{
            flex: 6,
            marginHorizontal: 16,
            flexGrow: 1,
          }}
          allowFontScaling={true}
          multiline={false}
          onChangeText={text => onChange(text)}
          value={value}
          underlineEnabled={true}
          highlightColor={primaryDark}
        />
      </View>
    );
  }
}

class NewContactBottomSheet extends Component {
  render() {
    const {
      onChangeName,
      onChangeAddress,
      onChangeTel,
      name,
      address,
      tel,
      onConfirm,
      haveEmptyField,
    } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 4,
          paddingTop: 8,
          margin: 4,
        }}
      >
        <NewContactBottomSheetRow
          iconImage={images.userIcon}
          placeholder="امید عباسپور"
          onChange={onChangeName}
          value={name}
          title="نام"
          isEssential={true}
          haveEmptyField={haveEmptyField}
        />
        <NewContactBottomSheetRow
          iconImage={images.phone_icon}
          placeholder="۰۹۱۲ *******"
          onChange={onChangeTel}
          value={tel}
          title="شماره تماس"
          numericKeyPad
          isEssential={true}
          haveEmptyField={haveEmptyField}
        />
        <NewContactBottomSheetRow
          iconImage={images.address_icon}
          placeholder="خیابان سوم شهریور"
          onChange={onChangeAddress}
          value={address}
          title="آدرس"
          isEssential={true}
          haveEmptyField={haveEmptyField}
        />
        <TouchableOpacity
          style={{
            height: 48,
            backgroundColor: primaryDark,
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: 16,
          }}
          onPress={onConfirm}
        >
          <Text style={{ color: 'white', fontSize: 16 }}>ذخیره</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

@observer
export default class AddContact extends Component {
  @observable
  region = {
    latitude: 35.6892,
    longitude: 51.389,
    latitudeDelta: 0.002,
    longitudeDelta: 0.002,
  };
  @observable marker = null;

  constructor() {
    super();
    this.state = {
      currentStep: 0,
      name: '',
      tel: '',
      address: '',
      loading: false,
      haveEmptyField: false,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.myLocation();
    }, 1000);
  }

  render() {
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: 'افزودن آدرس جدید',
      },
    };
    return (
      <View style={{ flex: 1 }}>
        <Toolbar customStyle={toolbarStyle} />
        <AndroidBackButton
          onPress={() => {
            if (this.state.loading) {
              return true;
            } else if (this.state.currentStep == 1) {
              this.setState({ currentStep: 0 });
            } else {
              this.onBackPress();
            }
            return true;
          }}
        />
        <View style={{ flex: 1 }}>
          <View style={{ flex: 1 }}>
            <MapView
              ref="map"
              style={{ flex: 1 }}
              showsUserLocation={this.state.currentStep == 0}
              showsMyLocationButton={false}
              initialRegion={this.region}
              region={this.region}
              onRegionChange={this.onRegionChange.bind(this)}
              scrollEnabled={this.state.currentStep == 0}
              zoomEnabled={this.state.currentStep == 0}
              rotateEnabled={false}
              pitchEnabled={false}
              loadingEnabled={false}
            >
              {this.state.currentStep == 0 ? null : (
                <Marker coordinate={this.marker}>
                  <Image
                    source={images.address_icon}
                    style={{ height: 48, width: 48, tintColor: primaryDark }}
                  />
                </Marker>
              )}
            </MapView>
            {
              <View
                pointerEvents="box-none"
                style={{
                  flex: 1,
                  ...StyleSheet.absoluteFillObject,
                  alignItems: 'flex-start',
                  justifyContent: 'flex-end',
                }}
              >
                <TouchableOpacity onPress={this.myLocation.bind(this)}>
                  <View
                    style={{
                      backgroundColor: 'rgba(255,255,255,0.8)',
                      padding: 12,
                      borderRadius: 36,
                      margin: 16,
                    }}
                  >
                    <Image
                      source={images.location_icon}
                      style={{ tintColor: primaryDark, height: 24, width: 24 }}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            }
            {this.state.currentStep == 1 && (
              <Overlay
                catchTouch={true}
                onPress={() => this.setState({ currentStep: 0 })}
              />
            )}
          </View>
          {this.state.currentStep == 0 && (
            <View
              pointerEvents="box-none"
              style={{
                ...StyleSheet.absoluteFillObject,
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <TouchableOpacity onPress={this.onMarkerPress.bind(this)}>
                <Image
                  source={images.address_icon}
                  style={{
                    height: 48,
                    width: 48,
                    resizeMode: 'contain',
                    tintColor: primaryDark,
                    marginBottom: 48,
                  }}
                />
              </TouchableOpacity>
            </View>
          )}
        </View>
        {this.state.currentStep == 1 && (
          <NewContactBottomSheet
            onChangeName={text => this.setState({ name: text })}
            onChangeAddress={text => this.setState({ address: text })}
            onChangeTel={text => this.setState({ tel: text })}
            name={this.state.name}
            address={this.state.address}
            tel={this.state.tel}
            onConfirm={this.saveContact.bind(this)}
            haveEmptyField={this.state.haveEmptyField}
          />
        )}
        {this.state.loading && (
          <Overlay>
            <LoadingPopUp message="در حال ذخیره آدرس جدید ..." />
          </Overlay>
        )}
        {Platform.OS == 'ios' && <KeyboardSpacer />}
      </View>
    );
  }

  onBackPress() {
    this.props.navigation.goBack();
  }

  async saveContact() {
    Keyboard.dismiss();
    if (
      this.state.name == '' ||
      this.state.tel == '' ||
      this.state.address == ''
    ) {
      this.setState({ haveEmptyField: true });
      ToastAndroid.show('پر کردن همه فیلدها ضروری می‌باشد', ToastAndroid.LONG);

      return;
    }
    this.setState({ loading: true });
    const item = {
      apartment_ID: apartmentStore.apartmentID,
      name: this.state.name,
      address: this.state.address,
      tel: this.state.tel,
      lat: this.marker.latitude,
      lng: this.marker.longitude,
    };
    const addPhoneBookRes = await addPhoneBookQuery(item);
    this.setState({ loading: false });
    if (addPhoneBookRes) {
      this.onBackPress();
    } else {
      //TODO: show proper error
    }
  }
  onRegionChange(region) {
    this.region = region;
  }

  onMarkerPress() {
    this.marker = {
      latitude: this.region.latitude,
      longitude: this.region.longitude,
    };
    this.setState({ currentStep: this.state.currentStep + 1 });
  }

  async myLocation() {
    navigator.geolocation.getCurrentPosition(
      position => {
        const { latitude, longitude } = position.coords;
        let region = {
          latitude,
          longitude,
          latitudeDelta: 0.005,
          longitudeDelta: 0.005,
        };
        try {
          this.refs.map.animateToRegion(region);
        } catch (e) {
          return;
        }
      },
      error => null,
      { enableHighAccuracy: false, timeout: 20000, maximumAge: 1 * 60 * 1000 }, //TODO: Check later
    );
  }
}
