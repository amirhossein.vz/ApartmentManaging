import React, { Component } from 'react';
import {
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  Text,
  Keyboard,
  Platform,
} from 'react-native';
import { observer } from 'mobx-react/native';
import { phonecall } from 'react-native-communications';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import {
  FacilityCard,
  Toolbar,
  Fab,
  Overlay,
  AddFacilityPopUp,
  LoadingPopUp,
  AndroidBackButton,
} from '../components';
import images from '@assets/images';
import { primaryDark } from '../constants/colors';
import { apartmentStore } from '../stores';
import { addFacilityQuery } from '../network/Queries';

class DeletePopUp extends Component {
  render() {
    const { onConfirm, onDismiss, message, confirmationMessage } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          paddingTop: 16,
          paddingBottom: 8,
          paddingHorizontal: 8,
          width: 300,
          height: 160,
          justifyContent: 'space-between',
          borderRadius: 4,
        }}
      >
        <View>
          <Text style={{ color: 'black' }}>{message}</Text>
          <Text>{confirmationMessage}</Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            onPress={onConfirm}
            style={{ paddingHorizontal: 16 }}
          >
            <Text style={{ color: primaryDark }}>تایید</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={onDismiss}
            style={{ paddingHorizontal: 16 }}
          >
            <Text style={{ color: primaryDark }}>انصراف</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

class ShowPressPopUp extends Component {
  render() {
    const { onDelete, onCall } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          borderRadius: 2,
          width: 250,
          height: 100,
          padding: 8,
        }}
      >
        <TouchableOpacity
          style={{ paddingVertical: 8, paddingHorizontal: 8 }}
          onPress={onDelete}
        >
          <Text style={{ color: 'black' }}>حذف</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{ paddingVertical: 8, paddingHorizontal: 8 }}
          onPress={onCall}
        >
          <Text style={{ color: 'black' }}>تماس</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
///////////////MOCK DATA MUST BE DELETE//////////

const facilityTypes = [
  { value: 'Pool', ID: 1, label: 'استخر', image: 'pool.jpg' },
  { value: 'RoofGarden', ID: 2, label: 'روف گاردن', image: 'roof.jpg' },
  { value: 'Billiard', ID: 4, label: 'بیلیارد', image: 'billiard.jpg' },
  { value: 'Tennis', ID: 3, label: 'زمین تنیس', image: 'tennis.jpg' },
];

@observer
export default class Facilities extends Component {
  state = {
    showOverlay: false,
    showFacilityPopUp: false,
    showPickerPopUp: false,
    showOnLongPressPopUp: false,
    showDeleteConfirmation: false,
    nominatedToDeleteItem: null,
    nominatedToEditItem: null,
    loading: false,
    loadingMessage: '',
  };

  render() {
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: 'امکانات رفاهی',
      },
    };
    return (
      <View style={{ flex: 1 }}>
        <Toolbar customStyle={toolbarStyle} />
        <AndroidBackButton
          onPress={() => {
            if (this.state.loading) {
              return true;
            } else if (this.state.showDeleteConfirmation) {
              this.setState({
                showDeleteConfirmation: false,
                showOnLongPressPopUp: true,
              });
            } else if (this.state.showOnLongPressPopUp) {
              this.setState({
                showOnLongPressPopUp: false,
                showOverlay: false,
              });
            } else if (this.state.showPickerPopUp) {
              this.setState({
                showPickerPopUp: false,
                showFacilityPopUp: true,
              });
            } else if (this.state.showFacilityPopUp) {
              this.setState({ showFacilityPopUp: false, showOverlay: false });
            } else {
              this.onBackPress();
            }
            return true;
          }}
        />
        <FlatList
          keyExtractor={(item, index) => index}
          data={apartmentStore.facility}
          renderItem={({ item }) => (
            <FacilityCard
              onPress={this.goToDetails.bind(this)}
              onLongPress={item => {
                if (
                  apartmentStore.userRole == 'Admin' ||
                  apartmentStore.userRole == 'Manager'
                ) {
                  this.onCardLongPress(item);
                }
              }}
              data={item}
            />
          )}
        />
        {(apartmentStore.userRole == 'Admin' ||
          apartmentStore.userRole == 'Manager') && (
          <View
            pointerEvents="box-none"
            style={{
              ...StyleSheet.absoluteFillObject,
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
              flex: 1,
              padding: 16,
            }}
          >
            <Fab
              bgColor={primaryDark}
              onPress={this.showSavePanel.bind(this)}
              icon={images.plus_icon}
              iconColor="white"
            />
          </View>
        )}
        {this.state.showOverlay && (
          <Overlay
            catchTouch={true}
            onPress={() =>
              this.setState({
                showOverlay: false,
                showFacilityPopUp: false,
                showPickerPopUp: false,
                showOnLongPressPopUp: false,
                showDeleteConfirmation: false,
                nominatedToDeleteItem: null,
                nominatedToEditItem: null,
              })}
          >
            {this.state.showDeleteConfirmation ? (
              <DeletePopUp
                message={`آیا از حذف ${this.state.nominatedToDeleteItem
                  .Title} مطمئن هستید؟`}
                onConfirm={item => this.confirmDelete(item)}
                onDismiss={this.dismissSavePanel.bind(this)}
                confirmationMessage="توجه:‌ در صورت حذف، تمامی سانس‌های موجود نیز حذف خواهد شد!"
              />
            ) : this.state.showOnLongPressPopUp ? (
              <ShowPressPopUp
                onDelete={() => this.showDeletePopUp()}
                onCall={() => this.call()}
              />
            ) : (
              <View>
                <AddFacilityPopUp
                  onConfirm={this.saveNewFacility.bind(this)}
                  onPickerPress={() =>
                    this.setState({
                      showPickerPopUp: true,
                      showFacilityPopUp: false,
                    })}
                  onPickerItemPress={() =>
                    this.setState({
                      showFacilityPopUp: true,
                      showPickerPopUp: false,
                    })}
                  showPicker={this.state.showPickerPopUp}
                />
                {Platform.OS == 'ios' && <KeyboardSpacer />}
                {Platform.OS == 'ios' && <KeyboardSpacer />}
              </View>
            )}
          </Overlay>
        )}
        {this.state.loading && (
          <Overlay>
            <LoadingPopUp message={this.state.loadingMessage} />
          </Overlay>
        )}
      </View>
    );
  }

  onBackPress() {
    this.props.navigation.goBack();
  }

  showDeletePopUp() {
    this.setState({
      showOverlay: true,
      showPickerPopUp: false,
      showFacilityPopUp: false,
      showOnLongPressPopUp: false,
      showDeleteConfirmation: true,
    });
  }

  onCardLongPress(item) {
    this.setState({
      showOverlay: true,
      showFacilityPopUp: false,
      showPickerPopUp: false,
      showOnLongPressPopUp: true,
      showDeleteConfirmation: false,
      nominatedToDeleteItem: item,
      nominatedToEditItem: item,
    });
  }

  call() {
    phonecall(this.state.nominatedToEditItem.Tel, true);
  }

  async confirmDelete() {
    Keyboard.dismiss();
    this.setState({
      loading: true,
      loadingMessage: 'در حال حذف امکانات رفاهی ...',
    });
    const {
      Title,
      Image,
      IsScheduled,
      Description,
      Tel,
      ID,
    } = this.state.nominatedToDeleteItem;
    const newFacility = {
      apartment_ID: apartmentStore.apartmentID,
      title_ID: apartmentStore.facilityTypes.find(item => item.Title == Title)
        .ID,
      image: Image,
      isScheduled: true,
      description: Description,
      tel: Tel,
      id: ID,
      isDisabled: true,
    };
    const addFacilityRes = await addFacilityQuery(newFacility);
    this.setState({
      showOverlay: false,
      showPickerPopUp: false,
      showFacilityPopUp: false,
      showOnLongPressPopUp: false,
      showDeleteConfirmation: false,
      loading: false,
    });
  }

  async saveNewFacility(item) {
    Keyboard.dismiss();
    this.setState({
      loading: true,
      loadingMessage: 'در حال ذخیره امکانات رفاهی جدید ...',
    });
    const newFacility = {
      apartment_ID: apartmentStore.apartmentID,
      title_ID: item.title_ID,
      image: item.image,
      isScheduled: item.isScheduled,
      description: item.description,
      tel: item.tel,
    };
    const addFacilityRes = await addFacilityQuery(newFacility);
    this.setState({
      showOverlay: false,
      showPickerPopUp: false,
      showFacilityPopUp: false,
      showOnLongPressPopUp: false,
      showDeleteConfirmation: false,
      loading: false,
    });
  }

  showSavePanel() {
    this.setState({
      showOverlay: true,
      showFacilityPopUp: true,
      showOnLongPressPopUp: false,
      showDeleteConfirmation: false,
    });
  }

  dismissSavePanel() {
    this.setState({
      showOverlay: false,
      showPickerPopUp: false,
      showFacilityPopUp: false,
      showOnLongPressPopUp: false,
      showDeleteConfirmation: false,
    });
  }

  goToDetails(item) {
    if (this.props.navigation.state.params.nextPage == 'GENERAL') {
      this.props.navigation.navigate('FacilityDetails', {
        title: item.Title,
        id: item.ID,
      });
    } else if (this.props.navigation.state.params.nextPage == 'SPECIAL') {
      if (item.IsScheduled) {
        this.props.navigation.navigate('FacilityManaging', {
          title: item.Title,
          id: item.ID,
        });
      } else {
        this.props.navigation.navigate('NonSchedule', {
          title: item.Title,
          id: item.ID,
        });
      }
    }
  }
}
