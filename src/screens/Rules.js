import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  Keyboard,
  Dimensions,
  Platform,
} from 'react-native';
import { observer } from 'mobx-react/native';
import _ from 'lodash';
import KeyboardSpacer from 'react-native-keyboard-spacer';

const { height, width } = Dimensions.get('window');
import {
  primary,
  fabColor,
  primaryDark,
  placeholderTextColor,
  transparent,
} from '../constants/colors';
import {
  CostCard,
  Toolbar,
  Fab,
  Overlay,
  LoadingPopUp,
  AndroidBackButton,
} from '../components';
import images from '@assets/images';
import { apartmentStore } from '../stores';
import { addRuleQuery, getRuleQuery } from '../network/Queries';

class DeletePopUp extends Component {
  render() {
    const { onConfirm, onDismiss, message } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          paddingTop: 16,
          paddingBottom: 8,
          paddingHorizontal: 8,
          width: 300,
          height: 120,
          justifyContent: 'space-between',
          borderRadius: 4,
        }}
      >
        <Text style={{ textAlign: 'left' }}>{message}</Text>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            onPress={onConfirm}
            style={{ paddingHorizontal: 16 }}
          >
            <Text style={{ color: primaryDark }}>تایید</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={onDismiss}
            style={{ paddingHorizontal: 16 }}
          >
            <Text style={{ color: primaryDark }}>انصراف</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

class RulesCard extends Component {
  constructor() {
    super();
    this.state = {
      textHeight: 120,
    };
  }
  render() {
    const { Description } = this.props.item;
    return (
      <TouchableOpacity
        onLongPress={() => {
          if (
            apartmentStore.userRole == 'Admin' ||
            apartmentStore.userRole == 'Manager'
          ) {
            this.props.onLongPress(this.props.item);
          }
        }}
      >
        <View style={{ alignItems: 'center' }}>
          <Image
            source={images.rule_holder}
            style={{
              width: width - 8,
              height: this.state.textHeight + 48,
              resizeMode: 'stretch',
              tintColor: primaryDark,
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginVertical: 16,
                marginHorizontal: 32,
                backgroundColor: 'transparent',
              }}
            >
              <Text
                style={{ color: 'white', textAlign: 'left' }}
                onLayout={event => {
                  const { height } = event.nativeEvent.layout;
                  this.setState({ textHeight: height });
                }}
              >
                {Description}
              </Text>
            </View>
          </Image>
        </View>
      </TouchableOpacity>
    );
  }
}

class AddRulePopUp extends Component {
  render() {
    return (
      <View style={{ backgroundColor: 'white', elevation: 2, borderRadius: 4 }}>
        <View style={{ padding: 16 }}>
          <View style={{ height: 250 }}>
            <Text
              style={{
                fontSize: 16,
                color: primaryDark,
                marginBottom: 8,
                textAlign: 'left',
              }}
            >
              اضافه کردن قانون جدید
            </Text>
            <TextInput
              placeholder="لطفا قانون جدید ساختمان را در این‌جا وارد کنید"
              placeholderTextColor={placeholderTextColor}
              style={styles.textInput}
              multiline={true}
              underlineColorAndroid={transparent}
              onChangeText={text => this.props.onChangeText(text)}
              value={this.props.value}
              blurOnSubmit={false}
              numberOfLines={4}
              onSubmitEditing={_.debounce(
                () => this.props.onChangeText(this.props.value + '\n'),
                100,
              )}
            />
          </View>
        </View>
        <TouchableOpacity onPress={this.props.onPress}>
          <View style={{ flexDirection: 'row' }}>
            <View
              style={{
                flex: 1,
                height: 60,
                backgroundColor: primaryDark,
                alignItems: 'center',
                justifyContent: 'center',
                borderBottomLeftRadius: 4,
                borderBottomRightRadius: 4,
              }}
            >
              <Text style={{ fontSize: 18, color: 'white' }}>ذخیره</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

@observer
export default class Rules extends Component {
  constructor() {
    super();
    this.state = {
      showOverlay: false,
      newRule: '',
      addRule: false,
      deleteRule: false,
      nominatedToDeleteItem: null,
      loading: false,
      loadingMessage: '',
    };
  }

  componentWillMount() {
    getRuleQuery(apartmentStore.apartmentID);
  }

  render() {
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: 'قوانین ساختمان',
      },
      right: {
        onPress: () => this.onBackPress(),
      },
    };
    return (
      <View style={{ flex: 1 }}>
        <Toolbar customStyle={toolbarStyle} />
        <AndroidBackButton
          onPress={() => {
            if (this.state.loading) {
              return true;
            } else if (this.state.deleteRule) {
              this.setState({
                deleteRule: false,
                nominatedToDeleteItem: null,
                showOverlay: false,
              });
            } else if (this.state.addRule) {
              this.setState({
                showOverlay: false,
                newRule: '',
                addRule: false,
              });
            } else {
              this.onBackPress();
            }
            return true;
          }}
        />
        <FlatList
          keyExtractor={(item, index) => index}
          data={apartmentStore.rule}
          renderItem={({ item }) => (
            <RulesCard
              item={item}
              onLongPress={item => this.onCardLongPress(item)}
            />
          )}
          style={{ marginVertical: 2 }}
        />
        {(apartmentStore.userRole == 'Admin' ||
          apartmentStore.userRole == 'Manager') && (
          <View
            pointerEvents="box-none"
            style={{
              ...StyleSheet.absoluteFillObject,
              alignItems: 'flex-end',
              justifyContent: 'flex-end',
              flex: 1,
              padding: 16,
            }}
          >
            <Fab
              bgColor={primaryDark}
              onPress={this.showNewRulePanel.bind(this)}
              icon={images.plus_icon}
              iconColor="white"
            />
          </View>
        )}
        {this.state.showOverlay && (
          <Overlay
            catchTouch={true}
            onPress={this.dismissNewRulePanel.bind(this)}
          >
            {this.state.addRule ? (
              <View>
                <AddRulePopUp
                  onPress={this.saveNewRule.bind(this)}
                  value={this.state.newRule}
                  onChangeText={text => this.setState({ newRule: text })}
                />
                {Platform.OS == 'ios' && <KeyboardSpacer />}
              </View>
            ) : this.state.deleteRule ? (
              <DeletePopUp
                message={'آیا از حذف قانون و مقررات انتخاب شده مطمئن هستید؟'}
                onConfirm={item => this.confirmDelete(item)}
                onDismiss={this.dismissDelete.bind(this)}
              />
            ) : null}
          </Overlay>
        )}
        {this.state.loading && (
          <Overlay>
            <LoadingPopUp message={this.state.loadingMessage} />
          </Overlay>
        )}
      </View>
    );
  }

  onBackPress() {
    this.props.navigation.goBack();
  }

  showNewRulePanel() {
    this.setState({ showOverlay: true, addRule: true });
  }

  dismissNewRulePanel() {
    this.setState({ showOverlay: false, addRule: false });
  }

  dismissAll() {
    this.setState({ showOverlay: false, addRule: false, deleteRule: false });
  }

  dismissDelete() {
    this.setState({ showOverlay: false, deleteRule: false });
  }

  async saveNewRule() {
    Keyboard.dismiss();
    this.setState({ loading: true, loadingMessage: 'در حال ذخیره ...' });
    const newItem = {
      apartment_ID: apartmentStore.apartmentID,
      description: this.state.newRule,
    };
    const addNewRuleRes = await addRuleQuery(newItem);
    if (await addNewRuleRes) {
      this.setState({ showOverlay: false });
    } else {
      //TODO: show proper error
    }
    this.setState({ loading: false, newRule: '', showDeletePopUp: false });
  }

  onCardLongPress(item) {
    this.setState({
      nominatedToDeleteItem: item,
      showOverlay: true,
      deleteRule: true,
      addRule: false,
    });
  }

  async confirmDelete() {
    this.setState({ loading: true, loadingMessage: 'در حال حذف ...' });
    const { ID, Description } = this.state.nominatedToDeleteItem;
    //
    const item = {
      apartment_ID: apartmentStore.apartmentID,
      description: Description,
      id: ID,
      isDisabled: true,
    };
    const removeRuleRes = await addRuleQuery(item);
    if (removeRuleRes) {
      this.setState({
        nominatedToDeleteItem: null,
        showOverlay: false,
        deleteRule: false,
        addRule: false,
      });
    } else {
      //TODO: show proper error
    }
    this.setState({ loading: false });
  }
}

const styles = StyleSheet.create({
  textInput: {
    paddingHorizontal: 4,
    borderColor: primaryDark,
    borderWidth: 0.5,
    height: 200,
    width: 240,
    borderRadius: 2,
    fontSize: 14,
    textAlignVertical: 'top',
    textAlign: 'right',
  },
});
