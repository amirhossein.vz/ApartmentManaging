import React, { Component } from 'react';
import {
  View,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  Keyboard,
  Platform,
  ToastAndroid,
} from 'react-native';
import { observer } from 'mobx-react/native';
import { observable } from 'mobx';
import { MKTextField } from 'react-native-material-kit';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import {
  primary,
  fabColor,
  primaryDark,
  placeholderTextColor,
} from '../constants/colors';
import {
  CostCard,
  Toolbar,
  Fab,
  Overlay,
  LoadingPopUp,
  AndroidBackButton,
} from '../components';
import images from '@assets/images';
import { apartmentStore } from '../stores';
import {
  addCostQuery,
  confirmCostQuery,
  getCostQuery,
} from '../network/Queries';

const BORDER_RADIUS = 20;

class ConfirmationPopUp extends Component {
  constructor() {
    super();
    this.state = {
      title: '',
    };
  }
  render() {
    const { confirm, dismiss, message, haveEmptyField } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          paddingTop: 16,
          paddingBottom: 8,
          paddingHorizontal: 8,
          width: 300,
          height: 200,
          justifyContent: 'space-between',
          borderRadius: 4,
        }}
      >
        <Text style={{ textAlign: 'left' }}>{message}</Text>

        <Text
          style={{
            color: 'black',
            fontSize: 14,
            marginTop: 8,
            textAlign: 'left',
          }}
        >
          عنوان اعلام هزینه
        </Text>

        <MKTextField
          ref={'textInput'}
          multiline={false}
          keyboardType={'default'}
          returnKeyType={'done'}
          floatingLabelEnable={false}
          tintColor={
            haveEmptyField && this.state.title == ''
              ? 'red'
              : placeholderTextColor
          }
          textInputStyle={{
            color: 'black',
            fontSize: 14,
            textAlign: 'center',
          }}
          underlineSize={1}
          placeholder={'شارژ آبان'}
          style={{ width: 200, alignSelf: 'center', marginBottom: 16 }}
          onChangeText={text => this.setState({ title: text })}
          highlightColor={primary}
          value={this.state.title}
        />
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            onPress={() => confirm(this.state.title)}
            style={{ paddingHorizontal: 16 }}
          >
            <Text style={{ color: primaryDark }}>تایید</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={dismiss} style={{ paddingHorizontal: 16 }}>
            <Text style={{ color: primaryDark }}>انصراف</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

class DeletePopUp extends Component {
  render() {
    const { confirm, dismiss, message } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          paddingTop: 16,
          paddingBottom: 8,
          paddingHorizontal: 8,
          width: 300,
          height: 120,
          justifyContent: 'space-between',
          borderRadius: 4,
        }}
      >
        <Text style={{ textAlign: 'left' }}>{message}</Text>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity onPress={confirm} style={{ paddingHorizontal: 16 }}>
            <Text style={{ color: primaryDark }}>تایید</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={dismiss} style={{ paddingHorizontal: 16 }}>
            <Text style={{ color: primaryDark }}>انصراف</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

function Button({ text, onClick, isSelected, isRight }) {
  return (
    <View
      style={[
        styles.button,
        isSelected ? styles.selectedButton : styles.unselecetedButton,
        isRight ? styles.rightStyle : styles.leftStyle,
      ]}
    >
      <TouchableOpacity onPress={onClick} style={{ alignItems: 'center' }}>
        <Text
          style={[
            styles.text,
            isSelected ? styles.selectedText : styles.unselectedText,
          ]}
        >
          {text}
        </Text>
      </TouchableOpacity>
    </View>
  );
}

class TypeSelector extends Component {
  render() {
    const { setSwitchValue, switchValue } = this.props;
    return (
      <View
        style={{
          flexDirection: 'row',
          height: 40,
          marginVertical: 8,
          marginHorizontal: 32,
          borderRadius: BORDER_RADIUS,
          borderWidth: 1,
          borderColor: '#00c9cb',
        }}
      >
        <Button
          text={'در انتظار تائید'}
          onClick={() => setSwitchValue('NOT_CONFIRMED')}
          isSelected={switchValue === 'NOT_CONFIRMED'}
          isRight
        />
        <Button
          text={'تائید شده'}
          onClick={() => setSwitchValue('CONFIRMED')}
          isSelected={switchValue === 'CONFIRMED'}
        />
      </View>
    );
  }
}

@observer
export default class Costs extends Component {
  @observable confirmList = [];
  constructor() {
    super();
    this.state = {
      showOverlay: false,
      nominatedToDeleteItem: null,
      selectingCosts: false,
      switchValue:
        apartmentStore.userRole == 'Admin' ||
        apartmentStore.userRole == 'Manager'
          ? 'NOT_CONFIRMED'
          : 'CONFIRMED',
      loading: false,
      loadingMessage: '',
      haveEmptyField: false,
    };
  }

  componentWillMount() {
    getCostQuery(apartmentStore.apartmentID);
  }

  render() {
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: 'هزینه‌های ساختمان',
      },
      right: apartmentStore.isManager
        ? {
            onPress: () =>
              this.setState({ selectingCosts: !this.state.selectingCosts }),
            content: this.state.selectingCosts
              ? images.cancel_icon
              : images.send_icon,
          }
        : null,
    };
    console.warn(this.state.switchValue);
    return (
      <View style={{ flex: 1 }}>
        <Toolbar customStyle={toolbarStyle} />
        <AndroidBackButton
          onPress={() => {
            if (this.state.loading) {
              return true;
            } else if (this.state.showOverlay) {
              this.setState({
                showOverlay: false,
                nominatedToDeleteItem: null,
              });
            } else if (this.state.selectingCosts) {
              this.setState({ selectingCosts: false });
            } else {
              this.onBackPress();
            }
            return true;
          }}
        />
        {apartmentStore.userRole == 'Admin' ||
          (apartmentStore.userRole == 'Manager' && (
            <View style={{ backgroundColor: 'white', elevation: 1 }}>
              <TypeSelector
                setSwitchValue={type => this.setState({ switchValue: type })}
                switchValue={this.state.switchValue}
              />
            </View>
          ))}
        <FlatList
          data={
            this.state.switchValue == 'CONFIRMED'
              ? apartmentStore.costs.filter(item => item.HasConfirmed)
              : apartmentStore.costs.filter(item => !item.HasConfirmed)
          }
          extraData={this.state.selectingCosts && this.state.switchValue}
          renderItem={({ item }) => (
            <CostCard
              cost={item}
              navigateToEdit={cost => this.goToEditCost(cost)}
              deleteCost={this.showDeletePanel.bind(this)}
              selectState={this.state.selectingCosts}
              onAddCostToConfirmation={this.addToConfirmationList.bind(this)}
            />
          )}
          keyExtractor={(item, index) => index}
        />
        {apartmentStore.userRole == 'Admin' ||
        apartmentStore.userRole == 'Manager' ? (
          this.state.selectingCosts ? (
            <TouchableOpacity
              onPress={() => this.setState({ showOverlay: true })}
            >
              <View
                style={{
                  backgroundColor: primaryDark,
                  height: 48,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                <Text style={{ color: 'white', fontSize: 16 }}>
                  اعلام به اهالی
                </Text>
              </View>
            </TouchableOpacity>
          ) : (
            <View
              pointerEvents="box-none"
              style={{
                ...StyleSheet.absoluteFillObject,
                alignItems: 'flex-end',
                justifyContent: 'flex-end',
                flex: 1,
                padding: 16,
              }}
            >
              <Fab
                bgColor={primaryDark}
                onPress={this.addNewCost.bind(this)}
                icon={images.plus_icon}
                iconColor="white"
              />
            </View>
          )
        ) : null}
        {this.state.showOverlay && (
          <Overlay
            catchTouch={true}
            onPress={this.dismissDeletePanel.bind(this)}
          >
            {this.state.selectingCosts ? (
              <View>
                <ConfirmationPopUp
                  confirm={title => this.onConfirmCosts(title)}
                  dismiss={this.dismissDeletePanel.bind(this)}
                  message="آیا از اعلام هزینه به اهالی ساختمان مطمئن هستید؟!"
                  haveEmptyField={this.state.haveEmptyField}
                />
                {Platform.OS == 'ios' && <KeyboardSpacer />}
              </View>
            ) : (
              <DeletePopUp
                confirm={this.onConfirmDelete.bind(this)}
                dismiss={this.dismissDeletePanel.bind(this)}
                message={`آیا از حذف "${this.state.nominatedToDeleteItem
                  .CostName}"  مطمئن هستید؟! `}
              />
            )}
          </Overlay>
        )}
        {this.state.loading && (
          <Overlay>
            <LoadingPopUp message={this.state.loadingMessage} />
          </Overlay>
        )}
      </View>
    );
  }

  onBackPress() {
    this.props.navigation.goBack();
  }

  goToEditCost(cost) {
    this.props.navigation.navigate('AddCost', { cost: cost });
  }

  addToConfirmationList(newItem) {
    if (
      this.confirmList.length > 0 &&
      this.confirmList.find(item => item.CostHeader_ID == newItem.CostHeader_ID)
    ) {
      this.confirmList = this.confirmList.filter(
        item => item.CostHeader_ID !== newItem.CostHeader_ID,
      );
    } else {
      this.confirmList.push(newItem);
    }
  }

  showDeletePanel(title) {
    this.setState({ showOverlay: true, nominatedToDeleteItem: title });
  }

  dismissDeletePanel() {
    this.setState({ showOverlay: false });
  }

  addNewCost() {
    this.props.navigation.navigate('AddCost', {
      onConfirm: item => this.setSwitchValue(item),
    });
  }

  async onConfirmCosts(title) {
    if (title == '') {
      this.setState({ haveEmptyField: true });
      ToastAndroid.show('لطفا عنوان هزینه را وارد کنید', ToastAndroid.LONG);
      return;
    }
    this.setState({
      loading: true,
      loadingMessage: 'در حال تائید هزینه‌ها ...',
    });
    let data = '<MyTable>\n';
    for (let item of this.confirmList) {
      data = data.concat(`<MyRow>
              <ID>${item.CostHeader_ID}</ID>
      </MyRow>`);
    }
    data = data.concat('\n</MyTable>');
    const newItem = {
      title: title,
      data: data,
      apartment_ID: apartmentStore.apartmentID,
      mah: 1,
    };
    const confirmCostRes = await confirmCostQuery(newItem);
    if (confirmCostRes) {
      this.setState({
        showOverlay: false,
        nominatedToDeleteItem: null,
        selectingCosts: false,
        switchValue: 'CONFIRMED',
      });
    } else {
      //TODO: show proper error
    }
    this.setState({
      loading: false,
      loadingMessage: 'در حال تائید هزینه‌ها ...',
    });
  }

  setSwitchValue(state) {
    this.setState({ switchValue: state });
  }

  async onConfirmDelete() {
    this.setState({ loading: true, loadingMessage: 'در حال حذف هزینه ...' });
    const {
      CostType_ID,
      Description,
      TotalPrice,
      OwnerOfCost,
      ForUnit,
      CalculateType_ID,
      Apartment_ID,
      HasConfirmed,
      CostHeader_ID,
    } = this.state.nominatedToDeleteItem;
    const newItem = {
      id: CostHeader_ID,
      costType_ID: CostType_ID,
      description: Description,
      totalPrice: TotalPrice,
      ownerOfCost: OwnerOfCost,
      forUnit: ForUnit,
      calculateType_ID: CalculateType_ID,
      apartment_ID: Apartment_ID,
      hasConfirmed: HasConfirmed,
      isDisabled: true,
    };
    const deleteCostRes = await addCostQuery(newItem);
    if (deleteCostRes) {
      this.setState({
        showOverlay: false,
        nominatedToDeleteItem: null,
        selectingCosts: false,
      });
    } else {
      //TODO: show proper error
    }
    this.setState({ loading: false, loadingMessage: 'در حال حذف هزینه ...' });
  }
}

const styles = StyleSheet.create({
  button: {
    flex: 1,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  rightStyle: {
    borderBottomRightRadius: BORDER_RADIUS,
    borderTopRightRadius: BORDER_RADIUS,
  },
  leftStyle: {
    borderBottomLeftRadius: BORDER_RADIUS,
    borderTopLeftRadius: BORDER_RADIUS,
  },
  selectedButton: {
    backgroundColor: '#00c9cb',
  },
  unselecetedButton: {
    backgroundColor: 'white',
  },
  text: {
    fontSize: 14,
  },
  selectedText: {
    color: 'white',
  },
  unselectedText: { color: '#00c9cb' },
});
