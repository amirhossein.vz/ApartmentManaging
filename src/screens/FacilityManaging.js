import React, { Component } from 'react';
import {
  View,
  Text,
  Dimensions,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
const PersianCalendarPicker = require('react-native-persian-calendar-picker');
import {
  Toolbar,
  Overlay,
  AddSansRow,
  AndroidBackButton,
  AddSansPopUp,
  LoadingPopUp,
} from '../components';
import images from '@assets/images';
var moment = require('moment-jalaali');
import { primaryDark } from '../constants/colors';
import { monthOfYear, dayOfWeek } from '../constants/values';
import { getFacilityDetailsSpecialDate } from '../network/Queries';
import { apartmentStore } from '../stores';

export default class FacilityManaging extends Component {
  constructor() {
    super();
    this.state = {
      date: new Date(),
      listData: [],
      showDayPicker: false,
      showEditPanel: false,
      nominatedToEditItem: null,
    };
  }

  componentWillMount() {
    this.getFacilityDetails(this.state.date);
  }

  render() {
    const jMoment = moment(this.state.date);
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: `برنامه ${this.props.navigation.state.params.title}`,
      },
    };
    return (
      <View style={{ flex: 1 }}>
        <Toolbar customStyle={toolbarStyle} />
        <AndroidBackButton
          onPress={() => {
            if (this.state.showDayPicker) {
              this.setState({ showDayPicker: false });
            } else {
              this.onBackPress();
            }
            return true;
          }}
        />
        <TouchableOpacity
          style={{ alignItems: 'center', flexDirection: 'row' }}
          onPress={this.showDayPicker.bind(this)}
        >
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              flex: 1,
              justifyContent: 'center',
              paddingVertical: 8,
              backgroundColor: 'white',
              elevation: 2,
            }}
          >
            <Image
              source={images.next_icon}
              style={{
                height: 24,
                width: 24,
                tintColor: primaryDark,
                marginRight: 16,
              }}
            />
            <View style={{ marginVertical: 4, alignItems: 'center' }}>
              <Text style={{ color: primaryDark, fontSize: 16 }}>
                {
                  dayOfWeek.find(item => item.english == jMoment.format('dddd'))
                    .label
                }{' '}
                {jMoment.jDate()} {monthOfYear[jMoment.jMonth()].label} ماه
              </Text>
            </View>
            <Image
              source={images.previous_icon}
              style={{
                height: 24,
                width: 24,
                tintColor: primaryDark,
                marginLeft: 16,
              }}
            />
          </View>
        </TouchableOpacity>
        <FlatList
          data={this.state.data}
          style={{ marginTop: 8 }}
          keyExtractor={(item, index) => index}
          renderItem={({ item }) => (
            <AddSansRow
              data={item}
              keyExtractor={(item, index) => index}
              onLongPress={item => this.showEditPanel(item)}
            />
          )}
        />
        {this.state.showDayPicker && (
          <Overlay catchTouch={true} onPress={this.dismissDayPicker.bind(this)}>
            <View style={{ backgroundColor: 'white', elevation: 4 }}>
              <PersianCalendarPicker
                selectedDate={this.state.date}
                onDateChange={this.onDateChange.bind(this)}
                screenWidth={Dimensions.get('window').width}
                selectedBackgroundColor={'#5ce600'}
              />
            </View>
          </Overlay>
        )}
        {this.state.showEditPanel && (
          <Overlay catchTouch={true} onPress={this.dismissSavePanel.bind(this)}>
            <AddSansPopUp
              edit
              item={this.state.nominatedToEditItem}
              onPress={this.dismissSavePanel.bind(this)}
              onRetrieveData={() => this.getFacilityDetails(this.state.date)}
              onChangeLoading={state => this.changeLoadingState(state)}
              specialDate={this.state.date}
              id={this.props.navigation.state.params.id}
            />
          </Overlay>
        )}
        {this.state.loading && (
          <Overlay>
            <LoadingPopUp message="در حال ثبت تغییرات ..." />
          </Overlay>
        )}
      </View>
    );
  }

  async onDateChange(date) {
    await this.setState({
      date: date,
      showDayPicker: false,
      showEditPanel: false,
    });
    this.getFacilityDetails(date);
  }

  changeLoadingState(state) {
    this.setState({ loading: state });
  }

  onBackPress() {
    this.props.navigation.goBack();
  }

  dismissDayPicker() {
    this.setState({ showDayPicker: false, showEditPanel: false });
  }

  showDayPicker() {
    this.setState({ showDayPicker: true, showEditPanel: false });
  }

  showEditPanel(item) {
    this.setState({
      showEditPanel: true,
      showDayPicker: false,
      nominatedToEditItem: item,
    });
  }

  dismissSavePanel() {
    this.setState({
      showDayPicker: false,
      showEditPanel: false,
      nominatedToEditItem: null,
    });
  }

  async getFacilityDetails(date) {
    const jMoment = moment(date);
    date.setHours(6);
    const queryRes = await getFacilityDetailsSpecialDate(
      {
        specialDate: date,
      },
      apartmentStore.apartmentID,
      this.props.navigation.state.params.id,
    );
    if (queryRes.status) {
      this.setState({ data: queryRes.data });
    } else {
      //TODO: show proper error
    }
  }
}
