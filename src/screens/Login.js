import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  Keyboard,
  ToastAndroid,
  TouchableWithoutFeedback,
  Platform,
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import KeyboardSpacer from 'react-native-keyboard-spacer';

import images from '@assets/images';
import {
  LoginInput,
  LoginButton,
  Overlay,
  LoadingPopUp,
  AndroidBackButton,
} from '../components';
import { primaryBg } from '../constants/colors';
import { fetchFactory } from '../utils';
import { persistStore, apartmentStore } from '../stores';
import {
  loginQuery,
  apartmentDataQuery,
  unitsQuery,
  phoneBookQuery,
  getContactUsQuery,
  getSuggestionQuery,
  getNotificationQuery,
  getFacilityQuery,
  getFacilityDetailsQuery,
  userInfoQuery,
  addContactUsQuery,
  getRuleQuery,
  addRuleQuery,
  getCostQuery,
  getSurveyQuery,
  getNoticeBoard,
  getCostTypeQuery,
  getInvoiceQuery,
  getFacilityTitles,
  getContactUsTypesQuery,
  getRequestQuery,
} from '../network/Queries';

export default class Login extends Component {
  constructor() {
    super();
    this.state = {
      username: '',
      password: '',
      showOverlay: false,
      showError: false,
    };
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: primaryBg }}>
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View style={{ flex: 1 }}>
            <Image
              source={images.loginBg}
              style={{
                flex: 1,
                resizeMode: 'contain',
                height: null,
                width: null,
                justifyContent: 'flex-end',
              }}
            >
              <LoginInput
                icon={images.userIcon}
                placeHolder="شماره تماس"
                value={this.state.username}
                onChangeText={text => this.setState({ username: text })}
              />
              <View
                style={{
                  marginTop: 16,
                  marginBottom: 16,
                  alignItems: 'center',
                }}
              >
                <LoginInput
                  icon={images.passIcon}
                  placeHolder="رمز عبور"
                  password={true}
                  value={this.state.password}
                  onChangeText={text => this.setState({ password: text })}
                />
                {this.state.showError && (
                  <Text style={{ color: 'red', fontSize: 14, marginTop: 4 }}>
                    شماره تماس یا رمز عبور اشتباه است
                  </Text>
                )}
              </View>
              <View style={{ marginBottom: 32 }}>
                <LoginButton
                  title="ورود کاربر"
                  onPress={this.onLogin.bind(this)}
                />
              </View>
              {Platform.OS == 'ios' && <KeyboardSpacer />}
            </Image>
            {this.state.showOverlay && (
              <Overlay>
                <LoadingPopUp login={true} message="در حال ورود کاربر ..." />
              </Overlay>
            )}
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }

  async onLogin() {
    Keyboard.dismiss();
    this.setState({ showOverlay: true });
    try {
      const loginRes = await loginQuery(
        this.state.username,
        this.state.password,
      );
      const userInfoRes = await userInfoQuery();
      if ((await userInfoRes) && apartmentStore.userRole == 'Admin') {
        console.warn('here');
        this.setState({ showOverlay: false });
        this.props.navigation.navigate('SelectApartment');
        return;
      }
      console.warn('here 2');
      const apartmentDataRes = await apartmentDataQuery();
      const phoneBookRes = await phoneBookQuery(apartmentStore.apartmentID);
      const facilityRes = await getFacilityQuery(apartmentStore.apartmentID);
      const suggestionRes = await getSuggestionQuery(
        apartmentStore.apartmentID,
      );
      const ruleRes = await getRuleQuery(apartmentStore.apartmentID);
      const notifRes = await getNotificationQuery();
      const contactUsRes = await getContactUsQuery(apartmentStore.apartmentID);
      const costRes = await getCostQuery(apartmentStore.apartmentID);
      const noticeRes = await getNoticeBoard(apartmentStore.apartmentID);
      const surveyRes = await getSurveyQuery(apartmentStore.apartmentID);
      const costTypeRes = await getCostTypeQuery();
      const invoiceRes = await getInvoiceQuery();
      const facilityTypesRes = await getFacilityTitles();
      const contactUsTypesRes = await getContactUsTypesQuery();
      const requestRes = await getRequestQuery(apartmentStore.apartmentID);
      // for (let i = 0; i < apartmentStore.facility.length; i++) {
      //   let facilityDetailsRes = await getFacilityDetailsQuery(
      //     apartmentStore.apartmentID,
      //     '1',
      //   );
      let unitRes;
      if (
        apartmentStore.userRole == 'Manager' ||
        apartmentStore.userRole == 'Admin'
      ) {
        unitRes = await unitsQuery(apartmentStore.apartmentID);
      }

      if ((await userInfoRes) && (await apartmentDataRes)) {
        if (
          apartmentStore.userRole == 'Manager' ||
          apartmentStore.userRole == 'Admin'
        ) {
          if (await unitRes) {
            this.setState({ showOverlay: false });
            Keyboard.dismiss();
            this.navigateToMain();
          } else {
            // TODO: Show error
          }
        } else {
          this.setState({ showOverlay: false });
          Keyboard.dismiss();
          this.navigateToMain();
        }
      } else {
        this.setState({ showOverlay: false, showError: true });

        Keyboard.dismiss();

        // TODO: Show error
      }
    } catch (error) {
      this.setState({ showOverlay: false, showError: true });
    }
  }

  navigateToMain() {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Main' })],
    });
    this.props.navigation.dispatch(resetAction);
  }
}
