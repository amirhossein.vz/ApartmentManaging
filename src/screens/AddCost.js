import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  Keyboard,
  ToastAndroid,
} from 'react-native';
import { MKTextField } from 'react-native-material-kit';
import Hr from 'react-native-hr';
import { observer } from 'mobx-react/native';
var moment = require('moment-jalaali');

import {
  CustomTextInput,
  CardView,
  Toolbar,
  Overlay,
  SpecialUnitsPopUp,
  LoadingPopUp,
  AndroidBackButton,
} from '../components';
import {
  primaryDark,
  primary,
  placeholderTextColor,
  transparent,
} from '../constants/colors';
import images from '@assets/images';
import { apartmentStore } from '../stores';
import { addCostQuery, confirmCostQuery } from '../network/Queries';

const ownerData = [{ Name: 'ساکن', ID: 0 }, { Name: 'مالک', ID: 1 }];
const divisionByUnitData = [
  { Name: 'همه واحدها', ID: 1 },
  { Name: 'چند واحد خاص', ID: 2 },
  { Name: 'واحدهای پر', ID: 3 },
  { Name: 'واحدهای خالی', ID: 4 },
];
const divisionParameterData = [
  { Name: 'تعداد واحد', ID: 1 },
  { Name: 'تعداد افراد', ID: 2 },
  { Name: 'تعداد پارکینگ', ID: 3 },
  { Name: 'متراژ', ID: 4 },
];

const monthData = [
  { Name: 'فروردین', ID: 1 },
  { Name: 'اردیبهشت', ID: 2 },
  { Name: 'خرداد', ID: 3 },
  { Name: 'تیر', ID: 4 },
  { Name: 'مرداد', ID: 5 },
  { Name: 'شهریور', ID: 6 },
  { Name: 'مهر', ID: 7 },
  { Name: 'آبان', ID: 8 },
  { Name: 'آذر', ID: 9 },
  { Name: 'دی', ID: 10 },
  { Name: 'بهمن', ID: 11 },
  { Name: 'اسفند', ID: 12 },
];

class FormCost extends Component {
  render() {
    const {
      essentail,
      title,
      placeholder,
      onChangeText,
      keyboardType,
      returnKeyType,
      value,
      haveEmptyField,
      isEssential,
    } = this.props;
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'flex-start',
          marginVertical: 8,
        }}
      >
        <View style={{ flexDirection: 'row', flex: 1, alignItems: 'flex-end' }}>
          <Text
            style={{ color: 'black', fontSize: 14, flex: 1, textAlign: 'left' }}
          >
            {title}
          </Text>
        </View>

        <MKTextField
          ref={'textInput'}
          multiline={false}
          keyboardType={keyboardType}
          returnKeyType={returnKeyType}
          floatingLabelEnable={false}
          tintColor={
            isEssential && value == '' && haveEmptyField
              ? 'red'
              : placeholderTextColor
          }
          textInputStyle={{
            color: 'black',
            fontSize: 14,
            textAlign: 'center',
          }}
          underlineSize={1}
          placeholder={placeholder}
          style={{ flex: 1 }}
          onChangeText={text => this.props.onChangeText(text)}
          highlightColor={primary}
          value={value}
        />
      </View>
    );
  }
}

class PickerRow extends Component {
  render() {
    const {
      title,
      pickerData,
      pickedData,
      onPickedChange,
      onPressPicker,
    } = this.props;
    return (
      <View style={{ flexDirection: 'row', marginVertical: 4 }}>
        <Text
          style={{ color: 'black', fontSize: 14, flex: 1, textAlign: 'left' }}
        >
          {title}
        </Text>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'flex-start',
            flexDirection: 'row',
            elevation: 1,
            backgroundColor: 'white',
          }}
        >
          <Picker
            pickedData={pickedData}
            pickerData={pickerData}
            onPress={onPressPicker}
          />
        </View>
      </View>
    );
  }
}

class Picker extends Component {
  render() {
    const { pickerData, pickedData, onPress } = this.props;
    const picked = pickerData.find(item => item.ID == pickedData);
    return (
      <TouchableOpacity
        style={{ flex: 1, flexDirection: 'row' }}
        onPress={onPress}
      >
        <View
          style={{
            flexDirection: 'row',
            elevation: 4,
            alignItems: 'center',
            backgroundColor: 'white',
            flex: 1,
            paddingVertical: 4,
            paddingHorizontal: 4,
          }}
        >
          <Image
            source={images.picker_icon}
            style={{
              height: 16,
              width: 16,
              tintColor: primaryDark,
              marginRight: 4,
            }}
          />
          <Text>{picked.Name}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

class PickerPopUp extends Component {
  render() {
    const { pickerData, pickerTitle, pickerType, onPress } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          borderRadius: 2,
          width: 250,
          height: 200 + (pickerData.length - 2) * 30,
          padding: 8,
        }}
      >
        <Text
          style={{
            fontSize: 16,
            color: primaryDark,
            marginBottom: 8,
            textAlign: 'left',
          }}
        >
          {pickerTitle}
        </Text>
        <ScrollView>
          {pickerData.map(item => (
            <TouchableOpacity
              style={{ paddingVertical: 8, paddingHorizontal: 8 }}
              onPress={() => onPress(item, pickerType)}
            >
              <Text style={{ color: 'black', textAlign: 'left' }}>
                {item.Name}
              </Text>
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
    );
  }
}

@observer
export default class AddCost extends Component {
  constructor() {
    super();
    this.state = {
      title: '',
      cost: '',
      costType: 0,
      costOwner: 1,
      selectedUnits: 1,
      divideBy: 1,
      pickerType: '',
      pickerTitle: '',
      showOverlay: false,
      showPicker: false,
      showSpecialUnitPopUp: false,
      selectedToPayCostUnits: [],
      id: null,
      month: moment(new Date()).jMonth() + 1,
      loading: false,
      haveEmptyField: false,
    };
  }

  componentWillMount() {
    if (
      this.props.navigation &&
      this.props.navigation.state &&
      this.props.navigation.state.params &&
      this.props.navigation.state.params.cost
    ) {
      const {
        CostHeader_ID,
        CostType_ID,
        Description,
        TotalPrice,
        OwnerOfCost,
        ForUnit,
        CalculateType_ID,
        Apartment_ID,
        HasConfirmed,
        Mah,
      } = this.props.navigation.state.params.cost;
      this.setState({
        title: Description,
        cost: TotalPrice,
        costOwner: OwnerOfCost,
        selectedUnits: ForUnit,
        divideBy: CalculateType_ID,
        id: CostHeader_ID,
        month: Mah,
      });
    }
  }

  render() {
    const toolbarStyle = {
      left: {
        onPress: this.onBackPress.bind(this),
        content: images.back_menu,
      },
      center: {
        isImage: false,
        content: 'ایجاد هزینه جدید',
      },
    };
    const {
      costOwner,
      selectedUnits,
      divideBy,
      pickerType,
      month,
      costType,
    } = this.state;
    let selectedUnitsText = '';
    for (i = 0; i < this.state.selectedToPayCostUnits.length - 1; i++) {
      selectedUnitsText =
        selectedUnitsText + `${this.state.selectedToPayCostUnits[i]}، `;
    }
    if (this.state.selectedToPayCostUnits.length) {
      selectedUnitsText =
        selectedUnitsText + `${this.state.selectedToPayCostUnits[i]}`;
    }
    return (
      <View style={{ flex: 1 }}>
        <Toolbar customStyle={toolbarStyle} />
        <AndroidBackButton
          onPress={() => {
            if (this.state.loading) {
              return true;
            } else if (this.state.showOverlay) {
              this.dismissAll();
            } else {
              this.onBackPress();
            }
            return true;
          }}
        />
        <ScrollView>
          <CardView>
            <PickerRow
              title="عنوان هزینه"
              pickerData={apartmentStore.costTypes}
              pickedData={costType}
              onPressPicker={() => {
                this.showSavePanel();
                this.setState({
                  pickerType: 'costSelect',
                  pickerTitle: 'انتخاب عنوان هزینه',
                });
              }}
            />
            {apartmentStore.costTypes.find(
              item => item.ID == this.state.costType,
            ).HasDetail && (
              <FormCost
                essentail={true}
                title="عنوان دیگر"
                placeholder="هزینه نظافت"
                onChangeText={text => this.setState({ title: text })}
                value={this.state.title}
                keyboardType="default"
                returnKeyType="next"
              />
            )}
            <FormCost
              essentail={false}
              title="مبلغ هزینه"
              placeholder="۲۰۰۰ تومان"
              onChangeText={text => this.setState({ cost: text })}
              value={this.state.cost}
              keyboardType="numeric"
              returnKeyType="next"
              haveEmptyField={this.state.haveEmptyField}
              isEssential={true}
            />
            <PickerRow
              title="تاریخ هزینه"
              pickerData={monthData}
              pickedData={month}
              onPressPicker={() => {
                this.showSavePanel();
                this.setState({
                  pickerType: 'monthSelect',
                  pickerTitle: 'انتخاب تاریخ هزینه',
                });
              }}
            />
          </CardView>
          <CardView>
            <PickerRow
              title="صاحب هزینه"
              pickerData={ownerData}
              pickedData={costOwner}
              onPressPicker={() => {
                this.showSavePanel();
                this.setState({
                  pickerType: 'costOwner',
                  pickerTitle: 'انتخاب صاحب هزینه',
                });
              }}
            />
            <PickerRow
              title="برای واحدهای"
              pickerData={divisionByUnitData}
              pickedData={selectedUnits}
              onPressPicker={() => {
                this.showSavePanel();
                this.setState({
                  pickerType: 'selectedUnits',
                  pickerTitle: 'انتخاب واحدها',
                });
              }}
            />
            {this.state.selectedUnits == 2 &&
            this.state.selectedToPayCostUnits.length ? (
              <View style={{ flexDirection: 'row', marginBottom: 8 }}>
                <Text style={{ marginRight: 8, fontSize: 12 }}>
                  واحدهای انتخابی
                </Text>
                <Text style={{ fontSize: 12 }}>{selectedUnitsText}</Text>
              </View>
            ) : null}
            <PickerRow
              title="برحسب"
              pickerData={divisionParameterData}
              pickedData={divideBy}
              onPressPicker={() => {
                this.showSavePanel();
                this.setState({
                  pickerType: 'divideBy',
                  pickerTitle: 'انتخاب نحوه تقسیم',
                });
              }}
            />
          </CardView>
        </ScrollView>
        <TouchableOpacity onPress={() => this.saveCost()}>
          <View style={{ flexDirection: 'row' }}>
            <View
              style={{
                flex: 1,
                height: 60,
                backgroundColor: primary,
                alignItems: 'center',
                justifyContent: 'center',
              }}
            >
              <Text style={{ fontSize: 18, color: 'white' }}>ذخیره</Text>
            </View>
          </View>
        </TouchableOpacity>
        {this.state.showOverlay && (
          <Overlay catchTouch={true} onPress={this.dismissAll.bind(this)}>
            {this.state.showPicker ? (
              <PickerPopUp
                pickerType={pickerType}
                pickerData={
                  pickerType == 'costOwner'
                    ? ownerData
                    : pickerType == 'selectedUnits'
                      ? divisionByUnitData
                      : pickerType == 'monthSelect'
                        ? monthData
                        : pickerType == 'costSelect'
                          ? apartmentStore.costTypes
                          : divisionParameterData
                }
                onPress={(item, pickerType) => {
                  this.dismissSavePanel();
                  this.setPickerValue(item, pickerType);
                }}
                pickerTitle={this.state.pickerTitle}
              />
            ) : this.state.showSpecialUnitPopUp ? (
              <SpecialUnitsPopUp
                onConfirm={units => this.onConfirmSelectingUnits(units)}
                onDismiss={this.onCancelSelectingUnits.bind(this)}
              />
            ) : null}
          </Overlay>
        )}
        {this.state.loading && (
          <Overlay>
            <LoadingPopUp message="در حال ذخیره هزینه جدید ..." />
          </Overlay>
        )}
      </View>
    );
  }

  setPickerValue(item, pickerType) {
    if (pickerType == 'costOwner') {
      this.setState({ costOwner: item.ID });
    } else if (pickerType == 'selectedUnits') {
      this.setState({ selectedUnits: item.ID });
      if (item.ID == 2) {
        this.setState({ showOverlay: true, showSpecialUnitPopUp: true });
      }
    } else if (pickerType == 'divideBy') {
      this.setState({ divideBy: item.ID });
    } else if (pickerType == 'monthSelect') {
      this.setState({ month: item.ID });
    } else if (pickerType == 'costSelect') {
      this.setState({ costType: item.ID });
    }
  }

  onBackPress() {
    this.props.navigation.goBack();
  }

  showSavePanel() {
    this.setState({ showOverlay: true, showPicker: true });
  }

  dismissSavePanel() {
    this.setState({
      showOverlay: false,
      showPicker: false,
      showSpecialUnitPopUp: false,
    });
  }

  onConfirmSelectingUnits(units) {
    this.setState({
      showOverlay: false,
      showPicker: false,
      showSpecialUnitPopUp: false,
      selectedToPayCostUnits: units,
    });
  }

  onCancelSelectingUnits() {
    this.setState({
      showOverlay: false,
      showPicker: false,
      showSpecialUnitPopUp: false,
    });
  }

  async saveCost() {
    if (this.state.costType == 0) {
      ToastAndroid.show('نوع هزینه را انتخاب کنید', ToastAndroid.LONG);
      return;
    }
    if (this.state.cost == '') {
      this.setState({ haveEmptyField: true });
      ToastAndroid.show('مبلغ هزینه را وارد کنید', ToastAndroid.LONG);

      return;
    }

    Keyboard.dismiss();
    this.setState({ loading: true });
    const {
      title,
      cost,
      costOwner,
      selectedUnits,
      divideBy,
      costType,
      month,
    } = this.state;
    let costData = {
      costType_ID: costType,
      description: title,
      totalPrice: cost,
      mah: month,
      ownerOfCost: costOwner,
      forUnit: selectedUnits,
      calculateType_ID: divideBy,
      apartment_ID: apartmentStore.apartmentID,
    };
    if (
      this.props &&
      this.props.navigation &&
      this.props.navigation.state &&
      this.props.navigation.state.params &&
      this.props.navigation.state.params.cost &&
      this.props.navigation.state.params.cost.CostHeader_ID
    ) {
      costData = Object.assign(costData, {
        id: this.props.navigation.state.params.cost.CostHeader_ID,
      });
    }
    if (this.state.selectedToPayCostUnits.length > 0) {
      let data = '<MyTable>\n';
      for (let item of this.state.selectedToPayCostUnits) {
        data = data.concat(`<MyRow>
                <Unit_ID>${apartmentStore.units.find(
                  myItem => myItem.UnitNumber == item,
                ).ID}</Unit_ID>
        </MyRow>`);
      }
      data = data.concat('\n</MyTable>');
      costData = Object.assign(costData, {
        data: data,
      });
    }
    console.warn(JSON.stringify(costData));

    const addCostRes = await addCostQuery(costData);
    this.setState({ loading: false });
    if (addCostRes) {
      this.onBackPress();
    } else {
      //TODO: show proper error
    }
  }

  dismissAll() {
    this.setState({
      showOverlay: false,
      showPicker: false,
      showSpecialUnitPopUp: false,
    });
  }
}
