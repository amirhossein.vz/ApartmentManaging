import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
var RNFetchBlob = require('react-native-fetch-blob').default;

import { serverURL } from '../constants/values';
import { primaryDark } from '../constants/colors';
import { fetchFactory } from '../utils';
import { persistStore } from '../stores';

export default class FacilityCard extends Component {
  constructor() {
    super();
    this.state = {
      image: null,
    };
  }

  async componentWillMount() {
    let self = this;
    RNFetchBlob.fetch('GET', `${serverURL}/upload/${this.props.data.Image}`, {
      'x-access-token': persistStore.token,
    })
      // when response status code is 200
      .then(res => {
        // the conversion is done in native code
        let base64Str = res.base64();
        self.setState({ image: base64Str });
      })
      // Status code is not 200
      .catch((errorMessage, statusCode) => {
        // error handling
      });
  }

  render() {
    const { Title, Description, ID } = this.props.data;
    return (
      <TouchableOpacity
        onPress={() => this.props.onPress(this.props.data)}
        onLongPress={() => this.props.onLongPress(this.props.data)}
      >
        <View
          style={{
            marginVertical: 4,
            marginHorizontal: 8,
            backgroundColor: 'white',
            elevation: 2,
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              height: 200,
              alignItems: 'center',
              justifyContent: 'center',
            }}
          >
            {this.state.image ? (
              <Image
                source={{ uri: `data:image/gif;base64,${this.state.image}` }}
                style={{ flex: 1, resizeMode: 'cover', height: 200 }}
              />
            ) : (
              <ActivityIndicator color={primaryDark} size="large" />
            )}
          </View>
          <View style={{ padding: 8 }}>
            <Text
              style={{ color: primaryDark, fontSize: 16, textAlign: 'left' }}
            >
              {Title}
            </Text>
            <Text style={{ textAlign: 'left' }}>{Description}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}
