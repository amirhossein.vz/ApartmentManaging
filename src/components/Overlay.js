import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';

import { overlayColor } from '../constants/colors';

export default class Overlay extends Component {
  render() {
    return (
      <View
        pointerEvents="box-none"
        style={{
          ...StyleSheet.absoluteFillObject,
          backgroundColor: 'rgba(0, 0, 0, 0.7)',
          justifyContent: 'center',
          elevation: 8,
          flex: 1,
        }}
      >
        <TouchableWithoutFeedback onPress={this.props.onPress}>
          <View
            style={{
              flex: 1,
              backgroundColor: overlayColor,
              opacity: 0.6,
              ...StyleSheet.absoluteFillObject,
            }}
            pointerEvents={this.props.catchTouch ? 'auto' : 'box-none'}
          />
        </TouchableWithoutFeedback>
        <View style={{ alignSelf: 'center' }}>{this.props.children}</View>
      </View>
    );
  }
}
