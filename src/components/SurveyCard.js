import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';

import { primary, primaryDark, transparent } from '../constants/colors';
import images from '@assets/images';
import { apartmentStore } from '../stores';
import { submitSurveyResultQuery } from '../network/Queries';
import { parseTimeToJalaali } from '../utils';

export default class SurveyCard extends Component {
  render() {
    const {
      Title,
      Question,
      QuestionType,
      AnswerOne,
      AnswerTwo,
      AnswerThree,
      AnswerFour,
      AnswerOneCount,
      AnswerTwoCount,
      AnswerThreeCount,
      AnswerFourCount,
      CreatedAtDatetime,
    } = this.props.item;
    return (
      <View
        style={{
          marginVertical: 4,
          marginHorizontal: 8,
          backgroundColor: 'white',
          elevation: 2,
        }}
      >
        <View
          style={{
            paddingVertical: 8,
            backgroundColor: primaryDark,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'row',
          }}
        >
          {apartmentStore.isManager && (
            <TouchableOpacity
              style={{ paddingVertical: 4, paddingHorizontal: 16 }}
              onPress={() => null}
            >
              <Image
                source={images.edit_icon}
                style={{
                  height: 18,
                  width: 18,
                  tintColor: 'white',
                  tintColor: transparent,
                }}
              />
            </TouchableOpacity>
          )}
          <Text
            style={{
              color: 'white',
              flex: 1,
              textAlign: 'center',
              fontSize: 16,
            }}
          >
            {Title}
          </Text>
          {apartmentStore.isManager && (
            <TouchableOpacity
              style={{ paddingVertical: 4, paddingHorizontal: 16 }}
              onPress={() => this.props.delete(this.props.item)}
            >
              <Image
                source={images.delete_icon}
                style={{ height: 18, width: 18, tintColor: 'white' }}
              />
            </TouchableOpacity>
          )}
        </View>
        <View style={{ padding: 8, alignItems: 'center' }}>
          <Text style={{ color: 'black', fontSize: 16 }}>{Question}</Text>
        </View>
        {QuestionType == 1 ? (
          <View style={{ marginTop: 8, marginHorizontal: 16 }}>
            <TouchableOpacity
              onPress={() => this.submitServeyResult(1, 'موافقم')}
            >
              <View
                style={{
                  borderWidth: 1,
                  borderColor: primary,
                  padding: 8,
                  borderRadius: 2,
                  marginBottom: 8,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
              >
                <Text style={{ flex: 1, textAlign: 'left' }}>موافقم</Text>
                <Text style={{ fontSize: 10, color: primaryDark }}>
                  {AnswerOneCount} نظر
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.submitServeyResult(2, 'مخالفم')}
            >
              <View
                style={{
                  borderWidth: 1,
                  borderColor: primary,
                  padding: 8,
                  borderRadius: 2,
                  marginBottom: 8,
                  flexDirection: 'row',
                  alignItems: 'center',
                }}
              >
                <Text style={{ flex: 1, textAlign: 'left' }}>مخالفم</Text>
                <Text style={{ fontSize: 10, color: primaryDark }}>
                  {AnswerTwoCount} نظر
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        ) : (
          <View style={{ marginTop: 8, marginHorizontal: 16 }}>
            {!!AnswerOne &&
              AnswerOne !== '' && (
                <TouchableOpacity
                  onPress={() => this.submitServeyResult(1, AnswerOne)}
                >
                  <View
                    style={{
                      borderWidth: 1,
                      borderColor: primary,
                      padding: 8,
                      borderRadius: 2,
                      marginBottom: 8,
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}
                  >
                    <Text style={{ flex: 1, textAlign: 'left' }}>
                      {AnswerOne}
                    </Text>
                    <Text style={{ fontSize: 10, color: primaryDark }}>
                      {AnswerOneCount} نظر
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
            {!!AnswerTwo &&
              AnswerTwo !== '' && (
                <TouchableOpacity
                  onPress={() => this.submitServeyResult(2, AnswerTwo)}
                >
                  <View
                    style={{
                      borderWidth: 1,
                      borderColor: primary,
                      padding: 8,
                      borderRadius: 2,
                      marginBottom: 8,
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}
                  >
                    <Text style={{ flex: 1, textAlign: 'left' }}>
                      {AnswerTwo}
                    </Text>
                    <Text style={{ fontSize: 10, color: primaryDark }}>
                      {AnswerTwoCount} نظر
                    </Text>
                  </View>
                </TouchableOpacity>
              )}

            {!!AnswerThree &&
              AnswerThree !== '' && (
                <TouchableOpacity
                  onPress={() => this.submitServeyResult(3, AnswerThree)}
                >
                  <View
                    style={{
                      borderWidth: 1,
                      borderColor: primary,
                      padding: 8,
                      borderRadius: 2,
                      marginBottom: 8,
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}
                  >
                    <Text style={{ flex: 1, textAlign: 'left' }}>
                      {AnswerThree}
                    </Text>
                    <Text style={{ fontSize: 10, color: primaryDark }}>
                      {AnswerThreeCount} نظر
                    </Text>
                  </View>
                </TouchableOpacity>
              )}

            {!!AnswerFour &&
              AnswerFour !== '' && (
                <TouchableOpacity
                  onPress={() => this.submitServeyResult(4, AnswerFour)}
                >
                  <View
                    style={{
                      borderWidth: 1,
                      borderColor: primary,
                      padding: 8,
                      borderRadius: 2,
                      marginBottom: 8,
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}
                  >
                    <Text style={{ flex: 1, textAlign: 'left' }}>
                      {AnswerFour}
                    </Text>
                    <Text style={{ fontSize: 10, color: primaryDark }}>
                      {AnswerFourCount} نظر
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
          </View>
        )}
        <Text
          style={{
            fontSize: 10,
            textAlign: 'right',
            marginHorizontal: 8,
            marginBottom: 8,
            marginTop: 8,
          }}
        >
          {parseTimeToJalaali(CreatedAtDatetime)}
        </Text>
      </View>
    );
  }

  async submitServeyResult(opinionID, title) {
    const newItem = {
      survey_ID: this.props.item.ID,
      title: title,
      answer: opinionID,
    };
    this.props.onOpinionPress(newItem);
    // const surveyOpinionRes = await submitServeyResultQuery(newItem);
    //
    // export async function submitSurveyResultQuery(result, ID) {
    //   const response = await fetchFactory(`/survey/opinion/${ID}`, {
    //     method: 'POST',
    //     body: result,
    //   });
    //   if (response.ok && (await response.json())[0].ResultCode == 1) {
    //     const queryRes = getSurveyQuery(apartmentStore.apartmentID);
    //     if (queryRes) {
    //       return true;
    //     }
    //   }
    //   return false;
    // }
  }
}
