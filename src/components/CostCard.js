import React, { Component } from 'react';
import { View, FlatList, Image, Text, TouchableOpacity } from 'react-native';
import { phonecall } from 'react-native-communications';

import { primary, primaryDark } from '../constants/colors';
import images from '@assets/images';
import { apartmentStore } from '../stores';

class CostCardRow extends Component {
  render() {
    const { icon, title, data } = this.props;
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <Image
          source={icon}
          style={{ height: 14, width: 14, tintColor: primaryDark }}
        />
        <Text
          style={{
            fontSize: 12,
            flex: 2,
            marginHorizontal: 16,
            color: 'black',
            textAlign: 'left'
          }}
        >
          {title}
        </Text>
        <View style={{ flex: 3, flexDirection: 'row' }}>
          <Text
            style={{ fontSize: 12, flex: 3, textAlign: 'left', color: 'black' }}
          >
            {data}
          </Text>
        </View>
      </View>
    );
  }
}

const ownerData = [
  { label: 'ساکن', value: 'resident', id: 0 },
  { label: 'مالک', value: 'owner', id: 1 },
];
const divisionByUnitData = [
  { label: 'همه واحدها', value: 'allUnits', id: 1 },
  { label: 'چند واحد خاص', value: 'specialUnits', id: 2 },
  { label: 'واحدهای پر', value: 'fullUnits', id: 3 },
  { label: 'واحدهای خالی', value: 'emptyUnit', id: 4 },
];
const divisionParameterData = [
  { label: 'تعداد واحد', value: 'count', id: 1 },
  { label: 'تعداد افراد', value: 'residents', id: 2 },
  { label: 'تعداد پارکینگ', value: 'parking', id: 3 },
  { label: 'متراژ', value: 'area', id: 4 },
];

export default class CostCard extends Component {
  constructor() {
    super();
    this.state = {
      costSelected: false,
    };
  }

  render() {
    const {
      Description,
      TotalPrice,
      OwnerOfCost,
      ForUnit,
      CalculateType_ID,
      Mah,
      CostName,
      HasConfirmed,
      TotalUnitPrice,
      Title,
    } = this.props.cost;

    const monthText = apartmentStore.monthData.find(item => item.ID == Mah)
      .Name;
    return (
      <TouchableOpacity
        disabled={!this.props.selectState}
        onPress={() => {
          this.setState({ costSelected: !this.state.costSelected });
          this.props.onAddCostToConfirmation(this.props.cost);
        }}
      >
        <View
          style={{
            marginVertical: 4,
            marginHorizontal: 8,
            backgroundColor: 'white',
            elevation: 2,
          }}
        >
          <View
            style={{
              paddingVertical: 8,
              backgroundColor: primaryDark,
              alignItems: 'center',
              justifyContent: 'center',
              flexDirection: 'row',
            }}
          >
            {apartmentStore.isManager &&
              !this.props.selectState &&
              !HasConfirmed && (
                <TouchableOpacity
                  style={{ paddingVertical: 4, paddingHorizontal: 16 }}
                  onPress={() => this.props.navigateToEdit(this.props.cost)}
                >
                  <Image
                    source={images.edit_icon}
                    style={{ height: 18, width: 18, tintColor: 'white' }}
                  />
                </TouchableOpacity>
              )}
            {HasConfirmed ? (
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text
                  style={{
                    color: 'white',
                    textAlign: 'center',
                    fontSize: 14,
                  }}
                >
                  {CostName} {monthText} ماه
                </Text>
                <Text style={{ color: 'white', fontSize: 12 }}>
                  {' '}
                  ({Title}){' '}
                </Text>
              </View>
            ) : (
              <Text
                style={{
                  color: 'white',
                  textAlign: 'center',
                  flex: 1,
                  fontSize: 14,
                }}
              >
                {CostName} {monthText} ماه
              </Text>
            )}
            {apartmentStore.isManager && !HasConfirmed ? (
              this.props.selectState ? (
                <TouchableOpacity
                  style={{ paddingVertical: 4, paddingHorizontal: 16 }}
                  onPress={() => {
                    this.setState({ costSelected: !this.state.costSelected });
                    this.props.onAddCostToConfirmation(this.props.cost);
                  }}
                >
                  <Image
                    source={
                      this.state.costSelected
                        ? images.checked_icon
                        : images.unchecked_icon
                    }
                    style={{ height: 18, width: 18, tintColor: 'white' }}
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  style={{ paddingVertical: 4, paddingHorizontal: 8 }}
                  onPress={this.props.deleteCost.bind(this, this.props.cost)}
                >
                  <Image
                    source={images.delete_icon}
                    style={{ height: 18, width: 18, tintColor: 'white' }}
                  />
                </TouchableOpacity>
              )
            ) : null}
          </View>
          <View style={{ padding: 8 }}>
            <CostCardRow
              icon={images.money_icon}
              title={'مبلغ کل'}
              data={TotalPrice}
            />
            {apartmentStore.userRole == 'User' && (
              <CostCardRow
                icon={images.money_icon}
                title={'مبلغ واحد شما'}
                data={TotalUnitPrice}
              />
            )}
            <CostCardRow
              icon={images.people_icon}
              title={'صاحب هزینه'}
              data={ownerData.find(item => item.id == OwnerOfCost).label}
            />
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Image
                source={images.information_icon}
                style={{ height: 14, width: 14, tintColor: primaryDark }}
              />
              <Text
                style={{
                  fontSize: 12,
                  flex: 2,
                  marginHorizontal: 16,
                  textAlign: 'left'
                }}
              >
                {`برای ${divisionByUnitData.find(item => item.id == ForUnit)
                  .label}،‌ تقسیم براساس ${divisionParameterData.find(
                  item => item.id == CalculateType_ID,
                ).label}`}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  onSelectCost() {}
}
