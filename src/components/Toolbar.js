import React from 'react';
import { View, Text, TouchableOpacity, Image, StatusBar,Platform } from 'react-native';
import { primary, primaryDark } from '../constants/colors';

export default class Toolbar extends React.Component {
  render() {
    const { left, center, right } = this.props.customStyle;
    return (
      <View>
        <StatusBar backgroundColor={primaryDark} barStyle="light-content" />
        <View
          style={{
            flexDirection: 'row',
            backgroundColor: primary,
            alignItems: 'center',
            height: 58,
            elevation: 4,
            paddingTop: Platform.OS=='ios' ? 24 : 0
          }}
        >
          {left &&
            <TouchableOpacity onPress={left.onPress} style={{ padding: 8 }}>
              <Image
                source={left.content}
                style={{ height: 24, width: 24, tintColor: 'white' }}
              />
            </TouchableOpacity>}
          {center
            ? <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
              >
                {center.isImage
                  ? <Image
                      source={center.content}
                      style={{ tintColor: 'white' }}
                    />
                  : <Text style={{ color: 'white', fontSize: 20 }}>
                      {center.content}
                    </Text>}
              </View>
            : <View style={{ flex: 1 }} />}
          {right
            ? <TouchableOpacity
                onPress={right.onPress}
                style={{ paddingHorizontal: 8 }}
              >
                <Image
                  source={right.content}
                  style={{ height: 24, width: 24, tintColor: 'white' }}
                />
              </TouchableOpacity>
            : <View style={{ paddingHorizontal: 16 }} />}
        </View>
      </View>
    );
  }
}
