import React, { Component } from 'react';
import { View, FlatList, Image, Text, TouchableOpacity } from 'react-native';
import { phonecall } from 'react-native-communications';

import { primary, primaryDark } from '../constants/colors';
import images from '@assets/images';

class AddSansCardRow extends Component {
  render() {
    const { icon, title, data } = this.props;
    return (
      <View style={{ flexDirection: 'row' }}>
        <Image
          source={icon}
          style={{ height: 14, width: 14, tintColor: primaryDark }}
        />
        <Text
          style={{
            fontSize: 12,
            flex: 2,
            marginHorizontal: 16,
            color: 'black',
          }}
        >
          {title}
        </Text>
        <View style={{ flex: 3, flexDirection: 'row' }}>
          <Text
            style={{ fontSize: 12, flex: 3, textAlign: 'left', color: 'black' }}
          >
            {data}
          </Text>
        </View>
      </View>
    );
  }
}

export default class AddSansCard extends Component {
  render() {
    const {
      startTime,
      endTime,
      type,
      male,
      female,
      price,
      afterCleaningSans,
      reserved,
    } = this.props.sans;
    let genderText;
    if (type == 'GENERAL') {
      if (male) {
        if (female) {
          genderText = 'آزاد برای عموم';
        } else {
          genderText = 'سانس مخصوص آقایان';
        }
      } else {
        if (female) {
          genderText = 'سانس مخصوص بانوان';
        } else {
          genderText = 'به گروه جنسیتی خاصی متعلق نیست';
        }
      }
    }
    return (
      <View
        style={{
          marginVertical: 4,
          marginHorizontal: 8,
          backgroundColor: 'white',
          elevation: 2,
        }}
      >
        <View style={{ padding: 8 }}>
          <AddSansCardRow
            icon={images.time_start_icon}
            title={'ساعت شروع'}
            data={startTime}
          />
          <AddSansCardRow
            icon={images.time_end_icon}
            title={'ساعت خاتمه'}
            data={endTime}
          />
          {type == 'GENERAL' && (
            <View style={{ flexDirection: 'row' }}>
              <Image
                source={images.people_icon}
                style={{ height: 14, width: 14, tintColor: primaryDark }}
              />
              <Text
                style={{
                  fontSize: 12,
                  flex: 2,
                  marginHorizontal: 16,
                }}
              >
                {genderText}
              </Text>
            </View>
          )}
          {type == 'SPECIAL' &&
            price && (
              <AddSansCardRow
                icon={images.money_icon}
                title={'هزینه رزور'}
                data={price}
              />
            )}
          {type == 'SPECIAL' && reserve}
        </View>
      </View>
    );
  }
}
