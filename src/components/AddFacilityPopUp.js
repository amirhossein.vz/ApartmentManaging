import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Text,
  Image,
  Keyboard
} from 'react-native';
import _ from 'lodash';

import { MKTextField } from 'react-native-material-kit';
import {
  primary,
  placeholderTextColor,
  transparent,
  primaryDark,
} from '../constants/colors';
import images from '@assets/images';
import { apartmentStore } from '../stores';

class PickerRow extends Component {
  render() {
    const {
      title,
      pickerData,
      pickedData,
      onPickedChange,
      onPressPicker,
    } = this.props;
    return (
      <View style={{ flexDirection: 'row', marginVertical: 4 }}>
        <Text style={{ color: 'black', fontSize: 14, flex: 1 , textAlign: 'left'}}>{title}</Text>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'flex-start',
            flexDirection: 'row',
            elevation: 1,
            backgroundColor: 'white',
          }}
        >
          <Picker
            pickedData={pickedData}
            pickerData={pickerData}
            onPress={onPressPicker}
          />
        </View>
      </View>
    );
  }
}

class Picker extends Component {
  render() {
    const { pickerData, pickedData, onPress } = this.props;
    const picked = pickerData.find(item => item.value == pickedData);
    return (
      <TouchableOpacity
        style={{ flex: 1, flexDirection: 'row' }}
        onPress={onPress}
      >
        <View
          style={{
            flexDirection: 'row',
            elevation: 4,
            alignItems: 'center',
            backgroundColor: 'white',
            flex: 1,
            paddingVertical: 4,
            paddingHorizontal: 4,
          }}
        >
          <Image
            source={images.picker_icon}
            style={{
              height: 16,
              width: 16,
              tintColor: primaryDark,
              marginRight: 4,
            }}
          />
          <Text>{pickedData}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

class PickerPopUp extends Component {
  render() {
    const { pickerData, pickerTitle, onPress } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          borderRadius: 2,
          width: 250,
          height: 200 + (pickerData.length - 2) * 30,
          padding: 8,
        }}
      >
        <Text style={{ fontSize: 16, color: primaryDark, marginBottom: 8 , textAlign: 'left'}}>
          {pickerTitle}
        </Text>
        {pickerData.map(item => (
          <TouchableOpacity
            style={{ paddingVertical: 8, paddingHorizontal: 8 }}
            onPress={() => onPress(item)}
          >
            <Text style={{ color: 'black' , textAlign: 'left'}}>{item.Title}</Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  }
}
const facilityImagesTypes = [
  { name: 'Badminton.jpg', image: images.Badminton },
  { name: 'billiard.jpg', image: images.billiard },
  { name: 'Cinema.jpg', image: images.Cinema },
  { name: 'Pingpong.jpg', image: images.Pingpong },
  { name: 'Roof.jpg', image: images.Roof },
  { name: 'Swimming.jpg', image: images.Swimming },
  { name: 'Tennis.jpg', image: images.Tennis },
];
export default class AddMessagePopUp extends Component {
  constructor() {
    super();
    this.state = {
      title: '',
      description: '',
      tel: '',
      haveSchedule: true,
      facilityType: 1,
    };
  }

  render() {
    const image = apartmentStore.facilityTypes.find(
      item => item.ID == this.state.facilityType,
    )
      ? facilityImagesTypes.find(
          item =>
            item.name ==
            apartmentStore.facilityTypes.find(
              item => item.ID == this.state.facilityType,
            ).DefaultImage,
        ).image
      : images.image_placeholder;

    return (
      <View>
        {this.props.showPicker ? (
          <PickerPopUp
            pickerData={apartmentStore.facilityTypes}
            pickerTitle="انتخاب عنوان"
            onPress={item => {
              this.props.onPickerItemPress();
              this.setState({
                facilityType: item.ID,
              });
            }}
          />
        ) : (
          <View
            style={{ backgroundColor: 'white', elevation: 2, borderRadius: 4 }}
          >
            <View style={{ padding: 16 }}>
              <PickerRow
                title="عنوان"
                pickerData={apartmentStore.facilityTypes}
                pickedData={
                  apartmentStore.facilityTypes.find(
                    item => item.ID == this.state.facilityType,
                  ).Title
                }
                onPressPicker={() => this.props.onPickerPress()}
              />
              <TouchableOpacity
                style={{
                  height: 160,
                  marginVertical: 16,
                }}
                disable={true}
              >
                <Image
                  style={{
                    height: 160,
                    width: 240,
                    resizeMode: 'stretch',
                  }}
                  source={image}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  this.setState({ haveSchedule: !this.state.haveSchedule })}
              >
                <View style={{ flexDirection: 'row', marginBottom: 16 }}>
                  <Text style={{ marginRight: 8 }}>
                    دارای زمان‌بندی مشخص می‌باشد
                  </Text>
                  <Image
                    style={{ height: 18, width: 18, tintColor: primaryDark }}
                    source={
                      this.state.haveSchedule
                        ? images.checked_icon
                        : images.unchecked_icon
                    }
                  />
                </View>
              </TouchableOpacity>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: 16,
                }}
              >
                <Text style={{ color: 'black', marginRight: 32 }}>
                  شماره تماس
                </Text>
                <MKTextField
                  ref="textInput"
                  multiline={false}
                  keyboardType="numeric"
                  returnKeyType="next"
                  floatingLabelEnable={false}
                  tintColor={placeholderTextColor}
                  textInputStyle={{
                    fontSize: 16,
                    color: 'black',
                    flexGrow: 1,
                    textAlign: 'center',
                  }}
                  underlineSize={1}
                  placeholder="۰۹۱۲ *******"
                  style={{ width: 120 }}
                  onChangeText={text => this.setState({ tel: text })}
                  highlightColor={primaryDark}
                  value={this.state.tel}
                />
              </View>
              <View style={{ height: 120 }}>
                <TextInput
                  placeholder="توضیحات مربوط به امکانات رفاهی جدید را در این‌جا وارد کنید"
                  placeholderTextColor={placeholderTextColor}
                  style={styles.textInput}
                  multiline={true}
                  underlineColorAndroid={transparent}
                  onChangeText={text => this.setState({ description: text })}
                  returnKeyType="default"
                  value={this.state.description}
                  blurOnSubmit={false}
                  numberOfLines={4}
                  onSubmitEditing={_.debounce(
                    () =>
                      this.setState({
                        description: this.state.description + '\n',
                      }),
                    100,
                  )}
                />
              </View>
            </View>
            <TouchableOpacity
              onPress={() =>
                this.props.onConfirm({
                  title_ID: this.state.facilityType,
                  image: null,
                  isScheduled: this.state.haveSchedule,
                  description: this.state.description,
                  tel: this.state.tel,
                })}
            >
              <View style={{ flexDirection: 'row' }}>
                <View
                  style={{
                    flex: 1,
                    height: 60,
                    backgroundColor: primaryDark,
                    alignItems: 'center',
                    justifyContent: 'center',
                    borderBottomLeftRadius: 4,
                    borderBottomRightRadius: 4,
                  }}
                >
                  <Text style={{ fontSize: 18, color: 'white' }}>ذخیره</Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  }

  async uploadImage(avatar) {}
}

const styles = StyleSheet.create({
  textInput: {
    paddingHorizontal: 4,
    borderColor: primaryDark,
    borderWidth: 0.5,
    height: 120,
    width: 240,
    borderRadius: 2,
    fontSize: 14,
    textAlignVertical: 'top',
    textAlign: 'right',
  },
});
