import React, { Component } from 'react';
import { View, TouchableOpacity, Image } from 'react-native';

export default class Fab extends Component {
  render() {
    const { bgColor, onPress, icon, iconColor } = this.props;
    return (
      <TouchableOpacity onPress={onPress}>
        <View
          style={{
            padding: 16,
            backgroundColor: '#00c9cb',
            elevation: 3,
            borderRadius: 32,
          }}
        >
          <Image
            source={icon}
            style={{ height: 24, width: 24, tintColor: iconColor }}
          />
        </View>
      </TouchableOpacity>
    );
  }
}
