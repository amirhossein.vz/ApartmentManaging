import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';

import { primary, primaryDark } from '../constants/colors';
import images from '@assets/images';
import { apartmentStore } from '../stores';
import { parseTimeToJalaali } from '../utils';

export default class SuggestionCard extends Component {
  render() {
    const {
      Title,
      Description,
      CreatedAtDatetime,
      UnitNumber,
      Status,
    } = this.props.item;
    userStatusText = Status == 0 ? 'ساکن' : 'مالک';

    return (
      <View
        style={{
          marginVertical: 4,
          marginHorizontal: 8,
          backgroundColor: 'white',
          elevation: 2,
        }}
      >
        <View
          style={{
            paddingVertical: 8,
            backgroundColor: primaryDark,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'row',
          }}
        >
          {apartmentStore.userRole == 'User' ? (
            <TouchableOpacity
              style={{ paddingVertical: 4, paddingHorizontal: 16 }}
              onPress={() => this.props.navigateToEdit(this.props.item)}
            >
              <Image
                source={images.edit_icon}
                style={{ height: 18, width: 18, tintColor: 'white' }}
              />
            </TouchableOpacity>
          ) : (
            <View style={{ width: 18, marginHorizontal: 16 }} />
          )}
          <Text
            style={{
              color: 'white',
              flex: 1,
              textAlign: 'center',
              fontSize: 16,
            }}
          >
            {Title}
          </Text>
          <TouchableOpacity
            style={{ paddingVertical: 4, paddingHorizontal: 16 }}
            onPress={() => this.props.delete(this.props.item)}
          >
            <Image
              source={images.delete_icon}
              style={{ height: 18, width: 18, tintColor: 'white' }}
            />
          </TouchableOpacity>
        </View>
        <View style={{ padding: 8 }}>
          <Text style={{ color: 'black', textAlign: 'left' }}>
            {Description}
          </Text>
          <Text
            style={{
              fontSize: 10,
              textAlign: 'right',
            }}
          >
            توسط {userStatusText} واحد {UnitNumber}
          </Text>
          <Text
            style={{
              fontSize: 10,
              textAlign: 'right',
            }}
          >
            {parseTimeToJalaali(CreatedAtDatetime)}
          </Text>
        </View>
      </View>
    );
  }
}
