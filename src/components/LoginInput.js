import React from 'react';
import { View, Text, Image } from 'react-native';
import { MKTextField } from 'react-native-material-kit';

export default class LoginInput extends React.Component {
  render() {
    const { icon, placeHolder, password = false } = this.props;
    return (
      <View style={{ flexDirection: 'row', marginHorizontal: 32 }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            borderRadius: 8,
            backgroundColor: 'rgba(0,0,0, 0.5)',
            padding: 10,
            alignItems: 'center',
          }}
        >
          <View
            style={{
              padding: 4,
              borderRadius: 128,
              borderWidth: 1,
              borderColor: '#FFF',
              marginRight: 24,
            }}
          >
            <Image
              source={icon}
              style={{ width: 18, height: 18, tintColor: '#FFF' }}
            />
          </View>
          <MKTextField
            multiline={false}
            keyboardType="numeric"
            onChangeText={text => this.props.onChangeText(text)}
            returnKeyType="next"
            floatingLabelEnable={false}
            tintColor="white"
            placeholderTextColor="rgba(255,255,255,0.7)"
            textInputStyle={{
              color: 'white',
              fontSize: 16,
              flexGrow: 1,
              padding: 8,
              textAlign: 'right',
            }}
            password={password}
            underlineSize={0}
            placeholder={placeHolder}
            style={{ flex: 1 }}
            highlightColor={'white'}
            value={this.props.value}
          />
        </View>
      </View>
    );
  }
}
