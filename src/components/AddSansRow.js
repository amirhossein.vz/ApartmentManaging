import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

import { SpecialSansStatus } from './index';
import { genderTypes, facilityStatusTypes } from '../constants/values';
import { apartmentStore } from '../stores';

export default class AddSansRow extends Component {
  render() {
    const {
      ID,
      DayOfWeek,
      FromHour,
      ToHour,
      Status,
      Price,
      GenderType,
      Description,
    } = this.props.data;
    let genderText;
    if (
      Status == facilityStatusTypes.find(item => item.value == 'GENERAL').id
    ) {
      genderText = genderTypes.find(item => item.id == GenderType).label;
    }
    return (
      <TouchableOpacity
        onLongPress={() =>
          apartmentStore.userRole == 'Admin' ||
          apartmentStore.userRole == 'Manager'
            ? this.props.onLongPress(this.props.data)
            : null}
      >
        <View
          style={{
            marginVertical: 2,
            marginHorizontal: 4,
            padding: 8,
            backgroundColor: 'white',
            elevation: 2,
          }}
        >
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <View
              style={{ flexDirection: 'row', flex: 4, alignItems: 'center' }}
            >
              <Text>از ساعت</Text>
              <Text
                style={{ color: 'black', fontSize: 16, marginHorizontal: 4 }}
              >
                {FromHour}
              </Text>
              <Text>تا ساعت</Text>
              <Text
                style={{
                  color: 'black',
                  fontSize: 16,
                  marginLeft: 4,
                  marginRight: 24,
                }}
              >
                {ToHour}
              </Text>
            </View>
            {Status ==
            facilityStatusTypes.find(item => item.value == 'GENERAL').id ? (
              <View
                style={{ flexDirection: 'row', alignItems: 'center', flex: 2 }}
              >
                <Text style={{ fontSize: 12, color: 'grey' }}>مخصوص</Text>
                <Text style={{ color: 'black', marginHorizontal: 8 }}>
                  {genderText}
                </Text>
              </View>
            ) : null}
          </View>
          {Status ==
          facilityStatusTypes.find(item => item.value == 'SPECIAL').id ? (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ fontSize: 12, color: 'grey' }}>قیمت سانس:</Text>
              <Text style={{ color: 'black', marginHorizontal: 8 }}>
                {Price} تومان
              </Text>
            </View>
          ) : null}

          {Status ==
          facilityStatusTypes.find(item => item.value == 'OFF').id ? (
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={{ fontSize: 12, color: 'grey' }}>علت تعطیلی:</Text>
              <Text style={{ color: 'black', marginHorizontal: 8 }}>
                {Description}
              </Text>
            </View>
          ) : null}
        </View>
      </TouchableOpacity>
    );
  }
}
