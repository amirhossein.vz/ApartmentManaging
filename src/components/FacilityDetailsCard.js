import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';

import { primary, primaryDark, transparent } from '../constants/colors';
import images from '@assets/images';

export default class FacillityDetailsCard extends Component {
  render() {
    const { title, generalSans, specialSans, description } = this.props.data;
    return (
      <View
        style={{
          marginVertical: 4,
          marginHorizontal: 8,
          backgroundColor: 'white',
          elevation: 2,
        }}
      >
        <View
          style={{
            paddingVertical: 8,
            backgroundColor: primaryDark,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'row',
          }}
        >
          <TouchableOpacity
            style={{ paddingVertical: 4, paddingHorizontal: 16 }}
            disabled={true}
          >
            <Image
              source={images.edit_icon}
              style={{ height: 18, width: 18, tintColor: 'transparent' }}
            />
          </TouchableOpacity>
          <Text
            style={{
              color: 'white',
              flex: 1,
              textAlign: 'center',
              fontSize: 16,
            }}
          >
            {title}
          </Text>
          <TouchableOpacity
            style={{ paddingVertical: 4, paddingHorizontal: 16 }}
            onPress={() => this.props.onPress(title)}
          >
            <Image
              source={images.edit_icon}
              style={{ height: 18, width: 18, tintColor: 'white' }}
            />
          </TouchableOpacity>
        </View>
        <View style={{ padding: 8 }}>
          {generalSans > 0 || specialSans > 0
            ? <Text style={{ color: 'black', fontSize: 14 }}>
                دارای {generalSans} سانس عمومی، {specialSans} سانس خصوصی
              </Text>
            : <Text style={{ color: 'black', fontSize: 14 }}>
                هیچ سانسی تعریف نشده است
              </Text>}
          {description &&
            <Text style={{ fontSize: 12 }}>
              {description}
            </Text>}
        </View>
      </View>
    );
  }
}
