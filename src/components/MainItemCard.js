import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native';

import { darkGrey, primary, primaryLight } from '../constants/colors';

export default function MainItemCard({ image, onPress, text, style }) {
  return (
    <View
      style={[
        style,
        { backgroundColor: 'white', elevation: 2, borderRadius: 4 },
      ]}
    >
      <TouchableOpacity onPress={onPress}>
        <View
          style={{ padding: 8, alignItems: 'center', justifyContent: 'center' }}
        >
          <View style={{ padding: 8 }}>
            <Image
              source={image}
              style={{
                height: 80,
                width: 80,
                resizeMode: 'contain',
                tintColor: primaryLight,
              }}
            />
          </View>
          <Text style={{ color: darkGrey, fontSize: 14 }}>
            {text}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}
