import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Text,
} from 'react-native';

import { MKTextField } from 'react-native-material-kit';
import {
  primary,
  placeholderTextColor,
  transparent,
  primaryDark,
} from '../constants/colors';

export default class AddMessagePopUp extends Component {
  constructor() {
    super();
    this.state = {
      title: '',
      message: '',
      id: '',
    };
  }

  componentDidMount() {
    const item = this.props.item;
    if (item) {
      this.setState({
        title: item.Title,
        message: item.Description,
        id: item.ID,
      });
    }
  }

  render() {
    const { title, message } = this.props;
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          width: 272,
          borderRadius: 4,
        }}
      >
        <View style={{ padding: 16 }}>
          <MKTextField
            ref="textInput"
            multiline={false}
            keyboardType="default"
            returnKeyType="next"
            floatingLabelEnable={false}
            tintColor={placeholderTextColor}
            textInputStyle={{
              fontSize: 16,
              flexGrow: 1,
              textAlign: 'right',
            }}
            underlineSize={1}
            placeholder="عنوان"
            style={{ width: 240 }}
            onChangeText={text => this.setState({ title: text })}
            highlightColor={primaryDark}
            value={this.state.title}
          />

          <TextInput
            placeholder="پیام خود را وارد کنید"
            placeholderTextColor={placeholderTextColor}
            style={styles.textInput}
            multiline={true}
            underlineColorAndroid={transparent}
            onChangeText={text => this.setState({ message: text })}
            returnKeyType="done"
            value={this.state.message}
          />
        </View>

        <TouchableOpacity onPress={this.save.bind(this)}>
          <View style={{ flexDirection: 'row' }}>
            <View
              style={{
                flex: 1,
                height: 48,
                backgroundColor: primaryDark,
                alignItems: 'center',
                justifyContent: 'center',
                borderBottomLeftRadius: 4,
                borderBottomRightRadius: 4,
              }}
            >
              <Text style={{ fontSize: 18, color: 'white' }}>ذخیره</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  save() {
    let data = {
      title: this.state.title,
      description: this.state.message,
    };
    if (this.state.id) {
      data = Object.assign(data, { id: this.state.id });
    }
    this.props.onPress(data);
  }
}

const styles = StyleSheet.create({
  textInput: {
    paddingHorizontal: 4,
    borderColor: primaryDark,
    borderWidth: 0.5,
    height: 120,
    width: 240,
    borderRadius: 2,
    fontSize: 14,
    textAlignVertical: 'top',
    textAlign: 'right',
    marginTop: 32,
  },
});
