import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Text,
  ActivityIndicator,
} from 'react-native';

import {
  primary,
  placeholderTextColor,
  transparent,
  primaryDark,
} from '../constants/colors';

export default class LoadingPopUp extends Component {
  render() {
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          width: 272,
          borderRadius: 4,
        }}
      >
        <View style={{ padding: 16, alignItems: 'center' }}>
          <ActivityIndicator
            color={primaryDark}
            size={'large'}
            style={{ marginVertical: 16 }}
          />
          <Text style={{ color: primaryDark, fontSize: 14 }}>
            {this.props.message}
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textInput: {
    paddingHorizontal: 4,
    borderColor: primaryDark,
    borderWidth: 0.5,
    height: 200,
    width: 240,
    borderRadius: 2,
    fontSize: 14,
    textAlignVertical: 'top',
    textAlign: 'right',
    marginTop: 32,
  },
});
