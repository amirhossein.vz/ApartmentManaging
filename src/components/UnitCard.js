import React, { Component } from 'react';
import { View, FlatList, Image, Text, TouchableOpacity } from 'react-native';
import { phonecall } from 'react-native-communications';

import { primary, primaryDark } from '../constants/colors';
import images from '@assets/images';

class UnitCardRow extends Component {
  render() {
    const { icon, title, data, mobile } = this.props;
    return (
      <View style={{ flexDirection: 'row' }}>
        <Image
          source={icon}
          style={{ height: 14, width: 14, tintColor: primaryDark }}
        />
        <Text
          style={{
            fontSize: 12,
            flex: 2,
            marginHorizontal: 16,
            color: 'black',
            textAlign: 'left'
          }}
        >
          {title}
        </Text>
        <View style={{ flex: 3, flexDirection: 'row' }}>
          <Text
            style={{ fontSize: 12, flex: 3, textAlign: 'left', color: 'black' }}
          >
            {data}
          </Text>
          {mobile && (
            <TouchableOpacity
              style={{ padding: 8 }}
              onPress={() => phonecall(mobile, true)}
            >
              <Image
                source={images.phone_icon}
                style={{ height: 14, width: 14, tintColor: primaryDark }}
              />
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  }
}

export default class UnitCard extends Component {
  render() {
    const {
      UnitNumber,
      FloorNumber,
      OwnerName,
      OwnerTel,
      TenantName,
      TenantTel,
      Area,
      ParkingCount,
      ParkingNumber,
      IsEmpty,
      IsSame,
      NumberOfPeople,
      Tell,
    } = this.props.unit;
    const residentText = IsEmpty
      ? 'خالی از سکنه'
      : NumberOfPeople + ' نفر خانوار';
    const parkingText = ParkingNumber
      ? `پارکینگ شماره ${ParkingNumber} `
      : 'پارکینگ ندارد';
    return (
      <View
        style={{
          marginVertical: 4,
          marginHorizontal: 8,
          backgroundColor: 'white',
          elevation: 2,
        }}
      >
        <View
          style={{
            paddingVertical: 8,
            backgroundColor: primaryDark,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'row',
          }}
        >
          <TouchableOpacity
            style={{ paddingVertical: 4, paddingHorizontal: 16 }}
            onPress={() => this.props.navigateToEdit(this.props.unit)}
          >
            <Image
              source={images.edit_icon}
              style={{ height: 18, width: 18, tintColor: 'white' }}
            />
          </TouchableOpacity>
          <Text
            style={{
              color: 'white',
              flex: 1,
              textAlign: 'center',
              fontSize: 16,
            }}
          >
            {`واحد ${UnitNumber}`}
          </Text>
          <TouchableOpacity
            style={{ paddingVertical: 4, paddingHorizontal: 16 }}
            onPress={this.props.deleteUnit.bind(this, this.props.unit)}
          >
            <Image
              source={images.delete_icon}
              style={{ height: 18, width: 18, tintColor: 'white' }}
            />
          </TouchableOpacity>
        </View>
        <View style={{ padding: 8 }}>
          <UnitCardRow
            icon={images.person_icon}
            title={IsSame ? 'نام مالک و ساکن' : 'نام مالک'}
            data={OwnerName}
            mobile={OwnerTel}
          />
          {!(IsEmpty || IsSame) && (
            <UnitCardRow
              icon={images.person_icon}
              title="نام ساکن"
              data={TenantName}
              mobile={TenantTel}
            />
          )}
          <View style={{ flexDirection: 'row' }}>
            <Image
              source={images.floor_icon}
              style={{ height: 14, width: 14, tintColor: primaryDark }}
            />
            <Text
              style={{
                fontSize: 12,
                flex: 2,
                marginHorizontal: 16,
                textAlign: 'left'
              }}
            >
              {`طبقه ${FloorNumber}  ، ${parkingText} ، ${residentText} `}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}
