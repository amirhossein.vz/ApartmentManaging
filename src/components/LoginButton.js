import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import { accentLight, accentDark } from '../constants/colors';

export default class LoginButton extends React.Component {
  render() {
    const { onPress, title } = this.props;
    return (
      <TouchableOpacity onPress={onPress} style={{ flexDirection: 'row' }}>
        <LinearGradient
          colors={[accentLight, accentDark]}
          locations={[0, 1]}
          style={{
            flex: 1,
            marginHorizontal: 32,
            backgroundColor: accentLight,
            borderRadius: 8,
            alignItems: 'center',
            justifyContent: 'center',
            paddingVertical: 10,
          }}
        >
          <Text style={{ fontSize: 20, color: 'white' }}>{title}</Text>
        </LinearGradient>
      </TouchableOpacity>
    );
  }
}
