import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Image,
  ScrollView,
  StyleSheet,
} from 'react-native';

import { apartmentStore, persistStore } from '../stores';
import { primary } from '../constants/colors';
import images from '@assets/images';

function Header({
  headerInfo: {
    name,
    apartmentName,
    role,
    postalCode,
    lobbyTell,
    numberOfFloors,
    numberOfUnits,
    apartmentImage,
    unit,
    userUnitNumber,
  },
}) {
  return (
    <View style={{ backgroundColor: primary }}>
      <View style={{ alignItems: 'center' }}>
        <View
          style={{
            borderRadius: 46,
            marginTop: 8,
            marginBottom: 8,
            marginRight: 8,
          }}
        >
          <Image
            source={images.user_image}
            style={{
              height: 92,
              width: 92,
              resizeMode: 'cover',
              borderRadius: 46,
            }}
          />
        </View>
        <Text style={{ color: 'white', fontSize: 16 }}>{name}</Text>
        {role == 'مدیر' ? (
          <Text style={{ color: 'white' }}>
            {role} آپارتمان {apartmentName}
          </Text>
        ) : (
          <Text style={{ color: 'white' }}>
            {role} واحد {userUnitNumber}
          </Text>
        )}
        {role == 'مدیر' && (
          <Text style={{ color: 'white' }}>
            {numberOfFloors} طبقه،‌ {numberOfUnits} واحد
          </Text>
        )}
        {!(role == 'مدیر') && (
          <Text style={{ color: 'white' }}>آپارتمان {apartmentName}</Text>
        )}
        <Text>{postalCode}</Text>
      </View>
    </View>
  );
}

function DrawerRow({ text, icon, onPress }) {
  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={{
          flexDirection: 'row',
          paddingVertical: 16,
          alignItems: 'center',
          paddingHorizontal: 16,
        }}
      >
        <Image
          source={icon}
          style={{ height: 22, width: 22, tintColor: 'black' }}
        />
        <Text
          style={{ flex: 1, color: 'black', textAlign: 'left', marginLeft: 16 }}
        >
          {text}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

export default class DrawerPanel extends Component {
  render() {
    const {
      userRole,
      apartmentName,
      userName,
      postalCode,
      numberOfFloors,
      numberOfUnits,
      lobbyTell,
      apartmentImage,
      userStatus,
      userUnitNumber,
    } = apartmentStore;
    return (
      <View style={{ flex: 1 }}>
        <Header
          headerInfo={{
            name: userName,
            apartmentName: apartmentName,
            role:
              (userRole == 'Admin') | (userRole == 'Manager')
                ? 'مدیر'
                : userStatus == 0 ? 'ساکن' : 'مالک',
            postalCode,
            lobbyTell,
            numberOfFloors,
            numberOfUnits,
            image: apartmentImage,
            userUnitNumber: userUnitNumber,
          }}
        />
        <ScrollView style={{ flex: 1 }}>
          {(apartmentStore.userRole == 'Admin' ||
            apartmentStore.userRole == 'Manager') && (
            <View>
              <DrawerRow
                icon={images.units_icon}
                text="لیست واحدها"
                onPress={() => this.props.navigation.navigate('Units')}
              />
              <DrawerRow
                icon={images.add_contact_icon}
                text="افزودن آدرس جدید"
                onPress={() => this.props.navigation.navigate('AddContact')}
              />
              <DrawerRow
                icon={images.facility_menu_icon}
                text="مدیریت امکانات رفاهی"
                onPress={() =>
                  this.props.navigation.navigate('Facilities', {
                    nextPage: 'GENERAL',
                  })}
              />
            </View>
          )}
          <DrawerRow
            icon={images.contactUs_icon}
            text="انتقادات و پیشنهادات"
            onPress={() => this.props.navigation.navigate('Suggestion')}
          />
          <DrawerRow
            icon={images.request_icon}
            text={
              apartmentStore.userRole == 'Admin' ||
              apartmentStore.userRole == 'Manager'
                ? 'درخواست‌ها'
                : 'پیگیری درخواست‌ها'
            }
            onPress={() => this.props.navigation.navigate('Requests')}
          />
          <DrawerRow
            icon={images.contact_apartment_icon}
            text="ارتباط با ساختمان"
            onPress={() => this.props.navigation.navigate('ContactUs')}
          />
          <DrawerRow
            icon={images.rule_icon}
            text="قوانین ساختمان"
            onPress={() => this.props.navigation.navigate('Rules')}
          />
          <DrawerRow
            icon={images.lobbey_icon}
            text="تماس با لابی"
            onPress={() => this.props.navigation.navigate('Lobby')}
          />
          <DrawerRow
            icon={images.logout_icon}
            onPress={this.logout.bind(this)}
            text="خروج"
          />
        </ScrollView>
      </View>
    );
  }

  logout() {
    persistStore.clearStore();
    this.props.navigation.navigate('Login');
  }
}
