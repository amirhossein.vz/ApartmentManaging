import React from 'react';
import { View, Text } from 'react-native';

import { primary } from '../constants/colors';

export default function SpecialSansStatus({ reserved }) {
  return (
    <View
      style={{
        borderWidth: 1,
        borderRadius: 2,
        paddingVertical: 2,
        paddingHorizontal: 4,
        borderColor: reserved ? 'red' : primary,
      }}
    >
      <Text style={{ color: reserved ? 'red' : primary }}>
        {reserved ? 'Reserved' : 'Free'}
      </Text>
    </View>
  );
}
