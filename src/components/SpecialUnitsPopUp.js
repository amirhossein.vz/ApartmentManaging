import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Text,
  Image,
  FlatList,
} from 'react-native';

import images from '@assets/images';
import {
  primary,
  placeholderTextColor,
  transparent,
  primaryDark,
} from '../constants/colors';
import { apartmentStore } from '../stores';

class SpecialUnitPopUpRow extends Component {
  render() {
    const { unitNumber, isSelected, onPress } = this.props;
    return (
      <View>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            paddingHorizontal: 8,
            justifyContent: 'space-between',
            paddingVertical: 8,
          }}
          onPress={() => onPress(unitNumber)}
        >
          <Text>واحد {unitNumber}</Text>
          <Image
            source={isSelected ? images.checked_icon : images.unchecked_icon}
            style={{ height: 18, width: 18, tintColor: primaryDark }}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default class SpecialUnitsPopUp extends Component {
  constructor() {
    super();
    this.state = {
      selectedUnits: [],
    };
  }

  componentDidMount() {}

  render() {
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          width: 272,
          borderRadius: 4,
          padding: 16,
        }}
      >
        <FlatList
          data={apartmentStore.units.map(item => item.UnitNumber)}
          renderItem={item => (
            <SpecialUnitPopUpRow
              unitNumber={item.item}
              isSelected={this.state.selectedUnits.includes(item.item)}
              onPress={unitNumber => this.onCheck(unitNumber)}
            />
          )}
          extraData={this.state.selectedUnits.length}
          style={{ height: 300 }}
          keyExtractor={(item, index) => index}
        />

        <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
          <TouchableOpacity
            style={{
              paddingLeft: 8,
              paddingHorizontal: 16,
              paddingVertical: 8,
            }}
            onPress={() => this.props.onDismiss()}
          >
            <Text style={{ color: primaryDark }}>لغو</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              marginRight: 8,
              paddingHorizontal: 16,
              paddingVertical: 8,
            }}
            onPress={this.onConfirm.bind(this)}
          >
            <Text style={{ color: primaryDark }}>تایید</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  onConfirm() {
    this.props.onConfirm(this.state.selectedUnits);
  }

  onCheck(unitNumber) {
    if (this.state.selectedUnits.includes(unitNumber)) {
      this.setState({
        selectedUnits: this.state.selectedUnits.filter(
          item => item != unitNumber,
        ),
      });
    } else {
      this.state.selectedUnits.push(unitNumber);
    }
    this.forceUpdate();
  }
}

const styles = StyleSheet.create({
  textInput: {
    paddingHorizontal: 4,
    borderColor: primaryDark,
    borderWidth: 0.5,
    height: 120,
    width: 240,
    borderRadius: 2,
    fontSize: 14,
    textAlignVertical: 'top',
    textAlign: 'right',
    marginTop: 16,
  },
});
