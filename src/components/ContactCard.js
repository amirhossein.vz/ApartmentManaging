import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import MapView from 'react-native-maps';
import { phonecall } from 'react-native-communications';

import { primary, darkGrey, primaryDark } from '../constants/colors';
import images from '@assets/images';

function ImageWrapper({ image }) {
  return (
    <View
      style={{
        borderColor: primary,
        padding: 2,
        height: 20,
        width: 20,
        borderWidth: 1,
        borderRadius: 100,
      }}
    >
      <Image
        source={image}
        style={{
          tintColor: primary,
          height: 14,
          width: 14,
          resizeMode: 'contain',
        }}
      />
    </View>
  );
}

function ContactRow({ image, title, data, fitToScreen }) {
  return (
    <View
      style={{
        flexDirection: 'row',
        marginVertical: 2,
        marginHorizontal: 4,
      }}
    >
      <ImageWrapper image={image} />
      <Text
        style={{
          marginHorizontal: 8,
          flex: 1,
          fontSize: fitToScreen ? 13 : 12,
          color: darkGrey,
          textAlign: 'left'
        }}
      >
        {title}
      </Text>
      <Text
        style={{
          flex: 3,
          fontSize: fitToScreen ? 13 : 12,
          color: darkGrey,
          textAlign: 'left',
        }}
      >
        {data}
      </Text>
    </View>
  );
}

export default class ContactCard extends React.Component {
  render() {
    const { contact, onPress, selectedPhone } = this.props;
    return (
      <View
        style={{
          marginHorizontal: 8,
          marginVertical: selectedPhone == contact.Tel ? 8 : 4,
          backgroundColor: 'white',
          elevation: 3,
          flex: selectedPhone == contact.Tel ? 1 : 0,
        }}
      >
        <TouchableOpacity
          onPress={this.onCardPress.bind(this)}
          onLongPress={() => this.props.onLongPress(contact)}
        >
          <MapView
            liteMode
            style={{ height: selectedPhone == contact.Tel ? 400 : 80 }}
            initialRegion={{
              latitude: parseFloat(parseFloat(contact.Lat).toFixed(4)),
              longitude: parseFloat(parseFloat(contact.Lng).toFixed(4)),
              latitudeDelta: 0.01,
              longitudeDelta: 0.01,
            }}
          >
            <MapView.Marker
              coordinate={{
                latitude: parseFloat(parseFloat(contact.Lat).toFixed(4)),
                longitude: parseFloat(parseFloat(contact.Lng).toFixed(4)),
              }}
            >
              <Image
                source={images.address_icon}
                style={{
                  height: selectedPhone == contact.Tel ? 48 : 32,
                  width: selectedPhone == contact.Tel ? 48 : 32,
                  tintColor: primaryDark,
                  resizeMode: 'cover',
                }}
              />
            </MapView.Marker>
          </MapView>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => phonecall(contact.Tel, true)}
          style={{ flex: selectedPhone == contact.Tel ? 1 : 0 }}
          onLongPress={() => this.props.onLongPress(contact)}
        >
          <View
            style={{
              marginVertical: selectedPhone == contact.Tel ? 16 : 0,
              flex: selectedPhone == contact.Tel ? 1 : 0,
              justifyContent: 'space-between',
            }}
          >
            <ContactRow
              image={images.userIcon}
              title="نام"
              data={contact.Name}
              fitToScreen={selectedPhone == contact.Tel ? true : false}
            />
            <ContactRow
              image={images.address_icon}
              title="آدرس"
              data={contact.Address}
              fitToScreen={selectedPhone == contact.Tel ? true : false}
            />
            <ContactRow
              image={images.phone_icon}
              title="شماره تماس"
              data={contact.Tel}
              fitToScreen={selectedPhone == contact.Tel ? true : false}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  onCardPress() {
    this.props.onPress(this.props.contact.Tel);
    this.forceUpdate();
  }
}
