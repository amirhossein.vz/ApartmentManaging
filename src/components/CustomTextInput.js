import React, { Component } from 'react';
import { View } from 'react-native';

import { placeholderTextColor, primary } from '../constants/colors';
import { MKTextField } from 'react-native-material-kit';

export default class CustomTextInput extends Component {
  state = {
    value: '',
  };
  render() {
    const {
      ref,
      onSubmitData,
      keyboardType = 'default',
      returnKeyType = 'done',
      placeholder,
      style,
      onReturnKeyPress,
    } = this.props;
    return (
      <View stlye={style | { flexDirection: 'row' }}>
        <MKTextField
          ref={ref | 'textInput'}
          multiline={false}
          onSubmitEditing={onSubmitData | onReturnKeyPress}
          keyboardType={keyboardType}
          returnKeyType={returnKeyType}
          floatingLabelEnable={false}
          tintColor={placeholderTextColor}
          textInputStyle={{
            color: 'black',
            fontSize: 14,
            flexGrow: 1,
            textAlign: 'center',
          }}
          underlineSize={1}
          placeholder={placeholder}
          style={{ flex: 1 }}
          onChangeText={text => [
            this.setState({ value: text }),
            this.updateParentInfo(text),
          ]}
          value={this.state.value}
          highlightColor={primary}
        />
      </View>
    );
  }

  updateParentInfo(data) {
    const { onChangeText } = this.props;
    if (onChangeText) {
      onChangeText(data);
    }
  }
}
