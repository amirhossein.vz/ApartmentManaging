import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Text,
  Image,
  ToastAndroid,
} from 'react-native';
import { MKTextField } from 'react-native-material-kit';
import DateTimePicker from 'react-native-modal-datetime-picker';

import images from '@assets/images';
import {
  primary,
  placeholderTextColor,
  transparent,
  primaryDark,
} from '../constants/colors';
import { apartmentStore } from '../stores';
import { addSansQuery, changeFacilityDetails } from '../network/Queries';
import { facilityStatusTypes } from '../constants/values';

const genderArray = [
  {
    value: 'MALE',
    id: 0,
  },
  {
    value: 'FEMALE',
    id: 1,
  },
  { value: 'BOTH', id: 2 },
  { value: 'NONE', id: 3 },
];

const textToDay = [
  {
    text: 'شنبه',
    value: 'SAT',
  },
  {
    text: 'یکشنبه',
    value: 'SUN',
  },
  {
    text: 'دوشنبه',
    value: 'MON',
  },
  {
    text: 'سه‌شنبه',
    value: 'TUS',
  },
  {
    text: 'چهارشنبه',
    value: 'WEN',
  },
  {
    text: 'پنج‌شنبه',
    value: 'THU',
  },
  {
    text: 'جمعه',
    value: 'FRI',
  },
];

const MALE = 'MALE';
const FEMALE = 'FEMALE';
const BOTH = 'BOTH';
const NONE = 'NONE';

const GENERAL_TYPE = 'GENERAL';
const SPECIAL_TYPE = 'SPECIAL';
const OFF_TYPE = 'OFF';

function TypeSelector({ text, selected, onPress }) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{ flexDirection: 'row', alignItems: 'center' }}
    >
      <Text style={{ marginRight: 4, fontSize: 12, color: 'black' }}>
        {text}
      </Text>
      <Image
        source={selected ? images.checked_icon : images.unchecked_icon}
        style={{ width: 18, height: 18, tintColor: primaryDark }}
      />
    </TouchableOpacity>
  );
}

export default class AddMessagePopUp extends Component {
  constructor() {
    super();
    this.state = {
      fromHour: null,
      fromMinute: null,
      toHour: null,
      toMinute: null,
      type: GENERAL_TYPE,
      gender: MALE,
      description: '',
      price: '',
      timePickerType: null,
    };
  }

  componentDidMount() {
    if (this.props.item) {
      const {
        FromHour,
        ToHour,
        DayOfWeek,
        Status,
        Price,
        GenderType,
        Description,
      } = this.props.item;
      this.setState({
        fromHour: parseInt(FromHour.substr(0, 2)),
        fromMinute: parseInt(FromHour.substr(3, 5)),
        toHour: parseInt(ToHour.substr(0, 2)),
        toMinute: parseInt(ToHour.substr(3, 5)),
        type: facilityStatusTypes.find(item => item.id == Status).value,
        gender: genderArray.find(item => item.id == GenderType).value,
        description: Description,
        price: Price,
        showTimePicker: false,
      });
    }
  }

  render() {
    let initialDate = new Date();
    initialDate.setHours(0);
    initialDate.setMinutes(0);
    return (
      <View
        style={{
          backgroundColor: 'white',
          elevation: 2,
          width: 272,
          borderRadius: 4,
        }}
      >
        <DateTimePicker
          isVisible={this.state.showTimePicker}
          onConfirm={time => this.onConfirmTime(time)}
          onCancel={() => this.setState({ showTimePicker: false })}
          mode="time"
          is24Hour={true}
          date={initialDate}
        />
        <View style={{ padding: 16, paddingTop: 8 }}>
          {!this.props.item && (
            <TouchableOpacity
              onPress={() =>
                this.setState({ showTimePicker: true, timePickerType: 'FROM' })}
            >
              <View
                style={{
                  flexDirection: 'row',
                  paddingVertical: 8,
                  alignItems: 'center',
                }}
              >
                <Text style={{ marginRight: 16 }}>از ساعت</Text>
                {this.state.fromHour != null &&
                this.state.fromMinute != null ? (
                  <Text style={{ color: 'black', fontSize: 16 }}>
                    {this.state.fromHour} : {this.state.fromMinute}
                  </Text>
                ) : (
                  <Image
                    source={images.time_start_icon}
                    style={{ tintColor: primaryDark, width: 18, height: 18 }}
                  />
                )}
              </View>
            </TouchableOpacity>
          )}
          {!this.props.item && (
            <TouchableOpacity
              onPress={() =>
                this.setState({ showTimePicker: true, timePickerType: 'TO' })}
            >
              <View
                style={{
                  flexDirection: 'row',
                  paddingVertical: 8,
                  alignItems: 'center',
                }}
              >
                <Text style={{ marginRight: 16 }}>تا ساعت</Text>
                {this.state.toHour != null && this.state.toMinute != null ? (
                  <Text style={{ color: 'black', fontSize: 16 }}>
                    {this.state.toHour} : {this.state.toMinute}
                  </Text>
                ) : (
                  <Image
                    source={images.time_end_icon}
                    style={{ tintColor: primaryDark, width: 18, height: 18 }}
                  />
                )}
              </View>
            </TouchableOpacity>
          )}
          <View style={{ marginTop: 8 }}>
            <Text style={{ textAlign: 'left' }}>نوع سانس را انتخاب کنید</Text>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}
            >
              <TypeSelector
                text="عمومی"
                onPress={() => this.setState({ type: GENERAL_TYPE })}
                selected={this.state.type == GENERAL_TYPE}
              />
              <TypeSelector
                text="خصوصی"
                onPress={() => this.setState({ type: SPECIAL_TYPE })}
                selected={this.state.type == SPECIAL_TYPE}
              />
              <TypeSelector
                text="تعطیلات"
                onPress={() => this.setState({ type: OFF_TYPE })}
                selected={this.state.type == OFF_TYPE}
              />
            </View>
          </View>
          {this.state.type == GENERAL_TYPE && (
            <View style={{ marginTop: 16 }}>
              <Text style={{ textAlign: 'left' }}>مخصوص</Text>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}
              >
                <TypeSelector
                  text="آقایان"
                  onPress={() => this.setState({ gender: MALE })}
                  selected={this.state.gender == MALE}
                />
                <TypeSelector
                  text="بانوان"
                  onPress={() => this.setState({ gender: FEMALE })}
                  selected={this.state.gender == FEMALE}
                />
                <TypeSelector
                  text="هردو"
                  onPress={() => this.setState({ gender: BOTH })}
                  selected={this.state.gender == BOTH}
                />
              </View>
            </View>
          )}
          {this.state.type == SPECIAL_TYPE && (
            <View style={{ marginTop: 16 }}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Text style={{ marginRight: 16 }}>قیمت سانس</Text>
                <MKTextField
                  ref="textInput"
                  multiline={false}
                  keyboardType="numeric"
                  returnKeyType="done"
                  floatingLabelEnable={false}
                  tintColor={
                    this.props.haveEmptyField && this.state.price == ''
                      ? 'red'
                      : placeholderTextColor
                  }
                  textInputStyle={{
                    fontSize: 14,
                    color: 'black',
                    flexGrow: 1,
                    textAlign: 'center',
                  }}
                  underlineSize={1}
                  placeholder="۱۵۰۰۰"
                  style={{ width: 80 }}
                  onChangeText={text => this.setState({ price: text })}
                  highlightColor={primaryDark}
                  value={this.state.price}
                />
                <Text style={{ marginLeft: 8 }}>تومان</Text>
              </View>
            </View>
          )}
          {this.state.type == OFF_TYPE && (
            <TextInput
              placeholder="علت تعطیلات سانس را بنویسید"
              placeholderTextColor={placeholderTextColor}
              style={styles.textInput}
              multiline={true}
              underlineColorAndroid={transparent}
              onChangeText={text => this.setState({ description: text })}
              returnKeyType="done"
              value={this.state.description}
            />
          )}
        </View>

        <TouchableOpacity onPress={this.onConfirmNewSans.bind(this)}>
          <View style={{ flexDirection: 'row' }}>
            <View
              style={{
                flex: 1,
                height: 48,
                backgroundColor: primaryDark,
                alignItems: 'center',
                justifyContent: 'center',
                borderBottomLeftRadius: 4,
                borderBottomRightRadius: 4,
              }}
            >
              <Text style={{ fontSize: 18, color: 'white' }}>ذخیره</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }

  onConfirmTime(time) {
    if (this.state.timePickerType == 'FROM') {
      this.setState({
        fromHour: time.getHours(),
        fromMinute: time.getMinutes(),
      });
    } else if ((this.state.timePickerType = 'TO')) {
      this.setState({ toHour: time.getHours(), toMinute: time.getMinutes() });
    }

    this.setState({ showTimePicker: false });
  }

  async onConfirmNewSans() {
    console.warn(this.state.type);
    if (this.state.type == 'SPECIAL' && this.state.price == '') {
      this.props.setEmptyField(true);
      ToastAndroid.show('لطفا قیمت سانس را وارد کنید', ToastAndroid.LONG);
      return;
    }

    this.props.onChangeLoading(true);
    const {
      fromHour,
      fromMinute,
      toHour,
      toMinute,
      gender,
      description,
      price,
      type,
    } = this.state;
    let fromHourText, toHourText, fromMinText, toMinText;
    if (fromHour < 10) {
      fromHourText = '0' + fromHour;
    } else {
      fromHourText = fromHour;
    }

    if (fromMinute < 10) {
      fromMinText = '0' + fromMinute;
    } else {
      fromMinText = fromMinute;
    }

    if (toHour < 10) {
      toHourText = '0' + toHour;
    } else {
      toHourText = toHour;
    }

    if (toMinute < 10) {
      toMinText = '0' + toMinute;
    } else {
      toMinText = toMinute;
    }
    let newItem = {
      apartment_ID: apartmentStore.apartmentID,
      facility_ID: this.props.id,
      price: price,
      genderType: genderArray.find(item => item.value == gender).id,
      status: facilityStatusTypes.find(item => item.value == type).id,
      fromHour: `${fromHourText}:${fromMinText}`,
      toHour: `${toHourText}:${toMinText}`,
      description: description,
    };
    if (this.props.item) {
      Object.assign(newItem, {
        id: this.props.item.Special_ID,
        facilityDetail_ID: this.props.item.FacilityDetail_ID,
        specialDate: this.props.specialDate,
      });
    } else {
      Object.assign(newItem, {
        dayOfWeek: this.props.day.value,
      });
    }
    if (this.props.item) {
      const changeRes = changeFacilityDetails(newItem);
      if (changeRes) {
        await this.props.onRetrieveData();
        this.props.onPress();
      }
    } else {
      const addSansRes = await addSansQuery(newItem);
      if (addSansRes.status) {
        this.props.onRetrieveData(addSansRes.data);
        this.props.onPress();
      } else {
        // this.props.onDismissPartly();
      }
    }
    this.props.onChangeLoading(false);
  }
}

const styles = StyleSheet.create({
  textInput: {
    paddingHorizontal: 4,
    borderColor: primaryDark,
    borderWidth: 0.5,
    height: 120,
    width: 240,
    borderRadius: 2,
    fontSize: 14,
    textAlignVertical: 'top',
    textAlign: 'right',
    marginTop: 16,
  },
});
