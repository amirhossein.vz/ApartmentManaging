import React, { Component } from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';

import { primary, primaryDark } from '../constants/colors';
import images from '@assets/images';
import { apartmentStore } from '../stores';
import { parseTimeToJalaali } from '../utils';
export default class InformCard extends Component {
  render() {
    const { Title, Description, CreatedAtDatetime } = this.props.item;
    return (
      <View
        style={{
          marginVertical: 4,
          marginHorizontal: 8,
          backgroundColor: 'white',
          elevation: 2,
        }}
      >
        <View
          style={{
            paddingVertical: 8,
            backgroundColor: primaryDark,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'row',
          }}
        >
          {apartmentStore.isManager && (
            <TouchableOpacity
              style={{ paddingVertical: 4, paddingHorizontal: 16 }}
              onPress={() => this.props.navigateToEdit(this.props.item)}
            >
              <Image
                source={images.edit_icon}
                style={{ height: 18, width: 18, tintColor: 'white' }}
              />
            </TouchableOpacity>
          )}
          <Text
            style={{
              color: 'white',
              flex: 1,
              textAlign: 'center',
              fontSize: 16,
            }}
          >
            {Title}
          </Text>
          {apartmentStore.isManager && (
            <TouchableOpacity
              style={{ paddingVertical: 4, paddingHorizontal: 16 }}
              onPress={() => this.props.delete(this.props.item)}
            >
              <Image
                source={images.delete_icon}
                style={{ height: 18, width: 18, tintColor: 'white' }}
              />
            </TouchableOpacity>
          )}
        </View>
        <View style={{ padding: 8 }}>
          <Text style={{textAlign: 'left'}}>{Description}</Text>
          <Text style={{ fontSize: 10, textAlign: 'right' }}>
            {parseTimeToJalaali(CreatedAtDatetime)}
          </Text>
        </View>
      </View>
    );
  }
}
