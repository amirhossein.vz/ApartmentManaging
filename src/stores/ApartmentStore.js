import { observable, computed } from 'mobx';
import { persist } from 'mobx-persist';

class ApartmentStore {
  @observable userRole = null;
  @observable userID = null;
  @observable apartmentID = null;
  @observable apartmentName = null;
  @observable userName = null;
  @observable postalCode = null;
  @observable numberOfFloors = null;
  @observable numberOfUnits = null;
  @observable lobbyTell = null;
  @observable apartmentImage = null;
  @observable units = [];
  @observable phoneBook = [];
  @observable facility = [];
  @observable contactUs = [];
  @observable suggestion = [];
  @observable rule = [];
  @observable notification = [];
  @observable costs = [];
  @observable survey = [];
  @observable noticeBoard = [];
  @observable costTypes = [];
  @observable facilityTypes = [];
  @observable contactUsTypes = [];
  @observable userStatus = null;
  @observable userUnitNumber = null;
  @observable requests = [];
  @observable allApartments = [];

  @observable
  monthData = [
    { Name: 'فروردین', ID: 1 },
    { Name: 'اردیبهشت', ID: 2 },
    { Name: 'خرداد', ID: 3 },
    { Name: 'تیر', ID: 4 },
    { Name: 'مرداد', ID: 5 },
    { Name: 'شهریور', ID: 6 },
    { Name: 'مهر', ID: 7 },
    { Name: 'آبان', ID: 8 },
    { Name: 'آذر', ID: 9 },
    { Name: 'دی', ID: 10 },
    { Name: 'بهمن', ID: 11 },
    { Name: 'اسفند', ID: 12 },
  ];
  @computed
  get isManager() {
    return this.userRole == 'Admin' || this.userRole == 'Manager';
  }

  @computed
  get notifCount() {
    if (this.notification.length > 0) {
      return this.notification.filter(item => item.IsDone == 0).length;
    }
    return 0;
  }

  clearStore() {
    this.userRole = null;
    this.userID = null;
    this.apartmentID = null;
    this.apartmentName = null;
    this.userName = null;
    this.postalCode = null;
    this.numberOfFloors = null;
    this.numberOfUnits = null;
    this.lobbyTell = null;
    this.apartmentImage = null;
    this.units = [];
    this.phoneBook = [];
    this.facility = [];
    this.contactUs = [];
    this.suggestion = [];
    this.rule = [];
    this.notification = [];
    this.costs = [];
    this.survey = [];
    this.noticeBoard = [];
  }
}

const apartmentStore = new ApartmentStore();

export default apartmentStore;
export { ApartmentStore };
