import { observable } from 'mobx';
import { persist } from 'mobx-persist';

class PersistStore {
  @persist
  @observable
  token = null;

  @persist
  @observable
  username = null; // TODO : MUST BE DELETED

  @persist
  @observable
  password = null; // TODO : MUST BE DELETED

  clearStore() {
    this.token = null;
    this.username = null;
    this.password = null;
  }
}

const persistStore = new PersistStore();

export default persistStore;
export { PersistStore };
